#! /bin/bash
database_name=$1
if [ -z "${database_name}" ]; then
    pretty_echo "remote database name not provided."
    exit 1
fi

pretty_echo( ) {
echo ">>> $1 <<<"
}

echo "--------------------------------------------"
pretty_echo "using $database_name as the database"
echo "--------------------------------------------"

psql -d $database_name -U postgres -p 5432 -h postgres -c "CREATE SCHEMA $database_name;"
psql -d $database_name -U postgres -p 5432 -h postgres -c "ALTER DATABASE $database_name SET search_path TO $database_name;"