#! /bin/bash
database_name=$1
if [ -z "${database_name}" ]; then
    database_name='xerox'
fi

pretty_echo( ) {
echo ">>> $1 <<<"
}

echo "--------------------------------------------"
pretty_echo "using $database_name as the database"
echo "--------------------------------------------"

# CHECK CONTAINER
check_if_container_is_running() {
  container_running=$(docker ps --filter NAME=xerox_postgres -q)
}

xerox_postgres_container_exists=$(docker ps -a --filter NAME=xerox_postgres -q)
if [ ${xerox_postgres_container_exists} ]; then
    pretty_echo "container exists"

   check_if_container_is_running
    if [ -z ${container_running} ]; then
      pretty_echo "starting container"
      docker start xerox_postgres
    fi
else
    pretty_echo "creating postgres container"
    docker run -e "POSTGRES_PASSWORD=postgres" \
               -p 5433:5432 \
               --name xerox_postgres -d postgres:15.3
fi

check_if_container_is_running
RETRIES=0
until [ ${container_running} ] || [ $RETRIES -eq 10 ]; do
   pretty_echo "waiting for container to start postgres service. Retry: $RETRIES"
   sleep 2
   (( RETRIES++ ))
   check_if_container_is_running
done

if [ ${container_running} ]; then
    pretty_echo "container running"
else
    pretty_echo "postgres container could not be started. Is there another container using the port? If not, try to rerun the script."
    exit 1
fi

# CHECK DATABASE ACCESSIBILITY
check_if_database_is_accessible() {
  docker exec xerox_postgres /bin/su postgres -c "psql -c 'select datname from pg_database;'"
  accessible=$?
}

check_if_database_is_accessible
RETRIES=0
until [ $accessible -eq 0 ] || [ $RETRIES -eq 10 ]; do
   pretty_echo "waiting for database to be accessible. Retry: $RETRIES"
   sleep 2
    (( RETRIES++ ))
   check_if_database_is_accessible
done

if [ $accessible -eq 0 ]; then
    pretty_echo "database accessible"
else
    pretty_echo "database is not accessible - maybe it is not ready yet? Try to rerun the script."
    exit 1
fi

# CREATE THE SUPERUSER and USER xerox_api_user
if [ -z ${xerox_postgres_container_exists} ]; then
   pretty_echo "since this is a new container, we create a superuser role"
   docker exec xerox_postgres /bin/su postgres -c "psql -c 'CREATE ROLE rds_superuser WITH SUPERUSER;'"
fi
docker exec xerox_postgres /bin/su postgres -c "psql -c \"CREATE USER xerox_api_user WITH PASSWORD 'xerox_api_password';\""

# CHECK THE DATABASE SCHEMA
check_if_database_exists() {
  all_dbs=$(docker exec xerox_postgres /bin/su postgres -c "psql -c 'select datname from pg_database;'")
  db_exists=`echo "${all_dbs}" | grep "${database_name}$"`
}

check_if_database_exists
if [ -n "$db_exists"  ]; then
    pretty_echo "$database_name database exists"
else
    pretty_echo "creating $database_name database"
    docker exec xerox_postgres /bin/su postgres -c "createdb $database_name -U postgres && psql -d $database_name -U postgres -c 'CREATE SCHEMA $database_name;' && psql -d $database_name -U postgres -c 'ALTER DATABASE $database_name SET search_path TO $database_name;'"
fi

check_if_database_exists
RETRIES=0
until [ -n "$db_exists" ] || [ $RETRIES -eq 10 ]; do
   pretty_echo "waiting for database to be available. Retry: $RETRIES"
   sleep 2
    (( RETRIES++ ))
   check_if_database_exists
done
if [ -n "$db_exists" ]; then
    pretty_echo "$database_name database exists"
    docker exec xerox_postgres /bin/su postgres -c "psql -d xerox -c 'GRANT ALL ON SCHEMA xerox TO xerox_api_user'"
else
    pretty_echo "database doesn't exists - maybe it is not ready yet? Try to rerun the script."
    exit 1
fi
