FROM eclipse-temurin:17-jdk-alpine

ARG app_name
ARG port
ARG config_file
ARG app_version
ARG app_env
ARG app_service

# Set timezone
RUN ln -fs /usr/share/zoneinfo/Europe/London /etc/localtime

# Remove SSHD
RUN rm -rf /etc/service/sshd /etc/my_init.d/00_regen_ssh_host_keys.sh

# Folders
WORKDIR /home/java
RUN mkdir /home/java/config
RUN mkdir -p /etc/datadog-agent/conf.d/dockerlogs.d

# Environment Variables
ENV APP_PATH=/home/java/$app_name.jar
ENV DD_ENV=$app_env
ENV DD_SERVICE=$app_service
ENV DD_VERSION=$app_version
ENV DD_PROFILING_ENABLED=true

# Download DD java agent
RUN wget -O dd-java-agent.jar https://dtdg.co/latest-java-tracer

# Application
COPY target/*.jar $APP_PATH
COPY $config_file /home/java/config/application.yml
EXPOSE $port

ENTRYPOINT java -javaagent:/home/java/dd-java-agent.jar -XX:FlightRecorderOptions=stackdepth=256 -jar $APP_PATH