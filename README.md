# Xerox  API

This is a Spring Boot project to provide Restful APIs responsible for managing database operations
for the xerox notification.

> **Note**
> The xerox postgres container runs locally on port 5433 instead of the default port 5432.

## Local Installation

Clone repository and import into IDE.

```bash
git clone git@gitlab.com:nhsbsa/citizen-services/exemptions/hrt-ppc/xerox-notification-api.git
cd xerox-notification-api
```
### Creating the database

The simplest way is to run [./setup-db.bash](./setup-db.bash)

Or you can follow the below steps:

1 - run the container
```bash
docker run -e "POSTGRES_PASSWORD=postgres" \
               -p 5433:5432 \
               --name xerox_postgres -d postgres:15.3
```
2 - create the super-user role
```bash
docker exec xerox_postgres /bin/su postgres -c "psql -c 'CREATE ROLE rds_superuser WITH SUPERUSER;'"
```
3 - create application user
```bash
docker exec xerox_postgres /bin/su postgres -c "psql -c \"CREATE USER xerox_api_user WITH PASSWORD 'xerox_api_password';\""
```
4 - create database
```bash
docker exec xerox_postgres /bin/su postgres -c "createdb xerox -U postgres && psql -d xerox -U postgres -c 'CREATE SCHEMA xerox;' && psql -d xerox -U postgres -c 'ALTER DATABASE xerox SET search_path TO xerox;'"
```
5 - grant access to user
```bash
docker exec xerox_postgres /bin/su postgres -c "psql -d xerox -c 'GRANT ALL ON SCHEMA xerox TO xerox_api_user'"
```

Update below command with liquibase user details and execute liquibase:

```bash
./mvnw liquibase:update -Dliquibase.url=jdbc:postgresql://localhost:5433/xerox -Dliquibase.username=xerox_api_user -Dliquibase.password=xerox_api_password
```

### Building the application

Build application using maven command 
`./mvnw clean install -U`

### Running application locally

Run application using maven command.
`./mvnw spring-boot:run -Dspring-boot.run.profiles=local`
