package uk.nhs.nhsbsa.xerox.notification.interceptor;

import static uk.nhs.nhsbsa.xerox.notification.model.Constants.QUERY_PARAM_RUN_DATE;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import uk.nhs.nhsbsa.exemptions.exception.InvalidQueryParameterException;
import uk.nhs.nhsbsa.xerox.notification.model.Constants;

/**
 * RunDateValidationInterceptor implementation class to validate runDate is present in query
 * parameters.
 */
@Slf4j
@Component
@Order(value = 4)
public class RunDateValidationInterceptor implements HandlerInterceptor {

  @Override
  public boolean preHandle(
      HttpServletRequest request, HttpServletResponse response, Object object) {
    String httpMethod = request.getMethod();
    String requestUri = request.getRequestURI();
    if ((httpMethod.equalsIgnoreCase(HttpMethod.GET.name())
        && Constants.XEROX_NOTIFICATION_API_SEARCH_BY_EFFECTIVE_ON_DATE_URI.equalsIgnoreCase(
            requestUri))) {
      String runDateParameter = request.getParameter(QUERY_PARAM_RUN_DATE);
      if (runDateParameter != null) {
        try {
          LocalDate.parse(runDateParameter);
          return true;
        } catch (DateTimeParseException exception) {
          log.info(
              "Exception handling for {} with message {}",
              exception.getClass(),
              exception.getMessage());
        }
      }
      throw new InvalidQueryParameterException(QUERY_PARAM_RUN_DATE);
    }
    return true;
  }
}
