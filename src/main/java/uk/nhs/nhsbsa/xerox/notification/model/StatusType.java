package uk.nhs.nhsbsa.xerox.notification.model;

import com.fasterxml.jackson.annotation.JsonFormat;

/** Enumeration class for status type. */
@JsonFormat(shape = JsonFormat.Shape.STRING)
public enum StatusType {
  PENDING,
  SENT,
  FAILED,
  DELIVERED
}
