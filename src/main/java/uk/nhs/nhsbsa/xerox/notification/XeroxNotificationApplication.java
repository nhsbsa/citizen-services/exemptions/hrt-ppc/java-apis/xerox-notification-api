package uk.nhs.nhsbsa.xerox.notification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * Main class to initialise spring application and imports related configuration from other files.
 */
@SpringBootApplication
@EnableJpaAuditing
public class XeroxNotificationApplication {

  public static void main(String[] args) {
    SpringApplication.run(XeroxNotificationApplication.class, args);
  }
}
