package uk.nhs.nhsbsa.xerox.notification;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * The bsa-redact library is written to use system properties. This class will set the system
 * property based on the spring configuration loaded
 */
@Component
@Slf4j
public class RedactDisableForDebugging {

  @Value("${logging.redact.disabled:false}")
  private Boolean disabled;

  /** Sets the system property if redact is to be disabled. This will log sensitive information */
  @PostConstruct
  public void setAsDisabled() {
    log.warn("Redacting disabled is set to {}", disabled);
    System.setProperty("logging.redact.disabled", disabled.toString());
  }
}
