package uk.nhs.nhsbsa.xerox.notification.interceptor;

import static uk.nhs.nhsbsa.xerox.notification.model.Constants.HEADER_PARAM_CORRELATION_ID;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.MDC;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import uk.nhs.nhsbsa.exemptions.exception.InvalidHeaderException;
import uk.nhs.nhsbsa.exemptions.validator.UuidValidator;

/** Web request interceptor to validate correlation-id header contents. */
@Component
@Order(2)
public class CorrelationIdValidationInterceptor implements HandlerInterceptor {

  public CorrelationIdValidationInterceptor() {}

  /** Check whether format of correlation-id is correct in request header. */
  public boolean preHandle(
      HttpServletRequest request, HttpServletResponse response, Object object) {
    String correlationId = request.getHeader(HEADER_PARAM_CORRELATION_ID);
    MDC.put("req.session.locator", correlationId);
    if (null == correlationId || UuidValidator.isValidUuid(correlationId)) {
      return true;
    } else {
      throw new InvalidHeaderException(HEADER_PARAM_CORRELATION_ID);
    }
  }

  public void afterCompletion(
      HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
    MDC.remove("req.session.locator");
  }
}
