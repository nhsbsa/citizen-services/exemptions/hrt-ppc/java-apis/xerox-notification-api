package uk.nhs.nhsbsa.xerox.notification.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.MappedInterceptor;
import uk.nhs.nhsbsa.exemptions.interceptor.UserIdValidationInterceptor;
import uk.nhs.nhsbsa.xerox.notification.interceptor.CorrelationIdValidationInterceptor;
import uk.nhs.nhsbsa.xerox.notification.interceptor.RunDateValidationInterceptor;
import uk.nhs.nhsbsa.xerox.notification.interceptor.ServiceNameValidationInterceptor;
import uk.nhs.nhsbsa.xerox.notification.interceptor.XeroxNotificationApplicationRequestContextInterceptor;

/**
 * Web configuration for interceptors. The order the interceptors are configured matters and this is
 * why the interceptor for {@link XeroxNotificationApplicationRequestContextInterceptor} is
 * configured towards the end, after the validation for userId happens.
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

  @Autowired CorrelationIdValidationInterceptor correlationIdValidationInterceptor;
  @Autowired UserIdValidationInterceptor userIdValidationInterceptor;
  @Autowired ServiceNameValidationInterceptor serviceNameValidationInterceptor;

  @Autowired
  XeroxNotificationApplicationRequestContextInterceptor
      xeroxNotificationApplicationRequestContextInterceptor;

  @Autowired RunDateValidationInterceptor runDateValidationInterceptor;

  /** Maps {@link CorrelationIdValidationInterceptor}. */
  @Bean
  public MappedInterceptor mapCorrelationIdValidationInterceptor() {
    return new MappedInterceptor(
        new String[] {
          "/v1/xerox-notifications/letter",
        },
        new String[] {"/v3/api-docs"},
        correlationIdValidationInterceptor);
  }

  /** Maps {@link UserIdValidationInterceptor}. */
  @Bean
  public MappedInterceptor mapUserIdValidationInterceptor() {
    return new MappedInterceptor(
        new String[] {
          "/v1/xerox-notifications/letter",
          "/v1/xerox-notifications/letter/*",
          "/v1/xerox-notifications/templates/*",
          "/v1/xerox-notifications/templates/search/findByEffectiveOnDate"
        },
        new String[] {"/v3/api-docs"},
        userIdValidationInterceptor);
  }

  /** Maps {@link ServiceNameValidationInterceptor}. */
  @Bean
  public MappedInterceptor mapServiceNameValidationInterceptor() {
    return new MappedInterceptor(
        new String[] {
          "/v1/xerox-notifications/letter",
        },
        new String[] {"/v3/api-docs"},
        serviceNameValidationInterceptor);
  }

  /** Maps {@link XeroxNotificationApplicationRequestContextInterceptor}. */
  @Bean
  public MappedInterceptor mapXeroxNotificationApplicationRequestContextInterceptor() {
    return new MappedInterceptor(
        new String[] {"/v1/xerox-notifications/letter", "/v1/xerox-notifications/letter/*"},
        new String[] {"/v3/api-docs"},
        xeroxNotificationApplicationRequestContextInterceptor);
  }

  /** Maps {@link RunDateValidationInterceptor}. */
  @Bean
  public MappedInterceptor mapRunDateValidationInterceptor() {
    return new MappedInterceptor(
        new String[] {"/v1/xerox-notifications/templates/search/findByEffectiveOnDate"},
        new String[] {"/v3/api-docs"},
        runDateValidationInterceptor);
  }
}
