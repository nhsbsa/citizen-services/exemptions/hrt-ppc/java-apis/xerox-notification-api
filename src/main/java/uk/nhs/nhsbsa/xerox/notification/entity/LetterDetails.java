package uk.nhs.nhsbsa.xerox.notification.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Table;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.format.annotation.DateTimeFormat;
import uk.nhs.nhsbsa.redact.format.RedactFormatter;
import uk.nhs.nhsbsa.xerox.notification.model.StatusType;
import uk.nhs.nhsbsa.xerox.notification.validators.PersonalisationDetails;

/** Letter details entity. */
@RestResource
@Entity
@Table(name = "letter_details")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Valid
public class LetterDetails extends VersionedEntity {

  @NotNull(message = "{letterTemplateId.not-null}")
  @Column(name = "letter_template_id", updatable = false)
  private UUID templateId;

  @NotNull(message = "{transactionDate.not-null}")
  @Column(name = "transaction_dt")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
  private LocalDate transactionDate;

  @NotNull(message = "{personalisationDetails.not-null}")
  @PersonalisationDetails
  @Column(name = "personalisation_details", columnDefinition = "json", updatable = false)
  @JdbcTypeCode(SqlTypes.JSON)
  private Map<String, String> personalisation;

  @NotEmpty(message = "{reference.not-null}")
  @Size(max = 100, message = "{reference.size}")
  @Column(name = "reference")
  private String reference;

  @Column(name = "correlation_id", updatable = false)
  private UUID correlationId;

  @Size(max = 500, message = "{fileName.size}")
  @Column(name = "file_name")
  private String fileName;

  @Setter(AccessLevel.NONE)
  @NotNull(message = "{status.not-null}")
  @Column(name = "status")
  @Enumerated(EnumType.STRING)
  private StatusType status;

  @Size(max = 500, message = "{failureReason.size}")
  @Column(name = "failure_reason")
  private String failureReason;

  @Setter(AccessLevel.NONE)
  @NotNull(message = "{statusChangeAt.not-null}")
  @Column(name = "status_change_at")
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  @JsonProperty(value = "statusUpdateAt", access = Access.READ_ONLY)
  private LocalDateTime statusChangeAt;

  /**
   * Set status and status change datetime when status different from current status.
   *
   * @param status current status
   */
  public void setStatus(StatusType status) {
    if (this.status != status) {
      this.status = status;
      this.statusChangeAt = LocalDateTime.now();
    }
  }

  @Override
  public String toString() {
    return new RedactFormatter()
        .append("LetterDetails [")
        .append("id=")
        .append(getId())
        .append(", meta=")
        .append(getMeta())
        .append(", transactionDate=")
        .append(transactionDate)
        .append(", personalisation=")
        .redact(personalisation)
        .append(", reference=")
        .append(reference)
        .append(", correlationId=")
        .append(correlationId)
        .append(", fileName=")
        .append(fileName)
        .append(", status=")
        .append(status)
        .append(", failureReason=")
        .append(failureReason)
        .append(", statusChangeAt=")
        .append(statusChangeAt)
        .append("]")
        .format();
  }
}
