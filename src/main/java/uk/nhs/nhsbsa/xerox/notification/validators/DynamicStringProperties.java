package uk.nhs.nhsbsa.xerox.notification.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/** Helps load values from the system environment. */
@Component
public class DynamicStringProperties {
  private static Environment environment;

  @Autowired
  public void setEnvironment(Environment environment) {
    DynamicStringProperties.environment = environment;
  }

  public static String getServiceName(String propertyName) {
    return DynamicStringProperties.environment.getProperty(propertyName);
  }
}
