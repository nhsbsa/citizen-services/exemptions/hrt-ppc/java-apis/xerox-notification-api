package uk.nhs.nhsbsa.xerox.notification.entity;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Version;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import uk.nhs.nhsbsa.xerox.notification.model.Meta;

/** Domain object for a versioned entity. */
@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public abstract class VersionedEntity extends BaseEntity {

  @Version
  @Column(name = "version_number")
  private Integer versionNumber;

  @Column(name = "updated_at")
  @LastModifiedDate
  @JsonIgnore
  private LocalDateTime updatedTimestamp;

  @Column(name = "updated_by")
  @LastModifiedBy
  @JsonIgnore
  private String updatedBy;

  /** Get Xerox Meta. */
  @JsonGetter("_meta")
  public Meta getMeta() {
    return Meta.builder()
        .createdTimestamp(this.getCreatedTimestamp())
        .createdBy(this.getCreatedBy())
        .updatedTimestamp(this.getUpdatedTimestamp())
        .updatedBy(this.getUpdatedBy())
        .build();
  }
}
