package uk.nhs.nhsbsa.xerox.notification.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.format.annotation.DateTimeFormat;
import uk.nhs.nhsbsa.redact.format.RedactFormatter;
import uk.nhs.nhsbsa.xerox.notification.validators.DynamicPattern;

/** Letter template entity. */
@RestResource
@Entity
@Table(name = "letter_template")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class LetterTemplate extends VersionedEntity {

  @NotNull(message = "{version.not-null}")
  @Column(name = "version")
  private Integer version;

  @NotNull(message = "{serviceName.not-null}")
  @DynamicPattern(patternKey = "xerox-services", message = "{serviceName.not-valid}")
  @Column(name = "service_name")
  private String serviceName;

  @NotNull(message = "{templateName.not-null}")
  @Column(name = "template_name")
  private String templateName;

  @NotNull(message = "{effectiveFrom.not-null}")
  @Column(name = "effective_from")
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private LocalDate effectiveFrom;

  @Column(name = "effective_to")
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private LocalDate effectiveTo;

  @NotNull(message = "{template.not-null}")
  @Column(name = "template")
  @JsonProperty("xeroxTemplate")
  private String template;

  @Override
  public String toString() {
    return new RedactFormatter()
        .append("LetterTemplate [")
        .append("id=")
        .append(getId())
        .append(", meta=")
        .append(getMeta())
        .append(", version=")
        .append(version)
        .append(", serviceName=")
        .append(serviceName)
        .append(", templateName=")
        .append(templateName)
        .append(", effectiveFrom=")
        .append(effectiveFrom)
        .append(", effectiveTo=")
        .append(effectiveTo)
        .append(", template=")
        .append(template)
        .append("]")
        .format();
  }
}
