package uk.nhs.nhsbsa.xerox.notification.model;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;
import uk.nhs.nhsbsa.exemptions.models.ApplicationRequestContext;

/** Xerox Notification request context object {@link XeroxNotificationApplicationRequestContext}. */
@Data
@EqualsAndHashCode(callSuper = false)
@RequestScope
@Component
public class XeroxNotificationApplicationRequestContext extends ApplicationRequestContext {
  @Getter(AccessLevel.NONE)
  private UUID correlationId;

  private LocalDate transactionDate;
  private StatusType status;

  public Optional<UUID> getCorrelationId() {
    return Optional.ofNullable(correlationId);
  }
}
