package uk.nhs.nhsbsa.xerox.notification.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;

/** Definitions of OpenAPI beans to generate Swagger documentation. */
@Configuration
@Import(BeanValidatorPluginsConfiguration.class) // enable documentation of JSR-305 constraints
@Slf4j
public class ApiDocumentationConfig {

  @Value("${app.version:1.0}")
  // use APP_VERSION env variable if available, otherwise default to 1.0
  private String appVersion;

  /**
   * Open api documentation open api.
   *
   * @return the open api
   */
  @Bean
  public OpenAPI openApiDocumentation() {
    return new OpenAPI()
        .components(new Components())
        .addServersItem(new Server().url("http://localhost:8160"))
        .info(
            new Info()
                .title("Xerox Notification service")
                .description("Responsible for the persistence and retrieval of xerox notifications")
                .version(appVersion)
                .contact(
                    new Contact()
                        .name("NHS Business Services Authority")
                        .url("https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc"))
                .license(
                    new License()
                        .name("Apache 2.0")
                        .url("https://opensource.org/licenses/Apache-2.0")));
  }
}
