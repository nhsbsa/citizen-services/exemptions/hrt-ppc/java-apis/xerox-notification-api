package uk.nhs.nhsbsa.xerox.notification.repository;

import java.util.UUID;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterDetails;

/** JPA repository for {@link LetterDetails} entity. */
@JaversSpringDataAuditable
@RepositoryRestResource(collectionResourceRel = "letter", path = "letter")
public interface LetterDetailsRepository
    extends JpaRepository<LetterDetails, UUID>, QuerydslPredicateExecutor<LetterDetails> {}
