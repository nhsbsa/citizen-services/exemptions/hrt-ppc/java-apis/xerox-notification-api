package uk.nhs.nhsbsa.xerox.notification.validators;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/** Custom dynamic string validator. */
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
@Retention(RUNTIME)
@Constraint(validatedBy = DynamicStringPatternValidator.class)
@Documented
public @interface DynamicPattern {

  /** Set the key value in application.yml. */
  String patternKey();

  /** Sets the default error message. */
  String message() default "Not valid";

  /** setter for the annotation group. */
  Class<?>[] groups() default {};

  /** setter for the annotation payload. */
  Class<? extends Payload>[] payload() default {};
}
