package uk.nhs.nhsbsa.xerox.notification.validators;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/** Custom map validator. */
@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = PersonalisationDetailsValidator.class)
@Documented
public @interface PersonalisationDetails {

  /** Sets the default error message. */
  String message() default "";

  /** Setter for the annotation group. */
  Class<?>[] groups() default {};

  /** Setter for the annotation payload. */
  Class<? extends Payload>[] payload() default {};
}
