package uk.nhs.nhsbsa.xerox.notification.model;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/** The type Constants. */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Constants {

  public static final String XEROX_NOTIFICATION_API_PATH_URI = "/v1/xerox-notifications/";
  public static final String XEROX_NOTIFICATION_API_SEARCH_BY_EFFECTIVE_ON_DATE_URI =
      "/v1/xerox-notifications/templates/search/findByEffectiveOnDate";
  public static final String XEROX_NOTIFICATION_API_CREATION_LETTER_URI =
      "/v1/xerox-notifications/letter";
  public static final String QUERY_PARAM_RUN_DATE = "runDate";
  public static final String HEADER_PARAM_SERVICE_NAME = "service-name";
  public static final String VALIDATOR_PATTERN_FOR_SERVICE_NAME = "xerox-services";
  public static final String HEADER_PARAM_CORRELATION_ID = "correlation-id";
  public static final String QUERY_PARAM_TEMPLATE_ID = "templateId";
  public static final String QUERY_PARAM_STATUS = "status";
  public static final String HEADER_PARAM_USER_ID = "user-id";
  public static final String POSTCODE_REGEX =
      "^(^$)|(GIR ?0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]([0-9ABEHMNPRV-Y])?)"
          + "|[0-9][A-HJKPS-UW])\\ ?[0-9][ABD-HJLNP-UW-Z]{2})$";
}
