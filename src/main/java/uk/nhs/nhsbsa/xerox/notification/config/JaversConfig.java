package uk.nhs.nhsbsa.xerox.notification.config;

import java.util.Collections;
import java.util.Map;
import org.javers.spring.auditable.AuthorProvider;
import org.javers.spring.auditable.CommitPropertiesProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterDetails;
import uk.nhs.nhsbsa.xerox.notification.javers.UserIdAuthorProvider;

/** Javers auditing configuration. */
@Configuration
public class JaversConfig {

  @Autowired UserIdAuthorProvider userIdAuthorProvider;

  @Bean
  public AuthorProvider authorProvider() {
    return userIdAuthorProvider;
  }

  /**
   * Custom commit properties provider to store TODO.
   *
   * @return the commit properties provider.
   */
  @Bean
  public CommitPropertiesProvider commitPropertiesProvider() {
    return new CommitPropertiesProvider() {
      @Override
      public Map<String, String> provideForCommittedObject(Object domainObject) {
        if (domainObject instanceof LetterDetails letterDetails) {
          return Map.of(
              "letterDetailsId",
              letterDetails.getId().toString(),
              "templateId",
              letterDetails.getTemplateId().toString(),
              "reference",
              letterDetails.getReference().toString());
        }

        return Collections.emptyMap();
      }
    };
  }
}
