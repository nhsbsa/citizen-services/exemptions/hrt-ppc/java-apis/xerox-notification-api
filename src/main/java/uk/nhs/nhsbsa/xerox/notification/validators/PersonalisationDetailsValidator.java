package uk.nhs.nhsbsa.xerox.notification.validators;

import static uk.nhs.nhsbsa.xerox.notification.model.Constants.POSTCODE_REGEX;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.stream.Collectors;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

/** Validator of {@link PersonalisationDetails}. */
public class PersonalisationDetailsValidator
    implements ConstraintValidator<PersonalisationDetails, Map<String, String>> {

  /** Load validation message from ValidationMessages.properties. */
  private static ReloadableResourceBundleMessageSource messageSource;

  static {
    messageSource = new ReloadableResourceBundleMessageSource();
    messageSource.setBasename("classpath:/ValidationMessages");
  }

  private String valueEmptyMessage;
  private String addressLineLessThan3Message;
  private String withoutPostCodeMessage;

  @Override
  public void initialize(PersonalisationDetails constraintAnnotation) {
    addressLineLessThan3Message =
        messageSource.getMessage(
            "personalisation.address-less-than-tree", (Object[]) null, Locale.getDefault());
    valueEmptyMessage =
        messageSource.getMessage("personalisation.empty", (Object[]) null, Locale.getDefault());
    withoutPostCodeMessage =
        messageSource.getMessage(
            "personalisation.address-no-postcode", (Object[]) null, Locale.getDefault());
  }

  /**
   * Valid address information of the personalisation.
   *
   * @param value object to validate
   * @param context context in which the constraint is evaluated
   * @return true if the value contains address$Num and $num must be between 1 and 7 and last one
   *     must be UK postcode and num of addressLine cannot less than 3 and any address line greater
   *     than 7 will be ignored
   */
  @Override
  public boolean isValid(Map<String, String> value, ConstraintValidatorContext context) {
    if (value == null) {
      return true;
    }
    context.disableDefaultConstraintViolation();
    TreeMap<String, String> addressMap =
        value.entrySet().stream()
            .filter(line -> line.getKey().matches("^addressLine[1-7]$"))
            .collect(
                Collectors.toMap(Entry::getKey, Entry::getValue, (e1, e2) -> e1, TreeMap::new));

    if (value.isEmpty()) {
      context.buildConstraintViolationWithTemplate(valueEmptyMessage).addConstraintViolation();
    } else if (!value.isEmpty() && addressMap.size() < 3) {
      context
          .buildConstraintViolationWithTemplate(addressLineLessThan3Message)
          .addConstraintViolation();
    } else if (!value.isEmpty() && !addressMap.lastEntry().getValue().matches(POSTCODE_REGEX)) {
      context.buildConstraintViolationWithTemplate(withoutPostCodeMessage).addConstraintViolation();
    } else {
      return true;
    }
    return false;
  }
}
