package uk.nhs.nhsbsa.xerox.notification.interceptor;

import static uk.nhs.nhsbsa.xerox.notification.model.Constants.HEADER_PARAM_CORRELATION_ID;
import static uk.nhs.nhsbsa.xerox.notification.model.Constants.HEADER_PARAM_USER_ID;
import static uk.nhs.nhsbsa.xerox.notification.model.Constants.XEROX_NOTIFICATION_API_CREATION_LETTER_URI;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import uk.nhs.nhsbsa.xerox.notification.model.StatusType;
import uk.nhs.nhsbsa.xerox.notification.model.XeroxNotificationApplicationRequestContext;

/** Web request interceptor to populate the creation request context. */
@Slf4j
@Component
@Order(value = 5)
public class XeroxNotificationApplicationRequestContextInterceptor implements HandlerInterceptor {

  @Autowired
  private XeroxNotificationApplicationRequestContext xeroxNotificationApplicationRequestContext;

  /**
   * If post request happened for /v1/xerox-notifications/letter, will add default value for status,
   * statusChangeAt, transactionDate and pass userId and correlationId to
   * xeroxNotificationApplicationRequestContext. If patch request happened for
   * /v1/xerox-notifications/letter/*, will only pass userId to
   * xeroxNotificationApplicationRequestContext.
   *
   * @param request Http Servlet Request
   * @param response Http Servlet Response
   * @param object handler
   * @return always return true
   */
  @Override
  public boolean preHandle(
      HttpServletRequest request, HttpServletResponse response, Object object) {
    String httpMethod = request.getMethod();
    String requestUri = request.getRequestURI();
    if ((httpMethod.equalsIgnoreCase(HttpMethod.POST.name())
        && XEROX_NOTIFICATION_API_CREATION_LETTER_URI.equalsIgnoreCase(requestUri))) {
      handleContextForCreateLetterDetails(request);
    } else if (requestUri.startsWith(XEROX_NOTIFICATION_API_CREATION_LETTER_URI + "/")) {
      if (!httpMethod.equalsIgnoreCase(HttpMethod.PATCH.name())) {
        return false;
      } else {
        xeroxNotificationApplicationRequestContext.setUser(request.getHeader(HEADER_PARAM_USER_ID));
      }
    }
    return true;
  }

  private void handleContextForCreateLetterDetails(HttpServletRequest request) {
    xeroxNotificationApplicationRequestContext.setUser(request.getHeader(HEADER_PARAM_USER_ID));
    xeroxNotificationApplicationRequestContext.setStatus(StatusType.PENDING);
    xeroxNotificationApplicationRequestContext.setTransactionDate(LocalDate.now());
    UUID correlationId = null;
    try {
      correlationId = UUID.fromString(request.getHeader(HEADER_PARAM_CORRELATION_ID));
    } catch (Exception exception) {
      log.info(
          "UUID parse exception handling for {} with message {}",
          exception.getClass(),
          exception.getMessage());
    }
    xeroxNotificationApplicationRequestContext.setCorrelationId(correlationId);
  }
}
