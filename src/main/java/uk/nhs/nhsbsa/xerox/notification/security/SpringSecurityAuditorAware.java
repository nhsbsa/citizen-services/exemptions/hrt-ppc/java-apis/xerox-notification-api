package uk.nhs.nhsbsa.xerox.notification.security;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;
import uk.nhs.nhsbsa.xerox.notification.model.XeroxNotificationApplicationRequestContext;

/** Populates user from {@link XeroxNotificationApplicationRequestContext} to audit aware. */
@Component
public class SpringSecurityAuditorAware implements AuditorAware<String> {

  @Autowired XeroxNotificationApplicationRequestContext xeroxNotificationApplicationRequestContext;

  @Override
  public Optional<String> getCurrentAuditor() {
    return Optional.of(xeroxNotificationApplicationRequestContext.getUser());
  }
}
