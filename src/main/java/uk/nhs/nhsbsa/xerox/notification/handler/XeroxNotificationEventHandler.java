package uk.nhs.nhsbsa.xerox.notification.handler;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterDetails;
import uk.nhs.nhsbsa.xerox.notification.model.XeroxNotificationApplicationRequestContext;

/** Xerox Notification Event Handler class. */
@Component
@RepositoryEventHandler
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class XeroxNotificationEventHandler {

  @Autowired
  private XeroxNotificationApplicationRequestContext xeroxNotificationApplicationRequestContext;

  /** Event to handle {@link LetterDetails} before creation. */
  @HandleBeforeCreate
  public void handleLetterDetailsBeforeCreate(LetterDetails letterDetails) {
    letterDetails.setCorrelationId(
        xeroxNotificationApplicationRequestContext.getCorrelationId().orElse(null));
    letterDetails.setTransactionDate(
        xeroxNotificationApplicationRequestContext.getTransactionDate());
    letterDetails.setStatus(xeroxNotificationApplicationRequestContext.getStatus());
  }

  /** Event to handle {@link LetterDetails} after creation. */
  @HandleAfterCreate
  public void handleLetterDetailsAfterCreate(LetterDetails letterDetails) {
    log.info("LetterDetails created [{}]", letterDetails.toString());
  }

  /** Event to handle {@link LetterDetails} after update. */
  @HandleAfterSave
  public void handleLetterDetailsAfterUpdate(LetterDetails letterDetails) {
    log.info("LetterDetails updated [{}]", letterDetails.toString());
  }
}
