package uk.nhs.nhsbsa.xerox.notification.config;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.RepresentationModelProcessor;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterDetails;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterTemplate;
import uk.nhs.nhsbsa.xerox.notification.model.Constants;

/** Configuration to expose id field and set base path in response. */
@Configuration
public class RestConfig implements RepositoryRestConfigurer {

  @Autowired HttpServletRequest httpServletRequest;

  @Override
  public void configureRepositoryRestConfiguration(
      RepositoryRestConfiguration config, CorsRegistry corsRegistry) {
    config.setBasePath(Constants.XEROX_NOTIFICATION_API_PATH_URI);
    config.exposeIdsFor(LetterTemplate.class);
    config.exposeIdsFor(LetterDetails.class);
  }

  /** RepresentationModelProcessor implementation to add links to letter details resource. */
  @Bean
  public RepresentationModelProcessor<EntityModel<LetterDetails>> letterDetailsProcessor() {
    // Replacing below with lambda expression causes 'cannot be cast' exception
    return new RepresentationModelProcessor<EntityModel<LetterDetails>>() {
      @Override
      public EntityModel<LetterDetails> process(EntityModel<LetterDetails> resource) {
        if (HttpMethod.POST.name().equalsIgnoreCase(httpServletRequest.getMethod())
            || HttpMethod.GET.name().equalsIgnoreCase(httpServletRequest.getMethod())) {
          resource
              .getLink("letterDetails")
              .map(Link::getHref)
              .ifPresent(
                  href -> {
                    resource.removeLinks();
                    resource.add(Link.of(href, "self"));
                    resource.add(Link.of(href, "letter"));
                    resource.add(
                        Link.of(
                            href.replace(
                                "letter/" + resource.getContent().getId().toString(),
                                "templates/" + resource.getContent().getTemplateId().toString()),
                            "template"));
                  });
        }
        return resource;
      }
    };
  }

  /** RepresentationModelProcessor implementation to add links to letter template resource. */
  @Bean
  public RepresentationModelProcessor<EntityModel<LetterTemplate>> letterTemplateProcessor() {
    // Replacing below with lambda expression causes 'cannot be cast' exception
    return new RepresentationModelProcessor<EntityModel<LetterTemplate>>() {
      @Override
      public EntityModel<LetterTemplate> process(EntityModel<LetterTemplate> resource) {
        if (HttpMethod.GET.name().equalsIgnoreCase(httpServletRequest.getMethod())) {
          resource
              .getLink("letterTemplate")
              .map(Link::getHref)
              .ifPresent(
                  href -> {
                    resource.removeLinks();
                    resource.add(Link.of(href, "self"));
                    resource.add(Link.of(href, "template"));
                  });
        }
        return resource;
      }
    };
  }
}
