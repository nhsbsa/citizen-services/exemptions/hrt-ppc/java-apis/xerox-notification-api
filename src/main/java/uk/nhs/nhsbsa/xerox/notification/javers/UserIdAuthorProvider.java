package uk.nhs.nhsbsa.xerox.notification.javers;

import org.javers.spring.auditable.AuthorProvider;
import org.javers.spring.auditable.MockAuthorProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uk.nhs.nhsbsa.xerox.notification.model.XeroxNotificationApplicationRequestContext;

/** Custom author provider using header user-id value. */
@Component
public class UserIdAuthorProvider implements AuthorProvider {

  @Autowired
  private XeroxNotificationApplicationRequestContext xeroxNotificationApplicationRequestContext;

  @Override
  public String provide() {
    String user = xeroxNotificationApplicationRequestContext.getUser();
    if (user != null) {
      return user;
    }
    return new MockAuthorProvider().provide();
  }
}
