package uk.nhs.nhsbsa.xerox.notification.advice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import uk.nhs.nhsbsa.exemptions.exception.InvalidHeaderException;
import uk.nhs.nhsbsa.exemptions.exception.InvalidQueryParameterException;
import uk.nhs.nhsbsa.exemptions.models.ErrorResponse;

/** Controller advice to handle exceptions and generate error response. */
@Slf4j
@RestControllerAdvice
public class ExceptionHandlerControllerAdvice {

  private static final String BAD_REQUEST_MESSAGE = "Bad Request";

  /**
   * Handler method for {@link InvalidHeaderException}, {@link InvalidQueryParameterException} and
   * {@link IllegalArgumentException} exception.
   *
   * @param exception the exception
   * @return {@link ErrorResponse} the error response
   */
  @ExceptionHandler({
    InvalidHeaderException.class,
    InvalidQueryParameterException.class,
    IllegalArgumentException.class
  })
  public ResponseEntity<ErrorResponse> handleInvalidHeaderOrParamAndIllegalArgumentException(
      final Exception exception) {

    log.warn(
        "Exception handling for {} with message {}", exception.getClass(), exception.getMessage());

    ErrorResponse errorResponse =
        ErrorResponse.builder()
            .status(HttpStatus.BAD_REQUEST.value())
            .message(BAD_REQUEST_MESSAGE)
            .build();
    return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
  }

  /**
   * handler method for {@link DataIntegrityViolationException } exception.
   *
   * @param dataIntegrityViolationException the DataIntegrityViolationException
   * @return {@link ErrorResponse} the error response
   */
  @ExceptionHandler(DataIntegrityViolationException.class)
  public ResponseEntity<ErrorResponse> handleDataIntegrityViolationException(
      final DataIntegrityViolationException dataIntegrityViolationException) {

    log.warn(
        "Exception handling for {} with message {}",
        dataIntegrityViolationException.getClass(),
        dataIntegrityViolationException.getMessage());

    ErrorResponse errorResponse =
        ErrorResponse.builder()
            .status(HttpStatus.CONFLICT.value())
            .message(
                dataIntegrityViolationException
                    .getCause()
                    .getCause()
                    .getCause()
                    .getLocalizedMessage())
            .build();
    return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
  }
}
