package uk.nhs.nhsbsa.xerox.notification.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import uk.nhs.nhsbsa.errorhandler.ErrorHandlerConfiguration;

/** Configuration for error handling. */
@Configuration
@Import({ErrorHandlerConfiguration.class})
public class ErrorHandlingConfig {}
