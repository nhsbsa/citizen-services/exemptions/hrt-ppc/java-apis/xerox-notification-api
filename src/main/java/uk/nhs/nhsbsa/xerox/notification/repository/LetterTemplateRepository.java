package uk.nhs.nhsbsa.xerox.notification.repository;

import java.time.LocalDate;
import java.util.UUID;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterTemplate;

/** JPA repository for {@link LetterTemplate} entity. */
@JaversSpringDataAuditable
@RepositoryRestResource(collectionResourceRel = "templates", path = "templates")
public interface LetterTemplateRepository extends JpaRepository<LetterTemplate, UUID> {

  /**
   * To retrieve LetterTemplate by using runDate as parameter. This will return the letter templates
   * filtered by run date is equal and after effective from and at meantime effective to is null or
   * runDate is before or equal to runDate.
   *
   * @param runDate the query date
   * @param pageable pagination information
   * @return Paged LetterTemplate
   */
  @Query(
      nativeQuery = true,
      value =
          """
                     SELECT * FROM letter_template t
                     WHERE :runDate >= t.effective_from
                     AND ( t.effective_to is null
                     OR :runDate <= t.effective_to)""")
  Page<LetterTemplate> findByEffectiveOnDate(
      @Param("runDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate runDate,
      Pageable pageable);
}
