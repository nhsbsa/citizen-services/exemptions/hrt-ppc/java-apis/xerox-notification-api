package uk.nhs.nhsbsa.xerox.notification.model;

import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Data;

/** Metadata object to store created and updated details. * */
@Builder
@Data
public class Meta {
  private LocalDateTime createdTimestamp;
  private String createdBy;
  private LocalDateTime updatedTimestamp;
  private String updatedBy;
}
