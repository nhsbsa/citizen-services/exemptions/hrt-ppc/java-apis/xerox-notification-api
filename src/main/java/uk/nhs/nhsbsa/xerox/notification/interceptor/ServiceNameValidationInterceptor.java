package uk.nhs.nhsbsa.xerox.notification.interceptor;

import static uk.nhs.nhsbsa.xerox.notification.model.Constants.HEADER_PARAM_SERVICE_NAME;
import static uk.nhs.nhsbsa.xerox.notification.model.Constants.VALIDATOR_PATTERN_FOR_SERVICE_NAME;
import static uk.nhs.nhsbsa.xerox.notification.validators.DynamicStringPatternValidator.getValidStringList;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import uk.nhs.nhsbsa.exemptions.exception.InvalidHeaderException;

/** Web request interceptor to validate that a service name is present in the headers. */
@Component
@Order(4)
public class ServiceNameValidationInterceptor implements HandlerInterceptor {
  /** Check whether service-name existed in request header. */
  public boolean preHandle(
      HttpServletRequest request, HttpServletResponse response, Object object) {
    if (request.getMethod().equalsIgnoreCase(HttpMethod.POST.name())) {
      List<String> validStringList = getValidStringList(VALIDATOR_PATTERN_FOR_SERVICE_NAME);
      String serviceName = request.getHeader(HEADER_PARAM_SERVICE_NAME);
      if (!validStringList.contains(serviceName)) {
        throw new InvalidHeaderException(HEADER_PARAM_SERVICE_NAME);
      }
    }
    return true;
  }
}
