package uk.nhs.nhsbsa.xerox.notification.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Validator of {@link DynamicPattern}. Checks whether the value of a string field exists in valid
 * string options set in the environment
 */
public class DynamicStringPatternValidator implements ConstraintValidator<DynamicPattern, String> {

  private List<String> validStringList;

  @Override
  public void initialize(DynamicPattern constraintAnnotation) {
    validStringList = getValidStringList(constraintAnnotation.patternKey());
  }

  /** Read valid string list from configure file. */
  public static List<String> getValidStringList(String patternKey) {
    String parameterValue = DynamicStringProperties.getServiceName(patternKey);
    return parameterValue == null
        ? new ArrayList<>()
        : Arrays.stream(parameterValue.split(",")).map(String::trim).collect(Collectors.toList());
  }

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    if (value == null || value.equals("") || validStringList == null) {
      return true;
    }
    return validStringList.contains(value);
  }
}
