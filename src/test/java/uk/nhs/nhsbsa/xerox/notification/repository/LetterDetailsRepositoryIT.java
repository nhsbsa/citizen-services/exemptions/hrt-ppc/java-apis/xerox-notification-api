package uk.nhs.nhsbsa.xerox.notification.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static uk.nhs.nhsbsa.exemptions.utilities.ComparisonUtil.recursiveComparisonConfiguration;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterDetailsTestDataFactory.aValidLetterDetails;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterDetailsTestDataFactory.aValidLetterDetailsWithStatus;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterTemplateTestDataFactory.aValidLetterTemplateWithEffectiveFromEffectiveTo;

import com.querydsl.core.types.Predicate;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockReset;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.dao.DataIntegrityViolationException;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterDetails;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterTemplate;
import uk.nhs.nhsbsa.xerox.notification.entity.QLetterDetails;
import uk.nhs.nhsbsa.xerox.notification.model.StatusType;
import uk.nhs.nhsbsa.xerox.notification.validators.DynamicStringProperties;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LetterDetailsRepositoryIT {

  @Autowired LetterDetailsRepository letterDetailsRepository;
  @Autowired LetterTemplateRepository letterTemplateRepository;

  @SpyBean(reset = MockReset.AFTER)
  DynamicStringProperties dynamicStringProperties;

  @BeforeEach
  void setup() {
    letterDetailsRepository.deleteAll();
    letterDetailsRepository.flush();
    letterTemplateRepository.deleteAll();
    letterTemplateRepository.flush();
  }

  @Test
  void shouldPersistLetterDetailsWhenValidDetailsProvided() {
    // given
    LetterDetails letterDetails = aValidLetterDetails();
    letterDetails.setTemplateId(createTemplate());

    // when
    LetterDetails result = letterDetailsRepository.save(letterDetails);

    // then
    assertThat(result).isNotNull();
    assertThat(result)
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(letterDetails);
    assertThat(result.getCreatedTimestamp()).isEqualTo(result.getUpdatedTimestamp());
  }

  @Test
  void shouldFindAllLetterDetailsWhenFetchAllRequested() {
    // given
    LetterDetails letterDetails = aValidLetterDetails();
    letterDetails.setTemplateId(createTemplate());
    letterDetails.setTransactionDate(LocalDate.now());
    letterDetailsRepository.save(letterDetails);

    // when
    List<LetterDetails> result = letterDetailsRepository.findAll();

    // then
    assertThat(result).hasSize(1);
    assertThat(result.get(0))
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(letterDetails);
  }

  @Test
  void shouldFindLetterDetailsWhenFindByLetterDetailsIdRequested() {
    // given
    LetterDetails letterDetails = aValidLetterDetails();
    letterDetails.setTemplateId(createTemplate());
    LetterDetails savedletterDetails = letterDetailsRepository.save(letterDetails);

    // when
    Optional<LetterDetails> result = letterDetailsRepository.findById(savedletterDetails.getId());

    // then
    assertThat(result).isPresent();
    assertThat(result.get())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(letterDetails);
  }

  @Test
  void shouldNotFindLetterDetailsWhenFindByLetterDetailsIdNotFoundInDatabase() {
    // given
    // when
    Optional<LetterDetails> result = letterDetailsRepository.findById(UUID.randomUUID());

    // then
    assertThat(result).isEmpty();
  }

  @Test
  void shouldDeleteLetterDetailsWhenValidLetterDetailsIsProvided() {
    // given
    LetterDetails letterDetails = aValidLetterDetails();
    letterDetails.setTemplateId(createTemplate());
    LetterDetails savedLetterDetails = letterDetailsRepository.save(letterDetails);

    // when
    letterDetailsRepository.deleteById(savedLetterDetails.getId());

    // then
    Optional<LetterDetails> result = letterDetailsRepository.findById(savedLetterDetails.getId());
    assertThat(result).isEmpty();
  }

  @Test
  void shouldFindLetterDetailsWhenValidStatusIsProvided() {
    // given
    UUID templateId = createTemplateWithDiffDaysEffectiveFrom(201);
    LetterDetails letterDetails = aValidLetterDetailsWithStatus(StatusType.PENDING);
    letterDetails.setTemplateId(templateId);
    letterDetailsRepository.save(letterDetails);
    templateId = createTemplateWithDiffDaysEffectiveFrom(204);
    letterDetails = aValidLetterDetailsWithStatus(StatusType.SENT);
    letterDetails.setTemplateId(templateId);
    letterDetailsRepository.save(letterDetails);
    Predicate predicate = QLetterDetails.letterDetails.status.eq(StatusType.PENDING);

    // when
    Iterable<LetterDetails> result = letterDetailsRepository.findAll(predicate);

    // then
    assertThat(result).hasSize(1);
    assertThat(result.iterator().next().getStatus()).isEqualTo(StatusType.PENDING);
  }

  @Test
  void shouldFindLetterDetailsWhenValidTemplateIdIsProvided() {
    // given
    UUID templateId = createTemplateWithDiffDaysEffectiveFrom(201);
    LetterDetails letterDetails = aValidLetterDetailsWithStatus(StatusType.PENDING);
    letterDetails.setTemplateId(templateId);
    letterDetailsRepository.save(letterDetails);
    templateId = createTemplateWithDiffDaysEffectiveFrom(204);
    letterDetails = aValidLetterDetailsWithStatus(StatusType.SENT);
    letterDetails.setTemplateId(templateId);
    letterDetailsRepository.save(letterDetails);
    Predicate predicate = QLetterDetails.letterDetails.templateId.eq(templateId);

    // when
    Iterable<LetterDetails> result = letterDetailsRepository.findAll(predicate);

    // then
    assertThat(result).hasSize(1);
    assertThat(result.iterator().next().getTemplateId()).isEqualTo(templateId);
  }

  @Test
  void shouldFindLetterDetailsWhenValidTemplateIdAndStatusIsProvided() {
    // given
    UUID templateId = createTemplateWithDiffDaysEffectiveFrom(201);
    LetterDetails letterDetails = aValidLetterDetailsWithStatus(StatusType.PENDING);
    letterDetails.setTemplateId(templateId);
    letterDetailsRepository.save(letterDetails);
    templateId = createTemplateWithDiffDaysEffectiveFrom(204);
    letterDetails = aValidLetterDetailsWithStatus(StatusType.SENT);
    letterDetails.setTemplateId(templateId);
    letterDetailsRepository.save(letterDetails);
    Predicate predicate =
        QLetterDetails.letterDetails
            .templateId
            .eq(templateId)
            .and(QLetterDetails.letterDetails.status.eq(StatusType.SENT));

    // when
    Iterable<LetterDetails> result = letterDetailsRepository.findAll(predicate);

    // then
    assertThat(result).hasSize(1);
    LetterDetails citizenResult = result.iterator().next();
    assertThat(citizenResult.getTemplateId()).isEqualTo(templateId);
    assertThat(citizenResult.getStatus()).isEqualTo(StatusType.SENT);
  }

  @Test
  void shouldNotPersistLetterDetailsWhenDuplicateDetailsProvided() {
    // given
    LetterDetails letterDetails1 = aValidLetterDetails();
    UUID templateId = createTemplate();
    letterDetails1.setTemplateId(templateId);
    letterDetailsRepository.saveAndFlush(letterDetails1);
    LetterDetails letterDetails2 = aValidLetterDetails();
    letterDetails2.setTemplateId(templateId);
    // when
    // then
    assertThatThrownBy(() -> letterDetailsRepository.saveAndFlush(letterDetails2))
        .isInstanceOf(DataIntegrityViolationException.class)
        .hasMessageContaining(
            "could not execute statement [ERROR: duplicate key value violates unique constraint \"transaction_dt_letter_template_id_reference_unique");
  }

  private UUID createTemplate() {
    return createTemplateWithDiffDaysEffectiveFrom(200);
  }

  private UUID createTemplateWithDiffDaysEffectiveFrom(int minusDays) {
    LocalDate effectiveTo = LocalDate.now();
    LocalDate effectiveFrom = effectiveTo.minusDays(minusDays);
    LetterTemplate actualResult = createATestLetterTemplate(effectiveTo, effectiveFrom);
    return actualResult.getId();
  }

  private LetterTemplate createATestLetterTemplate(LocalDate effectiveTo, LocalDate effectiveFrom) {
    LetterTemplate letterTemplate =
        aValidLetterTemplateWithEffectiveFromEffectiveTo(effectiveFrom, effectiveTo);
    LetterTemplate actualResult = letterTemplateRepository.save(letterTemplate);
    return actualResult;
  }
}
