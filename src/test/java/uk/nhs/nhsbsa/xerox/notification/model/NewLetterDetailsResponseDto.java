package uk.nhs.nhsbsa.xerox.notification.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NewLetterDetailsResponseDto {
  private UUID id;
  private String reference;
  private String correlationId;
  private StatusType status;
  private UUID templateId;
  private LocalDate transactionDate;

  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime statusUpdateAt;

  @JdbcTypeCode(SqlTypes.JSON)
  private Map<String, String> personalisation;

  @JsonProperty("_meta")
  private Meta meta;

  @JsonProperty("_links")
  private Links links;
}
