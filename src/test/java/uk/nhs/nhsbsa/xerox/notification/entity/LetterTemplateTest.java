package uk.nhs.nhsbsa.xerox.notification.entity;

import static org.mockito.Mockito.never;
import static uk.nhs.nhsbsa.exemptions.assertions.ConstraintViolationAssert.assertThat;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.SERVICE_NAME_HRT_PPC;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.VALIDATION_ERROR_INVALID_SERVICE_NAME_MESSAGE;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.VALIDATION_ERROR_NOT_EMPTY_MESSAGE;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.VALIDATION_KEY;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterTemplateTestDataFactory.*;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.MetaUtility.getExpectedMetaData;

import jakarta.validation.ConstraintViolation;
import java.time.LocalDate;
import java.util.Set;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import uk.nhs.nhsbsa.exemptions.assertions.AbstractValidationTest;
import uk.nhs.nhsbsa.exemptions.models.ApplicationChannel;
import uk.nhs.nhsbsa.xerox.notification.validators.DynamicStringProperties;

public class LetterTemplateTest extends AbstractValidationTest {

  private static MockedStatic<DynamicStringProperties> utilities;

  @BeforeEach
  public void init() {
    utilities = Mockito.mockStatic(DynamicStringProperties.class);
  }

  @AfterEach
  public void close() {
    utilities.close();
  }

  @Test
  void shouldValidateLetterTemplateWhenValidLetterTemplateReceived() {
    // given
    utilities
        .when(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY))
        .thenReturn(SERVICE_NAME_HRT_PPC);
    LetterTemplate letterTemplate =
        aLetterTemplateWithAllFields(
            1,
            SERVICE_NAME_HRT_PPC,
            "ISSUE",
            LocalDate.parse("2023-04-01"),
            "HRT_PPC_LETTER_20230401221021.xml",
            ApplicationChannel.ONLINE);

    // when
    Set<ConstraintViolation<LetterTemplate>> violations = validator.validate(letterTemplate);

    // then
    assertThat(violations).hasNoViolations();
    utilities.verify(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY));
  }

  @Test
  void shouldFailToValidateLetterTemplateWithNullVersion() {
    // given
    utilities
        .when(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY))
        .thenReturn(SERVICE_NAME_HRT_PPC);
    LetterTemplate letterTemplate =
        aLetterTemplateWithAllFields(
            null,
            SERVICE_NAME_HRT_PPC,
            "ISSUE",
            LocalDate.parse("2023-04-01"),
            "HRT_PPC_LETTER_20230401221021.xml",
            ApplicationChannel.ONLINE);

    // when
    Set<ConstraintViolation<LetterTemplate>> violations = validator.validate(letterTemplate);

    // then
    assertThat(violations)
        .hasSingleConstraintViolation(VALIDATION_ERROR_NOT_EMPTY_MESSAGE, "version");
    utilities.verify(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY));
  }

  @Test
  void shouldFailToValidateLetterTemplateWithNullServiceName() {
    // given
    utilities
        .when(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY))
        .thenReturn(SERVICE_NAME_HRT_PPC);
    LetterTemplate letterTemplate =
        aLetterTemplateWithAllFields(
            1,
            null,
            "ISSUE",
            LocalDate.parse("2023-04-01"),
            "HRT_PPC_LETTER_20230401221021.xml",
            ApplicationChannel.ONLINE);

    // when
    Set<ConstraintViolation<LetterTemplate>> violations = validator.validate(letterTemplate);

    // then
    assertThat(violations)
        .hasSingleConstraintViolation(VALIDATION_ERROR_NOT_EMPTY_MESSAGE, "serviceName");
    utilities.verify(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY));
  }

  @Test
  void shouldFailToValidateLetterTemplateWithInvalidServiceName() {
    // given
    utilities
        .when(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY))
        .thenReturn(SERVICE_NAME_HRT_PPC);
    LetterTemplate letterTemplate =
        aLetterTemplateWithAllFields(
            1,
            "PPC2",
            "ISSUE",
            LocalDate.parse("2023-04-01"),
            "HRT_PPC_LETTER_20230401221021.xml",
            ApplicationChannel.ONLINE);

    // when
    Set<ConstraintViolation<LetterTemplate>> violations = validator.validate(letterTemplate);

    // then
    assertThat(violations)
        .hasSingleConstraintViolation(VALIDATION_ERROR_INVALID_SERVICE_NAME_MESSAGE, "serviceName");
    utilities.verify(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY));
  }

  @Test
  void shouldFailToValidateLetterTemplateWithNullTemplateName() {
    // given
    utilities
        .when(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY))
        .thenReturn(SERVICE_NAME_HRT_PPC);
    LetterTemplate letterTemplate =
        aLetterTemplateWithAllFields(
            1,
            SERVICE_NAME_HRT_PPC,
            null,
            LocalDate.parse("2023-04-01"),
            "HRT_PPC_LETTER_20230401221021.xml",
            ApplicationChannel.ONLINE);

    // when
    Set<ConstraintViolation<LetterTemplate>> violations = validator.validate(letterTemplate);

    // then
    assertThat(violations)
        .hasSingleConstraintViolation(VALIDATION_ERROR_NOT_EMPTY_MESSAGE, "templateName");
    utilities.verify(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY));
  }

  @Test
  void shouldFailToValidateLetterTemplateWithNullEffectiveFrom() {
    // given
    utilities
        .when(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY))
        .thenReturn(SERVICE_NAME_HRT_PPC);
    LetterTemplate letterTemplate =
        aLetterTemplateWithAllFields(
            1,
            SERVICE_NAME_HRT_PPC,
            "ISSUE",
            null,
            "HRT_PPC_LETTER_20230401221021.xml",
            ApplicationChannel.ONLINE);

    // when
    Set<ConstraintViolation<LetterTemplate>> violations = validator.validate(letterTemplate);

    // then
    assertThat(violations)
        .hasSingleConstraintViolation(VALIDATION_ERROR_NOT_EMPTY_MESSAGE, "effectiveFrom");
    utilities.verify(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY));
  }

  @Test
  void shouldFailToValidateLetterTemplateWithNullTemplate() {
    // given
    utilities
        .when(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY))
        .thenReturn(SERVICE_NAME_HRT_PPC);
    LetterTemplate letterTemplate =
        aLetterTemplateWithAllFields(
            1,
            SERVICE_NAME_HRT_PPC,
            "ISSUE",
            LocalDate.parse("2023-04-01"),
            null,
            ApplicationChannel.ONLINE);

    // when
    Set<ConstraintViolation<LetterTemplate>> violations = validator.validate(letterTemplate);

    // then
    assertThat(violations)
        .hasSingleConstraintViolation(VALIDATION_ERROR_NOT_EMPTY_MESSAGE, "template");
    utilities.verify(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY));
  }

  @Test
  void shouldRedactLetterTemplateWhenToStringIsCalled() {
    // given
    final var letterTemplate =
        aValidLetterTemplateWithEffectiveFromEffectiveTo(LocalDate.parse("2023-04-01"), null);
    final String expectedToStringOutput = buildExpectedLetterTemplateToString(letterTemplate);

    // when
    final String actualToStringOutput = letterTemplate.toString();

    // then
    Assertions.assertThat(actualToStringOutput)
        .as(
            "%s toString() does not match expected output",
            letterTemplate.getClass().getSimpleName())
        .isEqualTo(expectedToStringOutput);
    utilities.verify(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY), never());
  }

  private String buildExpectedLetterTemplateToString(LetterTemplate letterTemplate) {
    return letterTemplate.getClass().getSimpleName()
        + " [id="
        + (letterTemplate.getId() == null ? "" : letterTemplate.getId())
        + ", meta="
        + getExpectedMetaData(letterTemplate)
        + ", version="
        + letterTemplate.getVersion()
        + ", serviceName="
        + letterTemplate.getServiceName()
        + ", templateName="
        + letterTemplate.getTemplateName()
        + ", effectiveFrom="
        + letterTemplate.getEffectiveFrom()
        + ", effectiveTo="
        + (letterTemplate.getEffectiveTo() == null ? "" : letterTemplate.getEffectiveTo())
        + ", template="
        + letterTemplate.getTemplate()
        + "]";
  }
}
