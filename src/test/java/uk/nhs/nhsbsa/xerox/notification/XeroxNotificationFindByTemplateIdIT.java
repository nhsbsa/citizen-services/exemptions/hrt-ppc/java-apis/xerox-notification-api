package uk.nhs.nhsbsa.xerox.notification;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;
import static uk.nhs.nhsbsa.exemptions.TestConstants.CREATED_BY;
import static uk.nhs.nhsbsa.exemptions.assertions.TestAssertions.assertErrorInResponse;
import static uk.nhs.nhsbsa.exemptions.utilities.ComparisonUtil.recursiveComparisonConfiguration;
import static uk.nhs.nhsbsa.exemptions.utilities.UriBuilderUtil.buildQueryUri;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.BAD_REQUEST_ERROR_MESSAGE;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.LETTER_TEMPLATE_API_SEARCH_BY_TEMPLATE_ID_URI;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.NOT_FOUND_ERROR_MESSAGE;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterTemplateTestDataFactory.aValidLetterTemplate;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LinksTestDataFactory.aValidLinksBuilderWithASelfLinkAndTemplate;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.MetaUtility.validMetaWithAllFields;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.XeroxHeadersTestDataFactory.validXeroxRequestHeaders;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.UUID;
import org.hibernate.exception.SQLGrammarException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockReset;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import uk.nhs.nhsbsa.exemptions.models.ErrorResponse;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterTemplate;
import uk.nhs.nhsbsa.xerox.notification.model.Links;
import uk.nhs.nhsbsa.xerox.notification.model.Meta;
import uk.nhs.nhsbsa.xerox.notification.model.NewLetterTemplateResponseDto;
import uk.nhs.nhsbsa.xerox.notification.model.XeroxNotificationApplicationRequestContext;
import uk.nhs.nhsbsa.xerox.notification.repository.LetterDetailsRepository;
import uk.nhs.nhsbsa.xerox.notification.repository.LetterTemplateRepository;
import uk.nhs.nhsbsa.xerox.notification.security.SpringSecurityAuditorAware;
import uk.nhs.nhsbsa.xerox.notification.testsupport.converters.LetterTemplateToNewLetterTemplateResponseDtoConverter;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureWebTestClient
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class XeroxNotificationFindByTemplateIdIT extends AbstractBaseIT {
  @Mock
  private XeroxNotificationApplicationRequestContext xeroxNotificationApplicationRequestContext;

  @Autowired private SpringSecurityAuditorAware springSecurityAuditorAware;

  @SpyBean(reset = MockReset.AFTER)
  LetterDetailsRepository letterDetailsRepository;

  @SpyBean(reset = MockReset.AFTER)
  private LetterTemplateRepository letterTemplateRepository;

  @BeforeEach
  public void setup() {
    MockHttpServletRequest request = new MockHttpServletRequest();
    RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(CREATED_BY);
    ReflectionTestUtils.setField(
        springSecurityAuditorAware,
        "xeroxNotificationApplicationRequestContext",
        xeroxNotificationApplicationRequestContext);

    requestHeaders = validXeroxRequestHeaders("User1");
    letterDetailsRepository.deleteAll();
    letterDetailsRepository.flush();
    letterTemplateRepository.deleteAll();
    letterTemplateRepository.flush();
  }

  @AfterEach
  public void tearDown() {
    RequestContextHolder.resetRequestAttributes();
  }

  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource(strings = {"aaaaaaaaaahhhhhhhhhhhhslllllllllllllllllllllll4785960684373737"})
  void shouldReturnBadRequestWhenUserIdHeaderMissingOrInvalid(String user) throws Exception {
    // given
    requestHeaders = validXeroxRequestHeaders(user);
    LetterTemplate letterTemplate = letterTemplateRepository.save(aValidLetterTemplate());

    // when
    ResponseEntity<ErrorResponse> response =
        getLetterTemplateFindByTemplateIdRequest(
            letterTemplate.getId().toString(), requestHeaders, ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, BAD_REQUEST_ERROR_MESSAGE);
  }

  @Test
  void shouldReturnNotFoundWithBodyWhenEmptyTemplateId() throws Exception {
    // when
    ResponseEntity<ErrorResponse> response =
        getLetterTemplateFindByTemplateIdRequest("", requestHeaders, ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(NOT_FOUND);
    assertErrorInResponse(response, NOT_FOUND, NOT_FOUND_ERROR_MESSAGE);
  }

  @ParameterizedTest
  @ValueSource(strings = {"abcd", "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"})
  void shouldReturnBadRequestWhenRequestByWrongFormatTemplateId(String templateId)
      throws Exception {
    // given
    letterTemplateRepository.save(aValidLetterTemplate());

    // when
    ResponseEntity<ErrorResponse> response =
        getLetterTemplateFindByTemplateIdRequest(templateId, requestHeaders, ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, BAD_REQUEST_ERROR_MESSAGE);
  }

  @Test
  void shouldReturnBadRequestWhenRequestByMultipleTemplateId() throws Exception {
    // given
    letterTemplateRepository.save(aValidLetterTemplate());
    String templateId = UUID.randomUUID() + "," + UUID.randomUUID();

    // when
    ResponseEntity<ErrorResponse> response =
        getLetterTemplateFindByTemplateIdRequest(templateId, requestHeaders, ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, BAD_REQUEST_ERROR_MESSAGE);
  }

  @Test
  void shouldReturnNotFoundAndNoBodyWithNotExistedTemplateId() throws Exception {
    // given
    LetterTemplate letterTemplate = letterTemplateRepository.save(aValidLetterTemplate());

    UUID notExistedUUID = UUID.randomUUID();
    // extremely low probability
    while (notExistedUUID.equals(letterTemplate.getId())) {
      notExistedUUID = UUID.randomUUID();
    }
    // when
    ResponseEntity<ErrorResponse> response =
        getLetterTemplateFindByTemplateIdRequest(
            notExistedUUID.toString(), requestHeaders, ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(NOT_FOUND);
    assertThat(response.getBody()).isNull();
  }

  @Test
  void shouldFindTemplateFindByEffectiveOnDateAndReturnSuccessResponse() throws Exception {
    // given
    LetterTemplate letterTemplate = letterTemplateRepository.save(aValidLetterTemplate());
    Meta meta =
        validMetaWithAllFields(
            letterTemplate.getCreatedTimestamp(),
            letterTemplate.getCreatedBy(),
            letterTemplate.getUpdatedTimestamp(),
            letterTemplate.getUpdatedBy());

    // when
    ResponseEntity<NewLetterTemplateResponseDto> response =
        getLetterTemplateFindByTemplateIdRequest(
            letterTemplate.getId().toString(), requestHeaders, NewLetterTemplateResponseDto.class);

    // then
    NewLetterTemplateResponseDto resultLetterTemplate = response.getBody();
    Links links =
        aValidLinksBuilderWithASelfLinkAndTemplate(port, letterTemplate.getId().toString());
    NewLetterTemplateResponseDto expectedNewLetterTemplateResponseDto =
        LetterTemplateToNewLetterTemplateResponseDtoConverter.convert(letterTemplate, links);

    assertThat(response.getStatusCode()).isEqualTo(OK);
    assertThat(resultLetterTemplate)
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .ignoringFields("meta")
        .isEqualTo(expectedNewLetterTemplateResponseDto);
    assertThat(resultLetterTemplate.getMeta())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .ignoringFields("createdTimestamp", "updatedTimestamp")
        .isEqualTo(meta);
    assertThat(resultLetterTemplate.getMeta().getCreatedTimestamp())
        .isEqualToIgnoringMinutes(meta.getCreatedTimestamp());
    assertThat(resultLetterTemplate.getMeta().getUpdatedTimestamp())
        .isEqualToIgnoringMinutes(meta.getUpdatedTimestamp());
  }

  @Test
  void shouldReturnInternalServerErrorWhenExceptionOccurs() throws Exception {
    // given
    LetterTemplate letterTemplate = letterTemplateRepository.save(aValidLetterTemplate());
    SQLGrammarException sqlGrammarException = new SQLGrammarException("sql exception", null);
    InvalidDataAccessResourceUsageException exception =
        new InvalidDataAccessResourceUsageException("invalid data", sqlGrammarException);
    doThrow(exception).when(letterTemplateRepository).findById(any());

    // when
    ResponseEntity<ErrorResponse> response =
        getLetterTemplateFindByTemplateIdRequest(
            letterTemplate.getId().toString(), requestHeaders, ErrorResponse.class);
    // then
    assertThat(response.getStatusCode()).isEqualTo(INTERNAL_SERVER_ERROR);
    assertErrorInResponse(response, INTERNAL_SERVER_ERROR, "invalid data");
  }

  private ResponseEntity getLetterTemplateFindByTemplateIdRequest(
      String templateId, HttpHeaders headers, Class expectedType) throws URISyntaxException {
    URI uri =
        buildQueryUri(Map.of(), LETTER_TEMPLATE_API_SEARCH_BY_TEMPLATE_ID_URI + "/" + templateId);
    RequestEntity<?> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, uri);
    return testRestTemplate.exchange(requestEntity, expectedType);
  }
}
