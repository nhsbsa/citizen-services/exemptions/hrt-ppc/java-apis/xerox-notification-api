package uk.nhs.nhsbsa.xerox.notification.testsupport;

import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.SERVICE_NAME_HRT_PPC;

import java.time.LocalDate;
import uk.nhs.nhsbsa.exemptions.models.ApplicationChannel;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterTemplate;

public class LetterTemplateTestDataFactory {

  public static LetterTemplate aValidLetterTemplate() {
    return aValidLetterTemplateBuilder()
        .effectiveFrom(LocalDate.parse("2021-01-01"))
        .effectiveTo(LocalDate.parse("2024-03-30"))
        .build();
  }

  public static LetterTemplate aValidLetterTemplateWithNullEffectiveTo() {
    return aValidLetterTemplateBuilder()
        .effectiveFrom(LocalDate.parse("2021-01-01"))
        .effectiveTo(null)
        .build();
  }

  public static LetterTemplate aValidLetterTemplateWithNullEffectiveTo(String effectiveFrom) {
    return aValidLetterTemplateBuilder()
        .effectiveFrom(LocalDate.parse(effectiveFrom))
        .effectiveTo(null)
        .build();
  }

  public static LetterTemplate aGeneratedValidLetterTemplateWithEffectiveTo(
      LocalDate effectiveTo, int minusDays) {
    LocalDate effectiveFrom = effectiveTo.minusDays(minusDays);
    return aValidLetterTemplateBuilder()
        .effectiveFrom(effectiveFrom)
        .effectiveTo(effectiveTo)
        .build();
  }

  public static LetterTemplate aValidLetterTemplateWithEffectiveFromEffectiveTo(
      LocalDate effectiveFrom, LocalDate effectiveTo) {
    return aValidLetterTemplateBuilder()
        .effectiveFrom(effectiveFrom)
        .effectiveTo(effectiveTo)
        .build();
  }

  public static LetterTemplate aLetterTemplateWithAllFields(
      Integer version,
      String serviceName,
      String templateName,
      LocalDate effectiveFrom,
      String template,
      ApplicationChannel applicationChannel) {
    return LetterTemplate.builder()
        .version(version)
        .serviceName(serviceName)
        .templateName(templateName)
        .template(template)
        .effectiveFrom(effectiveFrom)
        .createdBy(applicationChannel.name())
        .build();
  }

  private static LetterTemplate.LetterTemplateBuilder<?, ?> aValidLetterTemplateBuilder() {
    return LetterTemplate.builder()
        .version(1)
        .serviceName(SERVICE_NAME_HRT_PPC)
        .templateName("ISSUE")
        .template("HRT_PPC_LETTER_20230401221021.xml")
        .createdBy(ApplicationChannel.ONLINE.name());
  }
}
