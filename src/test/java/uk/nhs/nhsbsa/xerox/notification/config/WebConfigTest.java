package uk.nhs.nhsbsa.xerox.notification.config;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verifyNoInteractions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.servlet.handler.MappedInterceptor;
import uk.nhs.nhsbsa.exemptions.interceptor.UserIdValidationInterceptor;
import uk.nhs.nhsbsa.xerox.notification.interceptor.CorrelationIdValidationInterceptor;
import uk.nhs.nhsbsa.xerox.notification.interceptor.RunDateValidationInterceptor;
import uk.nhs.nhsbsa.xerox.notification.interceptor.ServiceNameValidationInterceptor;
import uk.nhs.nhsbsa.xerox.notification.interceptor.XeroxNotificationApplicationRequestContextInterceptor;

@ExtendWith(MockitoExtension.class)
public class WebConfigTest {

  @Mock private CorrelationIdValidationInterceptor correlationIdValidationInterceptor;
  @Mock private ServiceNameValidationInterceptor serviceNameValidationInterceptor;
  @Mock private UserIdValidationInterceptor userIdValidationInterceptor;

  @Mock
  private XeroxNotificationApplicationRequestContextInterceptor
      xeroxNotificationApplicationRequestContextInterceptor;

  @Mock RunDateValidationInterceptor runDateValidationInterceptor;
  @InjectMocks private WebConfig webConfig;

  @Test
  void shouldMapCorrelationIdValidationInterceptor() {
    // when
    MappedInterceptor mappedInterceptor = webConfig.mapCorrelationIdValidationInterceptor();

    // then
    assertThat(mappedInterceptor.getPathPatterns())
        .containsExactly("/v1/xerox-notifications/letter");
    assertThat(mappedInterceptor.getInterceptor())
        .isInstanceOf(CorrelationIdValidationInterceptor.class);

    verifyNoInteractionsOnAllMocks();
  }

  @Test
  void shouldMapUserIdValidationInterceptor() {
    // when
    MappedInterceptor mappedInterceptor = webConfig.mapUserIdValidationInterceptor();

    // then
    assertThat(mappedInterceptor.getPathPatterns())
        .containsExactly(
            "/v1/xerox-notifications/letter",
            "/v1/xerox-notifications/letter/*",
            "/v1/xerox-notifications/templates/*",
            "/v1/xerox-notifications/templates/search/findByEffectiveOnDate");
    assertThat(mappedInterceptor.getInterceptor()).isInstanceOf(UserIdValidationInterceptor.class);

    verifyNoInteractionsOnAllMocks();
  }

  @Test
  void shouldMapServiceNameValidationInterceptor() {
    // when
    MappedInterceptor mappedInterceptor = webConfig.mapServiceNameValidationInterceptor();

    // then
    assertThat(mappedInterceptor.getPathPatterns())
        .containsExactly("/v1/xerox-notifications/letter");
    assertThat(mappedInterceptor.getInterceptor())
        .isInstanceOf(ServiceNameValidationInterceptor.class);

    verifyNoInteractionsOnAllMocks();
  }

  @Test
  void shouldMapXeroxNotificationApplicationRequestContextInterceptor() {
    // when
    MappedInterceptor mappedInterceptor =
        webConfig.mapXeroxNotificationApplicationRequestContextInterceptor();

    // then
    assertThat(mappedInterceptor.getPathPatterns())
        .containsExactly("/v1/xerox-notifications/letter", "/v1/xerox-notifications/letter/*");
    assertThat(mappedInterceptor.getInterceptor())
        .isInstanceOf(XeroxNotificationApplicationRequestContextInterceptor.class);

    verifyNoInteractionsOnAllMocks();
  }

  @Test
  void shouldMapRunDateValidationInterceptor() {
    // when
    MappedInterceptor mappedInterceptor = webConfig.mapRunDateValidationInterceptor();

    // then
    assertThat(mappedInterceptor.getPathPatterns())
        .containsExactly("/v1/xerox-notifications/templates/search/findByEffectiveOnDate");
    assertThat(mappedInterceptor.getInterceptor()).isInstanceOf(RunDateValidationInterceptor.class);

    verifyNoInteractionsOnAllMocks();
  }

  private void verifyNoInteractionsOnAllMocks() {
    verifyNoInteractions(
        userIdValidationInterceptor,
        xeroxNotificationApplicationRequestContextInterceptor,
        runDateValidationInterceptor,
        correlationIdValidationInterceptor,
        serviceNameValidationInterceptor);
  }
}
