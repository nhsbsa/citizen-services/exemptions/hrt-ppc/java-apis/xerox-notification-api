package uk.nhs.nhsbsa.xerox.notification.javers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import uk.nhs.nhsbsa.xerox.notification.model.XeroxNotificationApplicationRequestContext;

@ExtendWith(MockitoExtension.class)
public class UserIdAuthorProviderTest {

  @Mock
  private XeroxNotificationApplicationRequestContext xeroxNotificationApplicationRequestContext;

  @InjectMocks private UserIdAuthorProvider userIdAuthorProvider;

  private final String user = "myUser";

  @Test
  void shouldReturnAuthorWhenUserIsSetInApplicationRequestContext() {
    // given
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(user);

    // when
    String result = userIdAuthorProvider.provide();

    // then
    assertThat(result).isEqualTo(user);
    verify(xeroxNotificationApplicationRequestContext).getUser();
    verifyNoMoreInteractions(xeroxNotificationApplicationRequestContext);
  }

  @Test
  void shouldReturnUnknownAuthorWhenUserIsNotSetInApplicationRequestContext() {
    // given
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(null);

    // when
    String result = userIdAuthorProvider.provide();

    // then
    assertThat(result).isEqualTo("unknown");
    verify(xeroxNotificationApplicationRequestContext).getUser();
    verifyNoMoreInteractions(xeroxNotificationApplicationRequestContext);
  }
}
