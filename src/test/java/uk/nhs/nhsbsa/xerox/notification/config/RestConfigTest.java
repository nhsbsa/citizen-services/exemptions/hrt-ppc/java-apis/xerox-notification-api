package uk.nhs.nhsbsa.xerox.notification.config;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterDetailsTestDataFactory.aValidLetterDetails;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterTemplateTestDataFactory.aValidLetterTemplate;

import jakarta.servlet.http.HttpServletRequest;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.RepresentationModelProcessor;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterDetails;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterTemplate;
import uk.nhs.nhsbsa.xerox.notification.model.Constants;

@ExtendWith(MockitoExtension.class)
public class RestConfigTest {

  @Mock private RepositoryRestConfiguration config;
  @Mock private CorsRegistry corsRegistry;
  @Mock private HttpServletRequest httpServletRequest;
  @InjectMocks RestConfig configuration;

  @Test
  public void shouldSetBasePathAndExposeIdsWhenRestConfigurationsInitialized() {
    // when
    configuration.configureRepositoryRestConfiguration(config, corsRegistry);

    // then
    verify(config).setBasePath(Constants.XEROX_NOTIFICATION_API_PATH_URI);
    verify(config).exposeIdsFor(LetterTemplate.class);
    verify(config).exposeIdsFor(LetterDetails.class);
    verifyNoMoreInteractions(config);
    verifyNoInteractions(corsRegistry, httpServletRequest);
  }

  @Test
  public void
      shouldAddTemplateLinksAndRemoveLetterTemplateWhenFindByEffectiveOnDateRequestIsReceived() {
    // given
    given(httpServletRequest.getMethod()).willReturn(HttpMethod.GET.name());
    LetterTemplate letterTemplate = aValidLetterTemplate();
    EntityModel<LetterTemplate> preProcessModel = EntityModel.of(letterTemplate);
    preProcessModel.add(Link.of(Constants.XEROX_NOTIFICATION_API_PATH_URI, "letterTemplate"));

    // when
    RepresentationModelProcessor<EntityModel<LetterTemplate>> model =
        configuration.letterTemplateProcessor();
    EntityModel<LetterTemplate> letterTemplateResource = model.process(preProcessModel);

    // then
    verify(httpServletRequest).getMethod();
    verifyNoMoreInteractions(httpServletRequest);
    verifyNoInteractions(corsRegistry, config);
    assertThat(letterTemplateResource.getLink("template")).isPresent();
    assertThat(letterTemplateResource.getLink("letterTemplate")).isNotPresent();
  }

  @Test
  public void shouldAddLetterDetailsLinksAndRemoveLetterDetailsWhenPOSTRequestIsReceived() {
    // given
    given(httpServletRequest.getMethod()).willReturn(HttpMethod.POST.name());
    LetterDetails aletterDetails = aValidLetterDetails();
    LetterDetails letterDetails = Mockito.spy(aletterDetails);
    given(letterDetails.getId()).willReturn(UUID.randomUUID());
    given(letterDetails.getTemplateId()).willReturn(UUID.randomUUID());
    EntityModel<LetterDetails> preProcessModel = EntityModel.of(letterDetails);
    preProcessModel.add(Link.of(Constants.XEROX_NOTIFICATION_API_PATH_URI, "letterDetails"));

    // when
    RepresentationModelProcessor<EntityModel<LetterDetails>> model =
        configuration.letterDetailsProcessor();
    EntityModel<LetterDetails> letterDetailsResource = model.process(preProcessModel);

    // then
    verify(letterDetails).getId();
    verify(letterDetails).getTemplateId();
    verify(httpServletRequest).getMethod();
    verifyNoMoreInteractions(httpServletRequest);
    verifyNoMoreInteractions(letterDetails);
    verifyNoInteractions(corsRegistry, config);
    assertThat(letterDetailsResource.getLink("letter")).isPresent();
    assertThat(letterDetailsResource.getLink("letterDetails")).isNotPresent();
  }

  @Test
  public void shouldAddLetterDetailsLinksAndRemoveLetterDetailsWhenGETRequestIsReceived() {
    // given
    given(httpServletRequest.getMethod()).willReturn(HttpMethod.GET.name());
    LetterDetails aletterDetails = aValidLetterDetails();
    LetterDetails letterDetails = Mockito.spy(aletterDetails);
    given(letterDetails.getId()).willReturn(UUID.randomUUID());
    given(letterDetails.getTemplateId()).willReturn(UUID.randomUUID());
    EntityModel<LetterDetails> preProcessModel = EntityModel.of(letterDetails);
    preProcessModel.add(Link.of(Constants.XEROX_NOTIFICATION_API_PATH_URI, "letterDetails"));

    // when
    RepresentationModelProcessor<EntityModel<LetterDetails>> model =
        configuration.letterDetailsProcessor();
    EntityModel<LetterDetails> letterDetailsResource = model.process(preProcessModel);

    // then
    verify(letterDetails).getId();
    verify(letterDetails).getTemplateId();
    verify(httpServletRequest, times(2)).getMethod();
    verifyNoMoreInteractions(httpServletRequest);
    verifyNoMoreInteractions(letterDetails);
    verifyNoInteractions(corsRegistry, config);
    assertThat(letterDetailsResource.getLink("letter")).isPresent();
    assertThat(letterDetailsResource.getLink("letterDetails")).isNotPresent();
  }

  @ParameterizedTest
  @ValueSource(strings = {"HEAD", "PUT", "PATCH", "DELETE", "OPTIONS", "TRACE"})
  public void shouldNotAddLetterDetailsLinksAndNotRemoveLetterDetailsIfHttpMethodOtherThanPost(
      String httpMethod) {
    // given
    given(httpServletRequest.getMethod()).willReturn(httpMethod);
    LetterDetails letterDetails = aValidLetterDetails();
    EntityModel<LetterDetails> aPreProcessModel = EntityModel.of(letterDetails);
    aPreProcessModel.add(Link.of(Constants.XEROX_NOTIFICATION_API_PATH_URI, "letterDetails"));
    EntityModel<LetterDetails> preProcessModel = Mockito.spy(aPreProcessModel);

    // when
    RepresentationModelProcessor<EntityModel<LetterDetails>> model =
        configuration.letterDetailsProcessor();
    EntityModel<LetterDetails> letterDetailsResource = model.process(preProcessModel);

    // then
    verify(httpServletRequest, times(2)).getMethod();
    verifyNoMoreInteractions(httpServletRequest);
    verifyNoInteractions(preProcessModel, corsRegistry, config);
    assertThat(letterDetailsResource.getLink("letter")).isNotPresent();
    assertThat(letterDetailsResource.getLink("letterDetails")).isPresent();
  }
}
