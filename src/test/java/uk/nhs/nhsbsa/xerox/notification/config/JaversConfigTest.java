package uk.nhs.nhsbsa.xerox.notification.config;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verifyNoInteractions;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterDetailsTestDataFactory.aValidLetterDetailsWithIdTemplateIdAndReference;

import java.util.Map;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterDetails;
import uk.nhs.nhsbsa.xerox.notification.javers.UserIdAuthorProvider;

@ExtendWith(MockitoExtension.class)
public class JaversConfigTest {

  @Mock private UserIdAuthorProvider userIdAuthorProvider;

  @InjectMocks JaversConfig javersConfig;

  @Test
  public void shouldReturnUserIdAuthorProviderWhenAuthorProviderCalled() {
    // when
    Object result = javersConfig.authorProvider();

    // then
    assertThat(result).isEqualTo(userIdAuthorProvider);
    verifyNoInteractions(userIdAuthorProvider);
  }

  @Test
  public void shouldReturnEmptyMapWhenCommitPropertiesProviderCalled() {
    // when
    Map<String, String> result =
        javersConfig.commitPropertiesProvider().provideForCommittedObject(new Object());

    // then
    assertThat(result).isEmpty();
  }

  @Test
  public void shouldReturnValidMapWhenCommitPropertiesProviderCalled() {
    // given
    UUID id = UUID.randomUUID();
    UUID templateId = UUID.randomUUID();
    String reference = "HRTABCD123";
    LetterDetails letterDetails =
        aValidLetterDetailsWithIdTemplateIdAndReference(id, templateId, reference);

    // when
    Map<String, String> result =
        javersConfig.commitPropertiesProvider().provideForCommittedObject(letterDetails);

    // then
    assertThat(result)
        .isEqualTo(
            Map.of(
                "letterDetailsId",
                id.toString(),
                "templateId",
                templateId.toString(),
                "reference",
                reference));
  }
}
