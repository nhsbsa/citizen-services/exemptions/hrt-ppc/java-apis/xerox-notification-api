package uk.nhs.nhsbsa.xerox.notification.testsupport;

import static uk.nhs.nhsbsa.xerox.notification.model.Constants.HEADER_PARAM_USER_ID;

import org.springframework.http.HttpHeaders;

public class XeroxHeadersTestDataFactory {
  public static HttpHeaders validXeroxRequestHeaders(String userId) {
    HttpHeaders requestHeader = new HttpHeaders();
    requestHeader.set(HEADER_PARAM_USER_ID, userId);
    return requestHeader;
  }
}
