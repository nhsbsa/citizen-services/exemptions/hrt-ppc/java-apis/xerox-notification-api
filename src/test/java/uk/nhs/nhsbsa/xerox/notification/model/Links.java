package uk.nhs.nhsbsa.xerox.notification.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Links extends uk.nhs.nhsbsa.exemptions.model.Links {

  @JsonProperty("template")
  private LinkHolder xeroxTemplate;

  @JsonProperty("letter")
  private LinkHolder letterDetails;
}
