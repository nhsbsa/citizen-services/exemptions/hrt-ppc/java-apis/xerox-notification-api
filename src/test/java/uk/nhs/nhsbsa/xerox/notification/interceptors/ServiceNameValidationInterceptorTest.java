package uk.nhs.nhsbsa.xerox.notification.interceptors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static uk.nhs.nhsbsa.xerox.notification.model.Constants.HEADER_PARAM_SERVICE_NAME;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.SERVICE_NAME_HRT_PPC;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.VALIDATION_KEY;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpMethod;
import uk.nhs.nhsbsa.exemptions.exception.InvalidHeaderException;
import uk.nhs.nhsbsa.xerox.notification.interceptor.ServiceNameValidationInterceptor;
import uk.nhs.nhsbsa.xerox.notification.validators.DynamicStringProperties;

@ExtendWith(MockitoExtension.class)
public class ServiceNameValidationInterceptorTest {
  @Mock private HttpServletRequest request;
  @Mock private HttpServletResponse response;
  @InjectMocks private ServiceNameValidationInterceptor serviceNameValidationInterceptor;
  private static MockedStatic<DynamicStringProperties> utilities;

  @BeforeEach
  public void init() {
    utilities = Mockito.mockStatic(DynamicStringProperties.class);
  }

  @AfterEach
  public void close() {
    utilities.close();
  }

  @Test
  void shouldReturnTrueWhenServiceNameIsValid() {
    // given
    given(request.getMethod()).willReturn(HttpMethod.POST.name());
    given(request.getHeader(HEADER_PARAM_SERVICE_NAME)).willReturn(SERVICE_NAME_HRT_PPC);
    utilities
        .when(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY))
        .thenReturn(SERVICE_NAME_HRT_PPC);

    // when
    var result =
        serviceNameValidationInterceptor.preHandle(
            request, response, serviceNameValidationInterceptor);

    // then
    assertThat(result).isTrue();
    verify(request).getMethod();
    verify(request).getHeader(HEADER_PARAM_SERVICE_NAME);
    verifyNoMoreInteractions(request);
    verifyNoInteractions(response);
    utilities.verify(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY));
  }

  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource(strings = {"Test1", "PPC"})
  void shouldThrowExceptionWhenServiceNameIsInvalid(String serviceName) {
    // given
    given(request.getMethod()).willReturn(HttpMethod.POST.name());
    given(request.getHeader(HEADER_PARAM_SERVICE_NAME)).willReturn(serviceName);
    utilities
        .when(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY))
        .thenReturn(SERVICE_NAME_HRT_PPC);

    // when
    // then
    assertThatThrownBy(
            () ->
                serviceNameValidationInterceptor.preHandle(
                    request, response, serviceNameValidationInterceptor))
        .isInstanceOf(InvalidHeaderException.class)
        .hasMessage(HEADER_PARAM_SERVICE_NAME)
        .hasStackTraceContaining(HEADER_PARAM_SERVICE_NAME);
    verify(request).getMethod();
    verify(request).getHeader(HEADER_PARAM_SERVICE_NAME);
    verifyNoMoreInteractions(request);
    verifyNoInteractions(response);
    utilities.verify(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY));
  }

  @ParameterizedTest
  @ValueSource(strings = {"GET", "HEAD", "PUT", "PATCH", "DELETE", "OPTIONS", "TRACE"})
  void shouldNotValidateIfHttpMethodOtherThanPost(String httpMethod) {
    // given
    given(request.getMethod()).willReturn(httpMethod);
    utilities
        .when(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY))
        .thenReturn(SERVICE_NAME_HRT_PPC);

    // when
    var result =
        serviceNameValidationInterceptor.preHandle(
            request, response, serviceNameValidationInterceptor);

    // then
    assertThat(result).isTrue();
    verify(request).getMethod();
    verifyNoMoreInteractions(request);
    verifyNoInteractions(response);
    utilities.verify(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY), never());
  }
}
