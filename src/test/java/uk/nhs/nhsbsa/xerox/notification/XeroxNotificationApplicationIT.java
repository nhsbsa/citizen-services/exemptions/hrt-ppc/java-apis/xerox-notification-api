package uk.nhs.nhsbsa.xerox.notification;

import static uk.nhs.nhsbsa.exemptions.utilities.SwaggerGenerationUtil.assertSwaggerDocumentationRetrieved;

import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;

@SpringBootTest(
    classes = XeroxNotificationApplication.class,
    webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
    properties = {"server.port=57154"})
public class XeroxNotificationApplicationIT {
  @LocalServerPort private int port;
  @Autowired private TestRestTemplate testRestTemplate;

  @Test
  void contextLoads() {}

  /* This test generates the swagger.yml to place into root folder */
  @Test
  void swaggerDocumentationRetrieved() throws IOException {
    assertSwaggerDocumentationRetrieved(testRestTemplate, port);
  }
}
