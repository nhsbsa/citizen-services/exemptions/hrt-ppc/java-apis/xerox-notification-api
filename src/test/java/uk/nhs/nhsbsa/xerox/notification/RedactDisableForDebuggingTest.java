package uk.nhs.nhsbsa.xerox.notification;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

class RedactDisableForDebuggingTest {

  private final RedactDisableForDebugging redactDisableForDebugging =
      new RedactDisableForDebugging();

  @Test
  void shouldSetSystemPropertyWhenRedactIsDisabled() {
    // given
    System.setProperty("logging.redact.disabled", "");
    ReflectionTestUtils.setField(redactDisableForDebugging, "disabled", true);

    // when
    redactDisableForDebugging.setAsDisabled();

    // then
    Boolean redactDisabled = Boolean.valueOf(System.getProperty("logging.redact.disabled"));
    assertThat(redactDisabled).isTrue();
  }

  @Test
  void shouldSetSystemPropertyWhenRedactIsEnabled() {
    // given
    System.setProperty("logging.redact.disabled", "test");
    ReflectionTestUtils.setField(redactDisableForDebugging, "disabled", false);

    // when
    redactDisableForDebugging.setAsDisabled();

    // then
    Boolean redactDisabled = Boolean.valueOf(System.getProperty("logging.redact.disabled"));
    assertThat(redactDisabled).isFalse();
  }
}
