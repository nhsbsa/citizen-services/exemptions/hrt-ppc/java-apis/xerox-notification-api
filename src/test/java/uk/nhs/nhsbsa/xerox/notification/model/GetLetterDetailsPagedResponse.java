package uk.nhs.nhsbsa.xerox.notification.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import uk.nhs.nhsbsa.exemptions.model.Page;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetLetterDetailsPagedResponse {
  @JsonProperty("_embedded")
  private EmbeddedLetterDetailsDto embedded;

  @JsonProperty("_links")
  private Links links;

  @JsonProperty("page")
  private Page page;
}
