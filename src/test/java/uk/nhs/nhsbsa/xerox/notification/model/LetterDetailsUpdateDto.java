package uk.nhs.nhsbsa.xerox.notification.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LetterDetailsUpdateDto {
  private Optional<String> reference;
  private Optional<String> transactionDate;
  private Optional<String> status;
  private String fileName;
  private String failureReason;
}
