package uk.nhs.nhsbsa.xerox.notification.security;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import uk.nhs.nhsbsa.xerox.notification.model.XeroxNotificationApplicationRequestContext;

@ExtendWith(MockitoExtension.class)
public class SpringSecurityAuditorAwareTest {

  @Mock XeroxNotificationApplicationRequestContext xeroxNotificationApplicationRequestContext;

  @InjectMocks private SpringSecurityAuditorAware springSecurityAuditorAware;

  @Test
  void shouldGetUserWhenUserIsPresent() {
    // given
    String user = "USER_ONE";
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(user);

    // when
    Optional<String> result = springSecurityAuditorAware.getCurrentAuditor();

    // then
    assertThat(result).contains(user);
    verify(xeroxNotificationApplicationRequestContext).getUser();
    verifyNoMoreInteractions(xeroxNotificationApplicationRequestContext);
  }

  @ParameterizedTest
  @EmptySource
  void shouldGetEmptyStringWhenUserIsEmpty(String user) {
    // given
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(user);

    // when
    Optional<String> result = springSecurityAuditorAware.getCurrentAuditor();

    // then
    assertThat(result).containsSame("");
    verify(xeroxNotificationApplicationRequestContext).getUser();
    verifyNoMoreInteractions(xeroxNotificationApplicationRequestContext);
  }

  @ParameterizedTest
  @NullSource
  void shouldHaveExceptionWhenUserIsNull(String user) {
    // given
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(user);

    assertThatThrownBy(() -> springSecurityAuditorAware.getCurrentAuditor())
        .isInstanceOf(NullPointerException.class);
    verify(xeroxNotificationApplicationRequestContext).getUser();
    verifyNoMoreInteractions(xeroxNotificationApplicationRequestContext);
  }
}
