package uk.nhs.nhsbsa.xerox.notification.testsupport;

import java.util.LinkedHashMap;
import java.util.Map;

public class PersonalisationTestDataFactory {

  private static final String ADDRESS_LINE_1_KEY = "addressLine1";
  private static final String ADDRESS_LINE_2_KEY = "addressLine2";
  private static final String ADDRESS_LINE_3_KEY = "addressLine3";
  private static final String ADDRESS_LINE_4_KEY = "addressLine4";
  private static final String ADDRESS_LINE_5_KEY = "addressLine5";
  private static final String ADDRESS_LINE_6_KEY = "addressLine6";
  private static final String ADDRESS_LINE_7_KEY = "addressLine7";
  private static final String ADDRESS_LINE_8_KEY = "addressLine8";
  private static final String ADDRESS_LINE_1_VALUE = "Address line one";
  private static final String ADDRESS_LINE_2_VALUE = "Address line two";
  private static final String ADDRESS_LINE_3_VALUE = "Address line three";
  private static final String ADDRESS_LINE_4_VALUE = "Address line four";
  private static final String ADDRESS_LINE_5_VALUE = "Address line five";
  private static final String ADDRESS_LINE_6_VALUE = "Address line six";
  private static final String ADDRESS_LINE_7_VALUE = "Address line seven";
  private static final String ADDITIONAL_PROP_1_KEY = "additionalProp1";
  private static final String ADDITIONAL_PROP_2_KEY = "additionalProp2";
  private static final String ADDITIONAL_PROP_3_KEY = "additionalProp3";
  private static final String ADDITIONAL_PROP_VALUE = "string";
  private static final String POSTCODE = "NE15 8NY";
  private static Map<String, String> addressLineOne =
      Map.of(ADDRESS_LINE_1_KEY, ADDRESS_LINE_1_VALUE);

  private static Map<String, String> additionalPropLines =
      Map.of(
          ADDITIONAL_PROP_1_KEY,
          ADDITIONAL_PROP_VALUE,
          ADDITIONAL_PROP_2_KEY,
          ADDITIONAL_PROP_VALUE,
          ADDITIONAL_PROP_3_KEY,
          ADDITIONAL_PROP_VALUE);

  public static Map<String, String> aAddressLineNumberLessThan3Personalisation() {
    return combineMapWithAdditionPropLines(
        combineMapWithAddressLineOne(Map.of(ADDRESS_LINE_2_KEY, POSTCODE)));
  }

  public static Map<String, String> aNoPostcodePersonalisation() {
    return combineMapWithAdditionPropLines(combine7AddressLineWithPostCode("100088"));
  }

  public static Map<String, String> aNoPostcodePersonalisation8LineMixOrder() {
    return combineMapWithAddressLineOne(
        Map.of(
            ADDRESS_LINE_2_KEY,
            ADDRESS_LINE_2_VALUE,
            ADDITIONAL_PROP_2_KEY,
            ADDITIONAL_PROP_VALUE,
            ADDRESS_LINE_3_KEY,
            ADDRESS_LINE_3_VALUE,
            ADDRESS_LINE_4_KEY,
            ADDRESS_LINE_4_VALUE,
            ADDRESS_LINE_5_KEY,
            ADDRESS_LINE_5_VALUE,
            ADDITIONAL_PROP_1_KEY,
            ADDITIONAL_PROP_VALUE,
            ADDRESS_LINE_6_KEY,
            ADDRESS_LINE_6_VALUE,
            ADDRESS_LINE_7_KEY,
            ADDRESS_LINE_7_VALUE,
            ADDRESS_LINE_8_KEY,
            POSTCODE));
  }

  public static Map<String, String> aNoPostcodePersonalisation8Line() {
    return combineMapWithAdditionPropLines(combine8AddressLineWithPostCode("NE15 8NY"));
  }

  public static Map<String, String> aNoPostcodePersonalisation5LineMixOrder() {
    return Map.of(
        ADDITIONAL_PROP_1_KEY,
        ADDITIONAL_PROP_VALUE,
        ADDRESS_LINE_1_KEY,
        ADDRESS_LINE_1_VALUE,
        ADDRESS_LINE_2_KEY,
        ADDRESS_LINE_2_VALUE,
        ADDITIONAL_PROP_2_KEY,
        ADDITIONAL_PROP_VALUE,
        ADDRESS_LINE_3_KEY,
        ADDRESS_LINE_3_VALUE,
        ADDRESS_LINE_4_KEY,
        ADDRESS_LINE_4_VALUE,
        ADDRESS_LINE_5_KEY,
        "100088");
  }

  public static Map<String, String> aPersonalisation() {
    return combineMapWithAdditionPropLines(combine7AddressLineWithPostCode("NE15 8NY"));
  }

  private static Map<String, String> combine8AddressLineWithPostCode(String postcode) {
    return combineMaps(
        combine7AddressLineWithPostCode(ADDRESS_LINE_7_VALUE),
        Map.of(ADDRESS_LINE_8_KEY, postcode));
  }

  private static Map<String, String> combine7AddressLineWithPostCode(String postcode) {
    return combineMapWithAddressLineOne(
        Map.of(
            ADDRESS_LINE_2_KEY, ADDRESS_LINE_2_VALUE,
            ADDRESS_LINE_3_KEY, ADDRESS_LINE_3_VALUE,
            ADDRESS_LINE_4_KEY, ADDRESS_LINE_4_VALUE,
            ADDRESS_LINE_5_KEY, ADDRESS_LINE_5_VALUE,
            ADDRESS_LINE_6_KEY, ADDRESS_LINE_6_VALUE,
            ADDRESS_LINE_7_KEY, postcode));
  }

  private static Map<String, String> combineMapWithAddressLineOne(Map<String, String> map) {
    return combineMaps(addressLineOne, map);
  }

  private static Map<String, String> combineMapWithAdditionPropLines(Map<String, String> map) {
    return combineMaps(map, additionalPropLines);
  }

  private static Map<String, String> combineMaps(
      Map<String, String> map1, Map<String, String> map2) {
    Map map = new LinkedHashMap(map1);
    map.putAll(map2);
    return map;
  }
}
