package uk.nhs.nhsbsa.xerox.notification.testsupport.converters;

import io.micrometer.common.util.StringUtils;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterDetails;
import uk.nhs.nhsbsa.xerox.notification.model.NewLetterDetailsRequestDto;
import uk.nhs.nhsbsa.xerox.notification.testsupport.PersonalisationTestDataFactory;

/** Converts {@link LetterDetails} to {@link NewLetterDetailsRequestDto} */
public class LetterDetailsToNewLetterDetailsRequestDtoConverter {
  public static NewLetterDetailsRequestDto convert(LetterDetails letterDetails) {
    if (letterDetails == null) {
      return null;
    }
    return NewLetterDetailsRequestDto.builder()
        .reference(letterDetails.getReference())
        .templateId(letterDetails.getTemplateId().toString())
        .personalisation(letterDetails.getPersonalisation())
        .build();
  }

  public static NewLetterDetailsRequestDto convertMissFieldRequest(
      String reference, String templateId, String personalisation) {
    NewLetterDetailsRequestDto.NewLetterDetailsRequestDtoBuilder builder =
        NewLetterDetailsRequestDto.builder();
    if (reference != null) {
      builder = builder.reference(reference);
    }
    if (templateId != null) {
      builder = builder.templateId(templateId);
    }
    if (personalisation != null) {
      if (StringUtils.isEmpty(personalisation)) {
        builder = builder.personalisation(new HashMap<>());
      } else {
        Class<PersonalisationTestDataFactory> clazz = PersonalisationTestDataFactory.class;
        try {
          Method method = clazz.getDeclaredMethod(personalisation);
          builder = builder.personalisation((Map<String, String>) method.invoke(null));
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
          throw new RuntimeException(e);
        }
      }
    }
    return builder.build();
  }
}
