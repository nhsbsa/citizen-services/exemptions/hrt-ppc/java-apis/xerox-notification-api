package uk.nhs.nhsbsa.xerox.notification.entity;

import static uk.nhs.nhsbsa.exemptions.assertions.ConstraintViolationAssert.assertThat;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.VALIDATION_ERROR_ADDRESS_LESS_THAN_3_MESSAGE;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.VALIDATION_ERROR_ADDRESS_NO_POSTCODE_MESSAGE;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.VALIDATION_ERROR_NOT_EMPTY_MESSAGE;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.VALIDATION_ERROR_NOT_OVER_500_MESSAGE;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterDetailsTestDataFactory.aLetterDetailsWithFailureReason;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterDetailsTestDataFactory.aLetterDetailsWithPersonalisation;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterDetailsTestDataFactory.aLetterDetailsWithReference;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterDetailsTestDataFactory.aLetterDetailsWithStatus;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterDetailsTestDataFactory.aLetterDetailsWithStatusChangeAt;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterDetailsTestDataFactory.aLetterDetailsWithTemplateId;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterDetailsTestDataFactory.aLetterDetailsWithTransactionDate;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterDetailsTestDataFactory.aValidLetterDetails;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterDetailsTestDataFactory.aValidLetterDetailsWithStatusAndStatusChangeAt;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.MetaUtility.getExpectedMetaData;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.PersonalisationTestDataFactory.aAddressLineNumberLessThan3Personalisation;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.PersonalisationTestDataFactory.aNoPostcodePersonalisation;

import jakarta.validation.ConstraintViolation;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Set;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import uk.nhs.nhsbsa.exemptions.assertions.AbstractValidationTest;
import uk.nhs.nhsbsa.xerox.notification.model.Meta;
import uk.nhs.nhsbsa.xerox.notification.model.StatusType;

public class LetterDetailsTest extends AbstractValidationTest {

  @Test
  void shouldValidateLetterDetailsWhenValidLetterDetailsDetailsReceived() {
    // given
    LetterDetails letterDetails = aValidLetterDetails();

    // when
    Set<ConstraintViolation<LetterDetails>> violations = validator.validate(letterDetails);

    // then
    assertThat(violations).hasNoViolations();
  }

  @Test
  void shouldFailToValidatePersonalisationDetailsWithNoPostcode() {
    // given
    LetterDetails letterDetails = aLetterDetailsWithPersonalisation(aNoPostcodePersonalisation());

    // when
    Set<ConstraintViolation<LetterDetails>> violations = validator.validate(letterDetails);

    // then
    assertThat(violations)
        .hasSingleConstraintViolation(
            VALIDATION_ERROR_ADDRESS_NO_POSTCODE_MESSAGE, "personalisation");
  }

  @Test
  void shouldFailToValidatePersonalisationDetailsWithAddressLessThan3() {
    // given
    LetterDetails letterDetails =
        aLetterDetailsWithPersonalisation(aAddressLineNumberLessThan3Personalisation());

    // when
    Set<ConstraintViolation<LetterDetails>> violations = validator.validate(letterDetails);

    // then
    assertThat(violations)
        .hasSingleConstraintViolation(
            VALIDATION_ERROR_ADDRESS_LESS_THAN_3_MESSAGE, "personalisation");
  }

  @Test
  void shouldFailToValidatePersonalisationDetailsWithEmptyPersonalisation() {
    // given
    LetterDetails letterDetails = aLetterDetailsWithPersonalisation(new HashMap<>());

    // when
    Set<ConstraintViolation<LetterDetails>> violations = validator.validate(letterDetails);

    // then
    assertThat(violations)
        .hasSingleConstraintViolation(VALIDATION_ERROR_NOT_EMPTY_MESSAGE, "personalisation");
  }

  @Test
  void shouldFailToValidateLetterDetailsWithNullTransactionDate() {
    // given
    LetterDetails letterDetails = aLetterDetailsWithTransactionDate(null);

    // when
    Set<ConstraintViolation<LetterDetails>> violations = validator.validate(letterDetails);

    // then
    assertThat(violations)
        .hasSingleConstraintViolation(VALIDATION_ERROR_NOT_EMPTY_MESSAGE, "transactionDate");
  }

  @Test
  void shouldFailToValidateLetterDetailsWithNullPersonalisationDetails() {
    // given
    LetterDetails letterDetails = aLetterDetailsWithPersonalisation(null);

    // when
    Set<ConstraintViolation<LetterDetails>> violations = validator.validate(letterDetails);

    // then
    assertThat(violations)
        .hasSingleConstraintViolation(VALIDATION_ERROR_NOT_EMPTY_MESSAGE, "personalisation");
  }

  @Test
  void shouldNotValidateLetterDetailsWithNullReference() {
    // given
    LetterDetails letterDetails = aLetterDetailsWithReference(null);

    // when
    Set<ConstraintViolation<LetterDetails>> violations = validator.validate(letterDetails);

    // then
    assertThat(violations)
        .hasSingleConstraintViolation(VALIDATION_ERROR_NOT_EMPTY_MESSAGE, "reference");
  }

  @Test
  void shouldNotValidateLetterDetailsWithNullStatus() {
    // given
    LetterDetails letterDetails = aLetterDetailsWithStatus(null);

    // when
    Set<ConstraintViolation<LetterDetails>> violations = validator.validate(letterDetails);

    // then
    assertThat(violations)
        .hasSingleConstraintViolation(VALIDATION_ERROR_NOT_EMPTY_MESSAGE, "status");
  }

  @Test
  void shouldNotValidateLetterDetailsWithNullTemplateId() {
    // given
    LetterDetails letterDetails = aLetterDetailsWithTemplateId(null);

    // when
    Set<ConstraintViolation<LetterDetails>> violations = validator.validate(letterDetails);

    // then
    assertThat(violations)
        .hasSingleConstraintViolation(VALIDATION_ERROR_NOT_EMPTY_MESSAGE, "templateId");
  }

  @Test
  void shouldNotValidateLetterDetailsWithNullStatusChangedAt() {
    // given
    LetterDetails letterDetails = aLetterDetailsWithStatusChangeAt(null);

    // when
    Set<ConstraintViolation<LetterDetails>> violations = validator.validate(letterDetails);

    // then
    assertThat(violations)
        .hasSingleConstraintViolation(VALIDATION_ERROR_NOT_EMPTY_MESSAGE, "statusChangeAt");
  }

  @Test
  void shouldNotValidateLetterDetailsWithFailureReasonIsOver500() {
    // given
    LetterDetails letterDetails = aLetterDetailsWithFailureReason("1".repeat(501));

    // when
    Set<ConstraintViolation<LetterDetails>> violations = validator.validate(letterDetails);

    // then
    assertThat(violations)
        .hasSingleConstraintViolation(VALIDATION_ERROR_NOT_OVER_500_MESSAGE, "failureReason");
  }

  @Test
  void shouldReturnMetaWhenItsCalled() {
    // given
    LetterDetails letterDetails = aValidLetterDetails();
    Meta expectedMeta = getExpectedMetaData(letterDetails);

    // when
    Meta actualMeta = letterDetails.getMeta();

    // then
    Assertions.assertThat(actualMeta).isEqualTo(expectedMeta);
  }

  @Test
  void shouldStatusChangeAtNotChangeWhenStatusNotChanged() {
    // given
    LocalDateTime prevStatusChangeAt = LocalDateTime.of(2021, 10, 10, 10, 10);
    LetterDetails letterDetails =
        aValidLetterDetailsWithStatusAndStatusChangeAt(StatusType.PENDING, prevStatusChangeAt);

    // when
    letterDetails.setStatus(StatusType.PENDING);

    // then
    Assertions.assertThat(letterDetails.getStatusChangeAt()).isEqualTo(prevStatusChangeAt);
  }

  @Test
  void shouldStatusChangeAtChangeWhenStatusChanged() {
    // given
    LocalDateTime prevStatusChangeAt = LocalDateTime.of(2021, 10, 10, 10, 10);
    LetterDetails letterDetails =
        aValidLetterDetailsWithStatusAndStatusChangeAt(StatusType.PENDING, prevStatusChangeAt);

    // when
    letterDetails.setStatus(StatusType.SENT);

    // then
    Assertions.assertThat(letterDetails.getStatusChangeAt())
        .isEqualToIgnoringSeconds(LocalDateTime.now());
  }

  @Test
  void shouldRedactLetterDetailsWhenToStringIsCalled() {
    // given
    final var letterDetails = aValidLetterDetails();
    final String expectedToStringOutput = buildExpectedLetterDetailsToString(letterDetails);

    // when
    final String actualToStringOutput = letterDetails.toString();

    // then
    Assertions.assertThat(actualToStringOutput)
        .as(
            "%s toString() does not match expected output",
            letterDetails.getClass().getSimpleName())
        .isEqualTo(expectedToStringOutput);
  }

  private String buildExpectedLetterDetailsToString(LetterDetails letterDetails) {
    return letterDetails.getClass().getSimpleName()
        + " [id="
        + (letterDetails.getId() == null ? "" : letterDetails.getId())
        + ", meta="
        + getExpectedMetaData(letterDetails)
        + ", transactionDate="
        + letterDetails.getTransactionDate()
        + ", personalisation=*********************************************************************************************************************************************************************************************************************************************************************************************"
        + ", reference="
        + letterDetails.getReference()
        + ", correlationId="
        + (letterDetails.getCorrelationId() == null ? "" : letterDetails.getCorrelationId())
        + ", fileName="
        + letterDetails.getFileName()
        + ", status="
        + letterDetails.getStatus()
        + ", failureReason="
        + (letterDetails.getFailureReason() == null ? "" : letterDetails.getFailureReason())
        + ", statusChangeAt="
        + letterDetails.getStatusChangeAt()
        + "]";
  }
}
