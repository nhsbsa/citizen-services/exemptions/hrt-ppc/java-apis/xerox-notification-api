package uk.nhs.nhsbsa.xerox.notification.testsupport;

import java.net.URI;
import uk.nhs.nhsbsa.exemptions.model.Links.LinkHolder;
import uk.nhs.nhsbsa.xerox.notification.model.Links;

public class LinksTestDataFactory {

  public static Links aValidLinksBuilderWithASelfLinkAndTemplate(int port, String templateId) {
    LinkHolder aLink =
        LinkHolder.builder()
            .href("http://localhost:" + port + "/v1/xerox-notifications/templates/" + templateId)
            .build();
    return Links.builder().xeroxTemplate(aLink).selfLink(aLink).build();
  }

  public static LinkHolder aValidLinkHolderWithASearchLink(
      int port, URI searchUri, String page, String size) {
    LinkHolder searchLinkHolder =
        LinkHolder.builder()
            .href("http://localhost:" + port + searchUri + "&page=" + page + "&size=" + size)
            .build();
    return searchLinkHolder;
  }

  public static Links aValidLinksBuilderWithTemplateAndLetterDetails(
      int port, String letterDetailId, String templateId) {
    LinkHolder templateLink =
        LinkHolder.builder()
            .href("http://localhost:" + port + "/v1/xerox-notifications/templates/" + templateId)
            .build();
    LinkHolder letterLink =
        LinkHolder.builder()
            .href("http://localhost:" + port + "/v1/xerox-notifications/letter/" + letterDetailId)
            .build();
    return Links.builder()
        .selfLink(letterLink)
        .letterDetails(letterLink)
        .xeroxTemplate(templateLink)
        .build();
  }
}
