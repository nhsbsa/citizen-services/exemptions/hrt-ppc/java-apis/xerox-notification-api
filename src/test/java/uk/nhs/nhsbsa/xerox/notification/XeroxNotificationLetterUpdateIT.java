package uk.nhs.nhsbsa.xerox.notification;

import static java.util.Comparator.comparing;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static uk.nhs.nhsbsa.exemptions.TestConstants.CREATED_BY;
import static uk.nhs.nhsbsa.exemptions.TestConstants.UPDATED_BY;
import static uk.nhs.nhsbsa.exemptions.assertions.TestAssertions.assertErrorInResponse;
import static uk.nhs.nhsbsa.exemptions.assertions.TestAssertions.assertFieldError;
import static uk.nhs.nhsbsa.exemptions.utilities.ComparisonUtil.recursiveComparisonConfiguration;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.BAD_REQUEST_ERROR_MESSAGE;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.LETTER_DETAILS_ID;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.REFERENCE;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.STATUS;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.TEMPLATE_ID;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.TRANSACTION_DATE;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.VALIDATION_ERROR_NOT_EMPTY_MESSAGE;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.VALIDATION_ERROR_NOT_OVER_100_MESSAGE;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.VALIDATION_ERROR_NOT_VALID_DATE;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.VALIDATION_ERROR_NOT_VALID_STATUS;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.DataStorageUtils.saveALetterDetails;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.DataStorageUtils.saveLetterDetailsWithTemplateIdAndReference;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.DataStorageUtils.saveLetterTemplate;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.UpdateLetterDetailsTestDataFactory.aValidLetterDetailsUpdateDto;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.UpdateLetterDetailsTestDataFactory.aValidLetterDetailsUpdateDtoFromLetterDetails;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.UpdateLetterDetailsTestDataFactory.aValidLetterDetailsUpdateDtoWithOnlyReference;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.UpdateLetterDetailsTestDataFactory.aValidLetterDetailsUpdateDtoWithOnlyStatus;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.UpdateLetterDetailsTestDataFactory.aValidLetterDetailsUpdateDtoWithOnlyTransactionDate;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.UpdateLetterDetailsTestDataFactory.aValidLetterDetailsUpdateDtoWithSameValue;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.UpdateLetterDetailsTestDataFactory.buildLetterDetailsUpdateDto;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.XeroxHeadersTestDataFactory.validXeroxRequestHeaders;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import org.hibernate.exception.SQLGrammarException;
import org.javers.core.Changes;
import org.javers.repository.jql.QueryBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockReset;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import uk.nhs.nhsbsa.exemptions.models.ErrorResponse;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterDetails;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterTemplate;
import uk.nhs.nhsbsa.xerox.notification.model.LetterDetailsUpdateDto;
import uk.nhs.nhsbsa.xerox.notification.model.StatusType;
import uk.nhs.nhsbsa.xerox.notification.model.XeroxNotificationApplicationRequestContext;
import uk.nhs.nhsbsa.xerox.notification.repository.LetterDetailsRepository;
import uk.nhs.nhsbsa.xerox.notification.repository.LetterTemplateRepository;
import uk.nhs.nhsbsa.xerox.notification.security.SpringSecurityAuditorAware;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureWebTestClient
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class XeroxNotificationLetterUpdateIT extends AbstractBaseIT {

  @Mock
  private XeroxNotificationApplicationRequestContext xeroxNotificationApplicationRequestContext;

  @Autowired private SpringSecurityAuditorAware springSecurityAuditorAware;

  @SpyBean(reset = MockReset.AFTER)
  LetterDetailsRepository letterDetailsRepository;

  @SpyBean(reset = MockReset.AFTER)
  private LetterTemplateRepository letterTemplateRepository;

  @BeforeEach
  public void setup() {
    MockHttpServletRequest request = new MockHttpServletRequest();
    RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(CREATED_BY);
    ReflectionTestUtils.setField(
        springSecurityAuditorAware,
        "xeroxNotificationApplicationRequestContext",
        xeroxNotificationApplicationRequestContext);
    letterDetailsRepository.deleteAll();
    letterDetailsRepository.flush();
    letterTemplateRepository.deleteAll();
    letterTemplateRepository.flush();
  }

  @AfterEach
  public void tearDown() {
    RequestContextHolder.resetRequestAttributes();
  }

  @Test
  void shouldUpdateValidLetterDetailsAndReturnSuccessResponse() {
    // given
    LetterDetails letterDetailsOnCreation =
        saveALetterDetails(
            200, StatusType.PENDING, letterDetailsRepository, letterTemplateRepository);
    requestHeaders = validXeroxRequestHeaders(UPDATED_BY);
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(UPDATED_BY);
    LetterDetailsUpdateDto letterDetailsUpdateDto = aValidLetterDetailsUpdateDto();

    // when
    ResponseEntity response =
        patchLetterDetailsUpdateRequest(
            letterDetailsUpdateDto,
            requestHeaders,
            letterDetailsOnCreation.getId().toString(),
            Object.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(NO_CONTENT);
    assertThat(response.getBody()).isNull();
    assertUpdateResults(letterDetailsOnCreation, letterDetailsUpdateDto, null);
  }

  @ParameterizedTest
  @CsvSource({
    "aValidLetterDetailsUpdateDtoWithOnlyReference,test12122",
    "aValidLetterDetailsUpdateDtoWithOnlyTransactionDate,2000-01-01",
    "aValidLetterDetailsUpdateDtoWithOnlyStatus,SENT",
    "aValidLetterDetailsUpdateDtoWithOnlyFileName,HRT_PPC_LETTER_20230601321021.xml",
    "aValidLetterDetailsUpdateDtoWithOnlyFailureReason,Invalid address"
  })
  void shouldUpdateOnlyOneFieldInLetterDetailsAndReturnSuccessResponse(
      String builderName, String builderValue) {
    // given
    String updator =
        "User" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyMMddHHmmssSSS"));
    LetterDetails letterDetailsOnCreation =
        saveALetterDetails(
            200, StatusType.PENDING, letterDetailsRepository, letterTemplateRepository);
    requestHeaders = validXeroxRequestHeaders(updator);
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(updator);
    LetterDetailsUpdateDto letterDetailsUpdateDto =
        buildLetterDetailsUpdateDto(builderName, builderValue);

    // when
    ResponseEntity<String> response =
        patchLetterDetailsUpdateRequest(
            letterDetailsUpdateDto,
            requestHeaders,
            letterDetailsOnCreation.getId().toString(),
            String.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(NO_CONTENT);
    assertThat(response.getBody()).isNull();
    assertUpdateResults(letterDetailsOnCreation, letterDetailsUpdateDto, updator);
  }

  @Test
  void shouldReturnBadRequestWhenBodyParametersAreNull() {
    // given
    LetterDetails letterDetailsOnCreation =
        saveALetterDetails(
            200, StatusType.PENDING, letterDetailsRepository, letterTemplateRepository);
    requestHeaders = validXeroxRequestHeaders(UPDATED_BY);
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(UPDATED_BY);
    LetterDetailsUpdateDto letterDetailsUpdateDto = aValidLetterDetailsUpdateDtoWithSameValue(null);

    // when
    ResponseEntity<ErrorResponse> response =
        patchLetterDetailsUpdateRequest(
            letterDetailsUpdateDto,
            requestHeaders,
            letterDetailsOnCreation.getId().toString(),
            ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, "There were validation issues with the request.");
    List<ErrorResponse.FieldError> fieldErrors = response.getBody().getFieldErrors();
    assertThat(fieldErrors).hasSize(3);
    Collections.sort(fieldErrors, comparing(ErrorResponse.FieldError::getField));
    assertFieldError(fieldErrors.get(0), REFERENCE, VALIDATION_ERROR_NOT_EMPTY_MESSAGE);
    assertFieldError(fieldErrors.get(1), STATUS, VALIDATION_ERROR_NOT_EMPTY_MESSAGE);
    assertFieldError(fieldErrors.get(2), TRANSACTION_DATE, VALIDATION_ERROR_NOT_EMPTY_MESSAGE);
  }

  @ParameterizedTest
  @EmptySource
  @ValueSource(strings = {"PPC", "SENT1", "pending"})
  void shouldReturnBadRequestWhenInvalidStatus(String status) {
    // given
    LetterDetails letterDetailsOnCreation =
        saveALetterDetails(
            200, StatusType.PENDING, letterDetailsRepository, letterTemplateRepository);
    requestHeaders = validXeroxRequestHeaders(UPDATED_BY);
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(UPDATED_BY);
    LetterDetailsUpdateDto letterDetailsUpdateDto =
        aValidLetterDetailsUpdateDtoWithOnlyStatus(status);

    // when
    ResponseEntity<ErrorResponse> response =
        patchLetterDetailsUpdateRequest(
            letterDetailsUpdateDto,
            requestHeaders,
            letterDetailsOnCreation.getId().toString(),
            ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, "There were validation issues with the request.");
    List<ErrorResponse.FieldError> fieldErrors = response.getBody().getFieldErrors();
    assertThat(fieldErrors).hasSize(1);
    assertFieldError(fieldErrors.get(0), STATUS, VALIDATION_ERROR_NOT_VALID_STATUS);
  }

  @Test
  void shouldReturnBadRequestWhenReferenceOver100() {
    // given
    LetterDetails letterDetailsOnCreation =
        saveALetterDetails(
            200, StatusType.PENDING, letterDetailsRepository, letterTemplateRepository);
    requestHeaders = validXeroxRequestHeaders(UPDATED_BY);
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(UPDATED_BY);
    LetterDetailsUpdateDto letterDetailsUpdateDto =
        aValidLetterDetailsUpdateDtoWithOnlyReference("1".repeat(101));

    // when
    ResponseEntity<ErrorResponse> response =
        patchLetterDetailsUpdateRequest(
            letterDetailsUpdateDto,
            requestHeaders,
            letterDetailsOnCreation.getId().toString(),
            ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, "There were validation issues with the request.");
    List<ErrorResponse.FieldError> fieldErrors = response.getBody().getFieldErrors();
    assertThat(fieldErrors).hasSize(1);
    assertFieldError(fieldErrors.get(0), REFERENCE, VALIDATION_ERROR_NOT_OVER_100_MESSAGE);
  }

  @ParameterizedTest
  @ValueSource(strings = {"01-01-2000", "20000101", "01/01/2000", "2020.01.01"})
  void shouldReturnBadRequestWhenInvalidTransactionDate(String transactionDate) {
    // given
    LetterDetails letterDetailsOnCreation =
        saveALetterDetails(
            200, StatusType.PENDING, letterDetailsRepository, letterTemplateRepository);
    requestHeaders = validXeroxRequestHeaders(UPDATED_BY);
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(UPDATED_BY);
    LetterDetailsUpdateDto letterDetailsUpdateDto =
        aValidLetterDetailsUpdateDtoWithOnlyTransactionDate(transactionDate);

    // when
    ResponseEntity<ErrorResponse> response =
        patchLetterDetailsUpdateRequest(
            letterDetailsUpdateDto,
            requestHeaders,
            letterDetailsOnCreation.getId().toString(),
            ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, "There were validation issues with the request.");
    List<ErrorResponse.FieldError> fieldErrors = response.getBody().getFieldErrors();
    assertThat(fieldErrors).hasSize(1);
    assertFieldError(fieldErrors.get(0), TRANSACTION_DATE, VALIDATION_ERROR_NOT_VALID_DATE);
  }

  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource(strings = {"aaaaaaaaaahhhhhhhhhhhhslllllllllllllllllllllll4785960684373737"})
  void shouldReturnBadRequestWhenUserIdHeaderMissingOrInvalid(String user) {
    // given
    LetterDetails letterDetailsOnCreation =
        saveALetterDetails(
            200, StatusType.PENDING, letterDetailsRepository, letterTemplateRepository);
    requestHeaders = validXeroxRequestHeaders(user);
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(UPDATED_BY);
    LetterDetailsUpdateDto letterDetailsUpdateDto = aValidLetterDetailsUpdateDto();

    // when
    ResponseEntity<ErrorResponse> response =
        patchLetterDetailsUpdateRequest(
            letterDetailsUpdateDto,
            requestHeaders,
            letterDetailsOnCreation.getId().toString(),
            ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, BAD_REQUEST_ERROR_MESSAGE);
  }

  @ParameterizedTest
  @ValueSource(strings = {"abc", "xxxxx-xxxx-xxxxx-xxxxxxx"})
  void shouldReturnBadRequestWhenLetterIdInvalid(String letterDetailsId) {
    // given
    saveALetterDetails(200, StatusType.PENDING, letterDetailsRepository, letterTemplateRepository);
    requestHeaders = validXeroxRequestHeaders(UPDATED_BY);
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(UPDATED_BY);
    LetterDetailsUpdateDto letterDetailsUpdateDto = aValidLetterDetailsUpdateDto();

    // when
    ResponseEntity<ErrorResponse> response =
        patchLetterDetailsUpdateRequest(
            letterDetailsUpdateDto, requestHeaders, letterDetailsId, ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, BAD_REQUEST_ERROR_MESSAGE);
  }

  @Test
  void shouldReturnBadRequestWhenBodyIsNull() {
    // given
    LetterDetails letterDetailsOnCreation =
        saveALetterDetails(
            200, StatusType.PENDING, letterDetailsRepository, letterTemplateRepository);
    requestHeaders = validXeroxRequestHeaders(UPDATED_BY);
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(UPDATED_BY);

    // when
    ResponseEntity<ErrorResponse> response =
        patchLetterDetailsUpdateRequest(
            null, requestHeaders, letterDetailsOnCreation.getId().toString(), ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, BAD_REQUEST_ERROR_MESSAGE);
  }

  @Test
  void shouldReturnNotFoundAndRecordNoChangeWhenLetterIdNotFound() {
    // given
    LetterDetails letterDetailsOnCreation =
        saveALetterDetails(
            200, StatusType.PENDING, letterDetailsRepository, letterTemplateRepository);
    requestHeaders = validXeroxRequestHeaders(UPDATED_BY);
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(UPDATED_BY);

    // when
    ResponseEntity<ErrorResponse> response =
        patchLetterDetailsUpdateRequest(
            null, requestHeaders, UUID.randomUUID().toString(), ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(NOT_FOUND);
    assertThat(response.getBody()).isNull();
    Optional<LetterDetails> updatedLetterDetailsResult =
        letterDetailsRepository.findById(letterDetailsOnCreation.getId());
    assertThat(updatedLetterDetailsResult).isPresent();
    LetterDetails updatedLetterDetails = updatedLetterDetailsResult.get();
    assertThat(updatedLetterDetails)
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(letterDetailsOnCreation);
  }

  @Test
  void shouldReturnDuplicateRecordsExceptionWhenUpdateInformationDuplicatedExistedRecord() {
    // given
    LetterTemplate letterTemplate = saveLetterTemplate(200, letterTemplateRepository);
    LetterDetails existedLetterDetailsOnCreation =
        saveLetterDetailsWithTemplateIdAndReference(
            "HRTABCD123", letterDetailsRepository, letterTemplate.getId());
    LetterDetails duplicatedLetterDetailsOnCreation =
        saveLetterDetailsWithTemplateIdAndReference(
            "HRTABCD1234", letterDetailsRepository, letterTemplate.getId());
    LetterDetailsUpdateDto letterDetailsUpdateDto =
        aValidLetterDetailsUpdateDtoFromLetterDetails(existedLetterDetailsOnCreation);
    requestHeaders = validXeroxRequestHeaders(UPDATED_BY);
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(UPDATED_BY);

    // when
    ResponseEntity<ErrorResponse> response =
        patchLetterDetailsUpdateRequest(
            letterDetailsUpdateDto,
            requestHeaders,
            duplicatedLetterDetailsOnCreation.getId().toString(),
            ErrorResponse.class);

    // then
    assertThat(response.getBody().getMessage())
        .contains(
            "could not execute statement [ERROR: duplicate key value violates unique constraint \"transaction_dt_letter_template_id_reference_unique");
  }

  @Test
  void shouldReturnInternalServerErrorWhenExceptionOccurs() {
    // given
    LetterDetails letterDetailsOnCreation =
        saveALetterDetails(
            200, StatusType.PENDING, letterDetailsRepository, letterTemplateRepository);
    requestHeaders = validXeroxRequestHeaders(UPDATED_BY);
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(UPDATED_BY);
    LetterDetailsUpdateDto letterDetailsUpdateDto = aValidLetterDetailsUpdateDto();
    SQLGrammarException sqlGrammarException = new SQLGrammarException("sql exception", null);
    InvalidDataAccessResourceUsageException exception =
        new InvalidDataAccessResourceUsageException("invalid data", sqlGrammarException);
    doThrow(exception).when(letterDetailsRepository).save(any(LetterDetails.class));

    // when
    ResponseEntity<ErrorResponse> response =
        patchLetterDetailsUpdateRequest(
            letterDetailsUpdateDto,
            requestHeaders,
            letterDetailsOnCreation.getId().toString(),
            ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(INTERNAL_SERVER_ERROR);
    assertErrorInResponse(response, INTERNAL_SERVER_ERROR, "invalid data");
  }

  private void assertUpdateResults(
      LetterDetails letterDetailsOnCreation,
      LetterDetailsUpdateDto letterDetailsUpdateDto,
      String author) {
    assertJaversRecord(
        Map.of(
            LETTER_DETAILS_ID,
            letterDetailsOnCreation.getId().toString(),
            REFERENCE,
            letterDetailsUpdateDto.getReference() == null
                ? letterDetailsOnCreation.getReference()
                : letterDetailsUpdateDto.getReference().get(),
            TEMPLATE_ID,
            letterDetailsOnCreation.getTemplateId().toString()),
        author);
    Optional<LetterDetails> updatedLetterDetailsResult =
        letterDetailsRepository.findById(letterDetailsOnCreation.getId());
    assertThat(updatedLetterDetailsResult).isPresent();
    LetterDetails updatedLetterDetails = updatedLetterDetailsResult.get();
    assertThat(updatedLetterDetails)
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .ignoringFields(
            "versionNumber",
            "failureReason",
            "fileName",
            "reference",
            "status",
            "transactionDate",
            "updatedBy")
        .isEqualTo(letterDetailsOnCreation);
    assertThat(updatedLetterDetails.getVersionNumber()).isOne();
    assertThat(updatedLetterDetails.getReference())
        .isEqualTo(
            letterDetailsUpdateDto.getReference() == null
                ? letterDetailsOnCreation.getReference()
                : letterDetailsUpdateDto.getReference().get());
    assertThat(updatedLetterDetails.getStatus().name())
        .isEqualTo(
            letterDetailsUpdateDto.getStatus() == null
                ? letterDetailsOnCreation.getStatus().name()
                : letterDetailsUpdateDto.getStatus().get());
    assertThat(updatedLetterDetails.getTransactionDate().toString())
        .isEqualTo(
            letterDetailsUpdateDto.getTransactionDate() == null
                ? letterDetailsOnCreation.getTransactionDate().toString()
                : letterDetailsUpdateDto.getTransactionDate().get());
    assertThat(updatedLetterDetails.getFailureReason())
        .isEqualTo(
            letterDetailsUpdateDto.getFailureReason() == null
                ? letterDetailsOnCreation.getFailureReason()
                : letterDetailsUpdateDto.getFailureReason());
    assertThat(updatedLetterDetails.getFileName())
        .isEqualTo(
            letterDetailsUpdateDto.getFileName() == null
                ? letterDetailsOnCreation.getFileName()
                : letterDetailsUpdateDto.getFileName());
    assertThat(updatedLetterDetails.getUpdatedTimestamp())
        .isAfter(letterDetailsOnCreation.getUpdatedTimestamp());
    assertThat(updatedLetterDetails.getUpdatedBy()).isEqualTo(author == null ? UPDATED_BY : author);
  }

  private void assertJaversRecord(Map<String, String> query, String author) {
    QueryBuilder jqlQuery = QueryBuilder.byClass(LetterDetails.class);
    if (author != null) {
      jqlQuery.byAuthor(author);
    }
    query
        .entrySet()
        .forEach(entry -> jqlQuery.withCommitProperty(entry.getKey(), entry.getValue()));
    Changes changes = javers.findChanges(jqlQuery.build());
    assertThat(changes).isNotEmpty();
    changes.forEach(
        change -> {
          assertThat(change.getCommitMetadata()).isPresent();
          var meta = change.getCommitMetadata().get();
          assertThat(meta.getAuthor()).isEqualTo(author == null ? UPDATED_BY : author);
          assertThat(meta.getProperties()).isNotEmpty();
        });
  }

  private <T> ResponseEntity patchLetterDetailsUpdateRequest(
      T letterDetailsUpdateDto,
      HttpHeaders requestHeaders,
      String letterDetailsId,
      Class expectedType) {
    StringBuilder urlBuilder =
        new StringBuilder(String.format("/v1/xerox-notifications/letter/%1$s", letterDetailsId));
    RequestEntity<?> requestEntity =
        new RequestEntity<>(
            letterDetailsUpdateDto,
            requestHeaders,
            HttpMethod.PATCH,
            URI.create(urlBuilder.toString()));
    return testRestTemplate.exchange(requestEntity, expectedType);
  }
}
