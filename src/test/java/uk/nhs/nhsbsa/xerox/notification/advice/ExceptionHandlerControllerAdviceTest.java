package uk.nhs.nhsbsa.xerox.notification.advice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;
import static uk.nhs.nhsbsa.exemptions.assertions.TestAssertions.assertErrorInResponse;

import jakarta.servlet.http.HttpServletRequest;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import uk.nhs.nhsbsa.exemptions.exception.InvalidHeaderException;
import uk.nhs.nhsbsa.exemptions.exception.InvalidQueryParameterException;
import uk.nhs.nhsbsa.exemptions.models.ErrorResponse;
import uk.nhs.nhsbsa.xerox.notification.model.TestConstants;

@ExtendWith(MockitoExtension.class)
public class ExceptionHandlerControllerAdviceTest {

  @Mock private HttpServletRequest httpServletRequest;

  @InjectMocks ExceptionHandlerControllerAdvice exceptionHandlerControllerAdvice;

  @Test
  void shouldReturnBadRequestWithoutFieldErrorWhenHttpMessageNotReadableExceptionIsReceived() {
    // given
    HttpMessageNotReadableException exception = mock(HttpMessageNotReadableException.class);

    // when
    ResponseEntity<ErrorResponse> responseEntity =
        exceptionHandlerControllerAdvice.handleInvalidHeaderOrParamAndIllegalArgumentException(
            exception);

    // then
    assertErrorInResponse(responseEntity, BAD_REQUEST, TestConstants.BAD_REQUEST_ERROR_MESSAGE);
    List<ErrorResponse.FieldError> fieldErrors = responseEntity.getBody().getFieldErrors();
    assertThat(fieldErrors).isNull();
  }

  @Test
  void shouldReturnBadRequestWithoutFieldErrorWhenInvalidHeaderExceptionIsReceived() {
    // given
    InvalidHeaderException exception = mock(InvalidHeaderException.class);

    // when
    ResponseEntity<ErrorResponse> responseEntity =
        exceptionHandlerControllerAdvice.handleInvalidHeaderOrParamAndIllegalArgumentException(
            exception);

    // then
    assertErrorInResponse(responseEntity, BAD_REQUEST, TestConstants.BAD_REQUEST_ERROR_MESSAGE);
    List<ErrorResponse.FieldError> fieldErrors = responseEntity.getBody().getFieldErrors();
    assertThat(fieldErrors).isNull();
  }

  @Test
  void shouldReturnBadRequestWithoutFieldErrorWhenIllegalArgumentExceptionIsReceived() {
    // given
    IllegalArgumentException exception = mock(IllegalArgumentException.class);

    // when
    ResponseEntity<ErrorResponse> responseEntity =
        exceptionHandlerControllerAdvice.handleInvalidHeaderOrParamAndIllegalArgumentException(
            exception);

    // then
    assertErrorInResponse(responseEntity, BAD_REQUEST, TestConstants.BAD_REQUEST_ERROR_MESSAGE);
    ErrorResponse errorResponse = responseEntity.getBody();
    List<ErrorResponse.FieldError> fieldErrors = errorResponse.getFieldErrors();
    assertThat(fieldErrors).isNull();
  }

  @Test
  void shouldReturnBadRequestWithoutFieldErrorWhenInvalidQueryParameterExceptionIsReceived() {
    // given
    InvalidQueryParameterException exception = mock(InvalidQueryParameterException.class);

    // when
    ResponseEntity<ErrorResponse> responseEntity =
        exceptionHandlerControllerAdvice.handleInvalidHeaderOrParamAndIllegalArgumentException(
            exception);

    // then
    assertErrorInResponse(responseEntity, BAD_REQUEST, TestConstants.BAD_REQUEST_ERROR_MESSAGE);
    ErrorResponse errorResponse = responseEntity.getBody();
    List<ErrorResponse.FieldError> fieldErrors = errorResponse.getFieldErrors();
    assertThat(fieldErrors).isNull();
  }

  @Test
  void shouldReturnConflictErrorWithoutFieldErrorWhenDataIntegrityViolationExceptionIsReceived() {
    // given
    DataIntegrityViolationException exception =
        new DataIntegrityViolationException(
            "",
            new RuntimeException(
                "",
                new RuntimeException(
                    "",
                    new RuntimeException(
                        "ERROR: duplicate key value violates unique constraint"))));

    // when
    ResponseEntity<ErrorResponse> responseEntity =
        exceptionHandlerControllerAdvice.handleDataIntegrityViolationException(exception);

    // then
    assertErrorInResponse(
        responseEntity, CONFLICT, "ERROR: duplicate key value violates unique constraint");
    ErrorResponse errorResponse = responseEntity.getBody();
    List<ErrorResponse.FieldError> fieldErrors = errorResponse.getFieldErrors();
    assertThat(fieldErrors).isNull();
  }
}
