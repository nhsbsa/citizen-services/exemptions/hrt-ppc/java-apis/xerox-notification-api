package uk.nhs.nhsbsa.xerox.notification.testsupport;

import static uk.nhs.nhsbsa.xerox.notification.testsupport.PersonalisationTestDataFactory.aPersonalisation;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;
import uk.nhs.nhsbsa.exemptions.TestConstants;
import uk.nhs.nhsbsa.exemptions.models.ApplicationChannel;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterDetails;
import uk.nhs.nhsbsa.xerox.notification.model.StatusType;

public class LetterDetailsTestDataFactory {

  public static LetterDetails aValidLetterDetailsWithIdTemplateIdAndReference(
      UUID id, UUID templateId, String reference) {
    return LetterDetails.builder().id(id).templateId(templateId).reference(reference).build();
  }

  public static LetterDetails aValidLetterDetails() {
    return aValidLetterDetailsBuilder()
        .transactionDate(LocalDate.now())
        .statusChangeAt(LocalDateTime.now())
        .build();
  }

  public static LetterDetails aValidLetterDetailsWithStatus(StatusType status) {
    return aValidLetterDetailsBuilder()
        .transactionDate(LocalDate.now())
        .statusChangeAt(LocalDateTime.now())
        .status(status)
        .build();
  }

  public static LetterDetails aValidLetterDetailsWithStatusAndStatusChangeAt(
      StatusType status, LocalDateTime statusChangeAt) {
    return aValidLetterDetailsBuilder()
        .transactionDate(LocalDate.now())
        .statusChangeAt(statusChangeAt)
        .status(status)
        .build();
  }

  public static LetterDetails aLetterDetailsWithFailureReason(String failureReason) {
    return aValidLetterDetailsBuilder()
        .transactionDate(LocalDate.now())
        .statusChangeAt(LocalDateTime.now())
        .failureReason(failureReason)
        .build();
  }

  public static LetterDetails aLetterDetailsWithStatusChangeAt(LocalDateTime statusChangeAt) {
    return aValidLetterDetailsBuilder()
        .transactionDate(LocalDate.now())
        .statusChangeAt(statusChangeAt)
        .build();
  }

  public static LetterDetails aLetterDetailsWithTemplateId(UUID templateId) {
    return aValidLetterDetailsBuilder()
        .transactionDate(LocalDate.now())
        .statusChangeAt(LocalDateTime.now())
        .templateId(templateId)
        .build();
  }

  public static LetterDetails aLetterDetailsWithReference(String reference) {
    return aValidLetterDetailsBuilder()
        .transactionDate(LocalDate.now())
        .statusChangeAt(LocalDateTime.now())
        .reference(reference)
        .build();
  }

  public static LetterDetails aLetterDetailsWithStatus(StatusType statusType) {
    return aValidLetterDetailsBuilder()
        .transactionDate(LocalDate.now())
        .statusChangeAt(LocalDateTime.now())
        .status(statusType)
        .build();
  }

  public static LetterDetails aLetterDetailsWithPersonalisation(
      Map<String, String> personalisation) {
    return aValidLetterDetailsBuilder()
        .transactionDate(LocalDate.now())
        .statusChangeAt(LocalDateTime.now())
        .personalisation(personalisation)
        .build();
  }

  public static LetterDetails aValidLetterDetailsWithTemplateId(UUID templateId) {
    return aValidLetterDetailsBuilder()
        .transactionDate(LocalDate.now())
        .statusChangeAt(LocalDateTime.now())
        .templateId(templateId)
        .build();
  }

  public static LetterDetails aLetterDetailsWithTransactionDate(LocalDate transactionDate) {
    return aValidLetterDetailsBuilder()
        .transactionDate(transactionDate)
        .statusChangeAt(LocalDateTime.now())
        .build();
  }

  public static LetterDetails aValidLetterDetailsWithTemplateIdWithoutTransactionDate(
      UUID templateId) {
    return aValidLetterDetailsBuilder().templateId(templateId).transactionDate(null).build();
  }

  public static LetterDetails aLetterDetailsWithAllFields(
      UUID letterTemplateId,
      LocalDate transactionDate,
      Map<String, String> personalisationDetails,
      String reference,
      String fileName,
      StatusType statusType,
      LocalDateTime statusChangeAt,
      ApplicationChannel applicationChannel) {
    return LetterDetails.builder()
        .templateId(letterTemplateId)
        .transactionDate(transactionDate)
        .personalisation(personalisationDetails)
        .reference(reference)
        .fileName(fileName)
        .status(statusType)
        .statusChangeAt(statusChangeAt)
        .createdBy(applicationChannel.name())
        .build();
  }

  private static LetterDetails.LetterDetailsBuilder<?, ?> aValidLetterDetailsBuilder() {
    return LetterDetails.builder()
        .templateId(UUID.randomUUID())
        .personalisation(aPersonalisation())
        .reference("5136654")
        .fileName("HRT_PPC_LETTER_20230401221021.xml")
        .status(StatusType.PENDING)
        .createdTimestamp(LocalDateTime.now())
        .createdBy(TestConstants.CREATED_BY)
        .updatedTimestamp(LocalDateTime.now())
        .updatedBy(TestConstants.CREATED_BY);
  }
}
