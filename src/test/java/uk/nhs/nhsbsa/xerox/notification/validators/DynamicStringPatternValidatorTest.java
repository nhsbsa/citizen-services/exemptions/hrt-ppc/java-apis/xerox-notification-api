package uk.nhs.nhsbsa.xerox.notification.validators;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.SERVICE_NAME_HRT_PPC;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.VALIDATION_KEY;

import jakarta.validation.ConstraintValidatorContext;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DynamicStringPatternValidatorTest {

  @Mock DynamicPattern dynamicPattern;
  @Mock ConstraintValidatorContext context;
  private static MockedStatic<DynamicStringProperties> utilities;

  @BeforeEach
  public void init() {
    utilities = Mockito.mockStatic(DynamicStringProperties.class);
  }

  @AfterEach
  public void close() {
    utilities.close();
  }

  @Test
  void shouldBeTrueWhenValueIsValid() {
    // given
    given(dynamicPattern.patternKey()).willReturn(VALIDATION_KEY);
    utilities
        .when(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY))
        .thenReturn(SERVICE_NAME_HRT_PPC);

    // when
    DynamicStringPatternValidator validator = new DynamicStringPatternValidator();
    validator.initialize(dynamicPattern);
    boolean result = validator.isValid(SERVICE_NAME_HRT_PPC, context);

    // then
    assertThat(result).isTrue();
    verify(dynamicPattern).patternKey();
    verifyNoMoreInteractions(dynamicPattern);
    verifyNoInteractions(context);
    utilities.verify(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY));
  }

  @Test
  void shouldBeFalseWhenValueIsInValid() {
    // given
    given(dynamicPattern.patternKey()).willReturn(VALIDATION_KEY);
    utilities
        .when(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY))
        .thenReturn(SERVICE_NAME_HRT_PPC);

    // when
    DynamicStringPatternValidator validator = new DynamicStringPatternValidator();
    validator.initialize(dynamicPattern);
    boolean result = validator.isValid("PPC2", context);

    // then
    assertThat(result).isFalse();
    verify(dynamicPattern).patternKey();
    verifyNoMoreInteractions(dynamicPattern);
    verifyNoInteractions(context);
    utilities.verify(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY));
  }

  @ParameterizedTest
  @NullAndEmptySource
  void shouldBeTrueWhenValueIsNullOrEmpty(String value) {
    // given
    given(dynamicPattern.patternKey()).willReturn(VALIDATION_KEY);
    utilities
        .when(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY))
        .thenReturn(SERVICE_NAME_HRT_PPC);

    // when
    DynamicStringPatternValidator validator = new DynamicStringPatternValidator();
    validator.initialize(dynamicPattern);
    boolean result = validator.isValid(value, context);

    // then
    assertThat(result).isTrue();
    verify(dynamicPattern).patternKey();
    verifyNoMoreInteractions(dynamicPattern);
    verifyNoInteractions(context);
    utilities.verify(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY));
  }

  @ParameterizedTest
  @NullAndEmptySource
  void shouldBeFalseWhenPatternKeyIsNullOrEmpty(String patternKey) {
    // given
    given(dynamicPattern.patternKey()).willReturn(patternKey);

    // when
    DynamicStringPatternValidator validator = new DynamicStringPatternValidator();
    validator.initialize(dynamicPattern);
    boolean result = validator.isValid(SERVICE_NAME_HRT_PPC, context);

    // then
    assertThat(result).isFalse();
    verify(dynamicPattern).patternKey();
    verifyNoMoreInteractions(dynamicPattern);
    verifyNoInteractions(context);
    utilities.verify(() -> DynamicStringProperties.getServiceName(VALIDATION_KEY), never());
  }
}
