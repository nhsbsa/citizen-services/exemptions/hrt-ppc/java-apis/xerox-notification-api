package uk.nhs.nhsbsa.xerox.notification.interceptors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static uk.nhs.nhsbsa.xerox.notification.model.Constants.HEADER_PARAM_CORRELATION_ID;
import static uk.nhs.nhsbsa.xerox.notification.model.Constants.HEADER_PARAM_USER_ID;
import static uk.nhs.nhsbsa.xerox.notification.model.Constants.XEROX_NOTIFICATION_API_CREATION_LETTER_URI;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpMethod;
import uk.nhs.nhsbsa.xerox.notification.interceptor.XeroxNotificationApplicationRequestContextInterceptor;
import uk.nhs.nhsbsa.xerox.notification.model.StatusType;
import uk.nhs.nhsbsa.xerox.notification.model.XeroxNotificationApplicationRequestContext;

@ExtendWith(MockitoExtension.class)
public class XeroxNotificationApplicationRequestContextInterceptorTest {

  @Mock private HttpServletRequest request;
  @Mock private HttpServletResponse response;

  @Mock
  private XeroxNotificationApplicationRequestContext xeroxNotificationApplicationRequestContext;

  @InjectMocks private XeroxNotificationApplicationRequestContextInterceptor contextInterceptor;

  @Test
  void shouldPopulateInformationInXeroxNotificationApplicationRequestContext() {
    // given
    Clock clock = Clock.fixed(Instant.parse("2014-12-22T10:15:30.00Z"), ZoneId.of("UTC"));
    String dateTimeExpected = "2014-12-22T10:15:30";
    LocalDateTime dateTime = LocalDateTime.now(clock);
    given(request.getMethod()).willReturn(HttpMethod.POST.name());
    given(request.getRequestURI()).willReturn(XEROX_NOTIFICATION_API_CREATION_LETTER_URI);
    given(request.getHeader(HEADER_PARAM_USER_ID)).willReturn("USER1");
    UUID uuid = UUID.randomUUID();
    given(request.getHeader(HEADER_PARAM_CORRELATION_ID)).willReturn(uuid.toString());

    // when
    var result = contextInterceptor.preHandle(request, response, null);

    // then
    assertThat(result).isTrue();
    verify(xeroxNotificationApplicationRequestContext).setUser("USER1");
    verify(xeroxNotificationApplicationRequestContext).setStatus(any(StatusType.class));
    verify(xeroxNotificationApplicationRequestContext).setTransactionDate(any(LocalDate.class));
    verify(xeroxNotificationApplicationRequestContext).setCorrelationId(uuid);
    verify(request).getHeader(HEADER_PARAM_USER_ID);
    verify(request).getHeader(HEADER_PARAM_CORRELATION_ID);
    verifyNoMoreInteractions(xeroxNotificationApplicationRequestContext, request);
    verifyNoInteractions(response);
  }

  @Test
  void
      shouldPopulateInformationInXeroxNotificationApplicationRequestContextWhenCorrelationIdIsNull() {
    // given
    given(request.getMethod()).willReturn(HttpMethod.POST.name());
    given(request.getRequestURI()).willReturn(XEROX_NOTIFICATION_API_CREATION_LETTER_URI);
    given(request.getHeader(HEADER_PARAM_USER_ID)).willReturn("USER1");
    given(request.getHeader(HEADER_PARAM_CORRELATION_ID)).willReturn(null);

    // when
    var result = contextInterceptor.preHandle(request, response, null);

    // then
    assertThat(result).isTrue();
    verify(xeroxNotificationApplicationRequestContext).setUser("USER1");
    verify(xeroxNotificationApplicationRequestContext).setStatus(any(StatusType.class));
    verify(xeroxNotificationApplicationRequestContext).setTransactionDate(any(LocalDate.class));
    verify(xeroxNotificationApplicationRequestContext).setCorrelationId(null);
    verify(request).getHeader(HEADER_PARAM_USER_ID);
    verify(request).getHeader(HEADER_PARAM_CORRELATION_ID);
    verifyNoMoreInteractions(xeroxNotificationApplicationRequestContext, request);
    verifyNoInteractions(response);
  }

  @Test
  void
      shouldPopulateUseridInXeroxNotificationApplicationRequestContextWhenPatchMethodCalledAndValidUserId() {
    // given
    given(request.getMethod()).willReturn(HttpMethod.PATCH.name());
    given(request.getRequestURI())
        .willReturn(XEROX_NOTIFICATION_API_CREATION_LETTER_URI + "/xxxxx-xxxx-xxxx-xxxxxxx");
    given(request.getHeader(HEADER_PARAM_USER_ID)).willReturn("USER1");

    // when
    var result = contextInterceptor.preHandle(request, response, null);

    // then
    assertThat(result).isTrue();
    verify(xeroxNotificationApplicationRequestContext).setUser("USER1");
    verify(request).getHeader(HEADER_PARAM_USER_ID);
    verifyNoMoreInteractions(xeroxNotificationApplicationRequestContext, request);
    verifyNoInteractions(response);
  }

  @ParameterizedTest
  @ValueSource(strings = {"GET", "HEAD", "PUT", "PATCH", "DELETE", "OPTIONS", "TRACE"})
  void shouldNotValidateIfHttpMethodOtherThanPost(String httpMethod) {
    // given
    given(request.getMethod()).willReturn(httpMethod);
    given(request.getRequestURI()).willReturn(XEROX_NOTIFICATION_API_CREATION_LETTER_URI);

    // when
    var result = contextInterceptor.preHandle(request, response, contextInterceptor);

    // then
    assertThat(result).isTrue();
    verify(request).getMethod();
    verifyNoMoreInteractions(request);
    verifyNoInteractions(response, xeroxNotificationApplicationRequestContext);
  }

  @ParameterizedTest
  @ValueSource(strings = {"POST", "GET", "HEAD", "PUT", "DELETE", "OPTIONS", "TRACE"})
  void shouldNotValidateIfHttpMethodOtherThanPatch(String httpMethod) {
    // given
    given(request.getMethod()).willReturn(httpMethod);
    given(request.getRequestURI())
        .willReturn(XEROX_NOTIFICATION_API_CREATION_LETTER_URI + "/xxxxx-xxxx-xxxx-xxxxxxx");

    // when
    var result = contextInterceptor.preHandle(request, response, contextInterceptor);

    // then
    assertThat(result).isFalse();
    verify(request).getMethod();
    verifyNoMoreInteractions(request);
    verifyNoInteractions(response, xeroxNotificationApplicationRequestContext);
  }
}
