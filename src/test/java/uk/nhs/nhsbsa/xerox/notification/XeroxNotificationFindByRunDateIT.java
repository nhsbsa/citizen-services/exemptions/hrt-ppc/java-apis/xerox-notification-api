package uk.nhs.nhsbsa.xerox.notification;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.OK;
import static uk.nhs.nhsbsa.exemptions.TestConstants.CREATED_BY;
import static uk.nhs.nhsbsa.exemptions.assertions.TestAssertions.assertErrorInResponse;
import static uk.nhs.nhsbsa.exemptions.testsupport.PageTestDataFactory.validPageWithGivenDetails;
import static uk.nhs.nhsbsa.exemptions.utilities.ComparisonUtil.recursiveComparisonConfiguration;
import static uk.nhs.nhsbsa.exemptions.utilities.UriBuilderUtil.buildQueryUri;
import static uk.nhs.nhsbsa.xerox.notification.model.Constants.QUERY_PARAM_RUN_DATE;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.BAD_REQUEST_ERROR_MESSAGE;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.LETTER_TEMPLATE_API_SEARCH_BY_EFFECTIVE_ON_DATE_URI;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterTemplateTestDataFactory.aGeneratedValidLetterTemplateWithEffectiveTo;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterTemplateTestDataFactory.aValidLetterTemplate;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterTemplateTestDataFactory.aValidLetterTemplateWithNullEffectiveTo;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LinksTestDataFactory.aValidLinkHolderWithASearchLink;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LinksTestDataFactory.aValidLinksBuilderWithASelfLinkAndTemplate;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.MetaUtility.validMetaWithAllFields;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.XeroxHeadersTestDataFactory.validXeroxRequestHeaders;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.hibernate.exception.SQLGrammarException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockReset;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import uk.nhs.nhsbsa.exemptions.models.ErrorResponse;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterTemplate;
import uk.nhs.nhsbsa.xerox.notification.model.GetLetterTemplatePagedResponse;
import uk.nhs.nhsbsa.xerox.notification.model.Links;
import uk.nhs.nhsbsa.xerox.notification.model.Meta;
import uk.nhs.nhsbsa.xerox.notification.model.NewLetterTemplateResponseDto;
import uk.nhs.nhsbsa.xerox.notification.model.XeroxNotificationApplicationRequestContext;
import uk.nhs.nhsbsa.xerox.notification.repository.LetterDetailsRepository;
import uk.nhs.nhsbsa.xerox.notification.repository.LetterTemplateRepository;
import uk.nhs.nhsbsa.xerox.notification.security.SpringSecurityAuditorAware;
import uk.nhs.nhsbsa.xerox.notification.testsupport.LetterTemplateTestDataFactory;
import uk.nhs.nhsbsa.xerox.notification.testsupport.converters.LetterTemplateToNewLetterTemplateResponseDtoConverter;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureWebTestClient
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class XeroxNotificationFindByRunDateIT extends AbstractBaseIT {

  private static final String SIZE = "size";

  @Mock
  private XeroxNotificationApplicationRequestContext xeroxNotificationApplicationRequestContext;

  @Autowired private SpringSecurityAuditorAware springSecurityAuditorAware;

  @SpyBean(reset = MockReset.AFTER)
  LetterDetailsRepository letterDetailsRepository;

  @SpyBean(reset = MockReset.AFTER)
  private LetterTemplateRepository letterTemplateRepository;

  @BeforeEach
  public void setup() {
    MockHttpServletRequest request = new MockHttpServletRequest();
    RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(CREATED_BY);
    ReflectionTestUtils.setField(
        springSecurityAuditorAware,
        "xeroxNotificationApplicationRequestContext",
        xeroxNotificationApplicationRequestContext);

    requestHeaders = validXeroxRequestHeaders("User1");
    letterDetailsRepository.deleteAll();
    letterDetailsRepository.flush();
    letterTemplateRepository.deleteAll();
    letterTemplateRepository.flush();
  }

  @AfterEach
  public void tearDown() {
    RequestContextHolder.resetRequestAttributes();
  }

  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource(strings = {"aaaaaaaaaahhhhhhhhhhhhslllllllllllllllllllllll4785960684373737"})
  void shouldReturnBadRequestWhenUserIdHeaderMissingOrInvalid(String user) throws Exception {
    // given
    requestHeaders = validXeroxRequestHeaders(user);
    letterTemplateRepository.save(aValidLetterTemplate());
    Map<String, Object> queryParams = Map.of(QUERY_PARAM_RUN_DATE, "2023-03-31");

    // when
    ResponseEntity<ErrorResponse> response =
        getLetterTemplateFindByEffectiveOnDateRequest(
            queryParams, requestHeaders, ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, BAD_REQUEST_ERROR_MESSAGE);
  }

  @Test
  void shouldReturnBadRequestWhenRunDateMissing() throws Exception {
    // given
    letterTemplateRepository.save(aValidLetterTemplate());
    Map<String, Object> queryParams = new HashMap<>();

    // when
    ResponseEntity<ErrorResponse> response =
        getLetterTemplateFindByEffectiveOnDateRequest(
            queryParams, requestHeaders, ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, BAD_REQUEST_ERROR_MESSAGE);
  }

  @ParameterizedTest
  @ValueSource(
      strings = {
        "",
        "abcd",
        "01-01-2023",
        "2023-04-44",
        "2023-13-01",
        "20230101",
        "2023-04-01,2023-04-02"
      })
  void shouldReturnBadRequestWhenRunDateInvalid(String runDate) throws Exception {
    // given
    letterTemplateRepository.save(aValidLetterTemplate());
    Map<String, Object> queryParams = Map.of(QUERY_PARAM_RUN_DATE, runDate);

    // when
    ResponseEntity<ErrorResponse> response =
        getLetterTemplateFindByEffectiveOnDateRequest(
            queryParams, requestHeaders, ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, BAD_REQUEST_ERROR_MESSAGE);
  }

  @Test
  void shouldFindTemplateFindByEffectiveOnDateAndReturnSuccessResponse() throws Exception {
    // given
    LetterTemplate letterTemplate = letterTemplateRepository.save(aValidLetterTemplate());
    Meta meta =
        validMetaWithAllFields(
            letterTemplate.getCreatedTimestamp(),
            letterTemplate.getCreatedBy(),
            letterTemplate.getUpdatedTimestamp(),
            letterTemplate.getUpdatedBy());
    Map<String, Object> queryParams = Map.of(QUERY_PARAM_RUN_DATE, "2023-03-31");

    // when
    ResponseEntity<GetLetterTemplatePagedResponse> response =
        getLetterTemplateFindByEffectiveOnDateRequest(
            queryParams, requestHeaders, GetLetterTemplatePagedResponse.class);

    // then
    List<NewLetterTemplateResponseDto> templates = response.getBody().getEmbedded().getTemplates();
    Links links =
        aValidLinksBuilderWithASelfLinkAndTemplate(port, letterTemplate.getId().toString());
    NewLetterTemplateResponseDto expectedNewLetterTemplateResponseDto =
        LetterTemplateToNewLetterTemplateResponseDtoConverter.convert(letterTemplate, links);

    URI searchUri = buildQueryUri(queryParams, LETTER_TEMPLATE_API_SEARCH_BY_EFFECTIVE_ON_DATE_URI);
    var searchLink = aValidLinkHolderWithASearchLink(port, searchUri, "0", "20");
    var expectedPageResource = validPageWithGivenDetails("20", "1", "1", "0");

    assertThat(response.getStatusCode()).isEqualTo(OK);
    assertThat(templates.size()).isEqualTo(1);
    // The id, meta changes on every response
    NewLetterTemplateResponseDto resultLetterTemplate = templates.get(0);
    assertThat(resultLetterTemplate)
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .ignoringFields("id", "meta")
        .isEqualTo(expectedNewLetterTemplateResponseDto);
    assertThat(resultLetterTemplate.getMeta())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .ignoringFields("createdTimestamp", "updatedTimestamp")
        .isEqualTo(meta);
    assertThat(resultLetterTemplate.getMeta().getCreatedTimestamp())
        .isEqualToIgnoringMinutes(meta.getCreatedTimestamp());
    assertThat(resultLetterTemplate.getMeta().getUpdatedTimestamp())
        .isEqualToIgnoringMinutes(meta.getUpdatedTimestamp());
    assertThat(response.getBody().getLinks().getSelfLink())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(searchLink);
    assertThat(response.getBody().getPage())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(expectedPageResource);
  }

  @Test
  void shouldFindTemplateFindByEffectiveOnDateWithNullEffectToAndReturnSuccessResponse()
      throws Exception {
    // given
    LetterTemplate letterTemplate =
        letterTemplateRepository.save(
            LetterTemplateTestDataFactory.aValidLetterTemplateWithNullEffectiveTo());
    Meta meta =
        validMetaWithAllFields(
            letterTemplate.getCreatedTimestamp(),
            letterTemplate.getCreatedBy(),
            letterTemplate.getUpdatedTimestamp(),
            letterTemplate.getUpdatedBy());
    Map<String, Object> queryParams = Map.of(QUERY_PARAM_RUN_DATE, "2023-03-31");

    // when
    ResponseEntity<GetLetterTemplatePagedResponse> response =
        getLetterTemplateFindByEffectiveOnDateRequest(
            queryParams, requestHeaders, GetLetterTemplatePagedResponse.class);

    // then
    List<NewLetterTemplateResponseDto> templates = response.getBody().getEmbedded().getTemplates();
    Links links =
        aValidLinksBuilderWithASelfLinkAndTemplate(port, letterTemplate.getId().toString());
    NewLetterTemplateResponseDto expectedNewLetterTemplateResponseDto =
        LetterTemplateToNewLetterTemplateResponseDtoConverter.convert(letterTemplate, links);

    URI searchUri = buildQueryUri(queryParams, LETTER_TEMPLATE_API_SEARCH_BY_EFFECTIVE_ON_DATE_URI);
    var searchLink = aValidLinkHolderWithASearchLink(port, searchUri, "0", "20");
    var expectedPageResource = validPageWithGivenDetails("20", "1", "1", "0");

    assertThat(response.getStatusCode()).isEqualTo(OK);
    assertThat(templates.size()).isEqualTo(1);
    // The id, meta changes on every response
    NewLetterTemplateResponseDto resultLetterTemplate = templates.get(0);
    assertThat(resultLetterTemplate)
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .ignoringFields("id", "meta")
        .isEqualTo(expectedNewLetterTemplateResponseDto);
    assertThat(resultLetterTemplate.getMeta())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .ignoringFields("createdTimestamp", "updatedTimestamp")
        .isEqualTo(meta);
    assertThat(resultLetterTemplate.getMeta().getCreatedTimestamp())
        .isEqualToIgnoringMinutes(meta.getCreatedTimestamp());
    assertThat(resultLetterTemplate.getMeta().getUpdatedTimestamp())
        .isEqualToIgnoringMinutes(meta.getUpdatedTimestamp());
    assertThat(response.getBody().getLinks().getSelfLink())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(searchLink);
    assertThat(response.getBody().getPage())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(expectedPageResource);
  }

  @Test
  void shouldFindTemplateFindByEffectiveOnDateEqualEffectFromAndReturnSuccessResponse()
      throws Exception {
    // given
    String effectiveFrom = "2021-01-01";
    LetterTemplate letterTemplate =
        letterTemplateRepository.save(aValidLetterTemplateWithNullEffectiveTo(effectiveFrom));
    Meta meta =
        validMetaWithAllFields(
            letterTemplate.getCreatedTimestamp(),
            letterTemplate.getCreatedBy(),
            letterTemplate.getUpdatedTimestamp(),
            letterTemplate.getUpdatedBy());
    Map<String, Object> queryParams = Map.of(QUERY_PARAM_RUN_DATE, effectiveFrom);

    // when
    ResponseEntity<GetLetterTemplatePagedResponse> response =
        getLetterTemplateFindByEffectiveOnDateRequest(
            queryParams, requestHeaders, GetLetterTemplatePagedResponse.class);

    // then
    List<NewLetterTemplateResponseDto> templates = response.getBody().getEmbedded().getTemplates();
    Links links =
        aValidLinksBuilderWithASelfLinkAndTemplate(port, letterTemplate.getId().toString());
    NewLetterTemplateResponseDto expectedNewLetterTemplateResponseDto =
        LetterTemplateToNewLetterTemplateResponseDtoConverter.convert(letterTemplate, links);

    URI searchUri = buildQueryUri(queryParams, LETTER_TEMPLATE_API_SEARCH_BY_EFFECTIVE_ON_DATE_URI);
    var searchLink = aValidLinkHolderWithASearchLink(port, searchUri, "0", "20");
    var expectedPageResource = validPageWithGivenDetails("20", "1", "1", "0");

    assertThat(response.getStatusCode()).isEqualTo(OK);
    assertThat(templates.size()).isEqualTo(1);
    // The id, meta changes on every response
    NewLetterTemplateResponseDto resultLetterTemplate = templates.get(0);
    assertThat(resultLetterTemplate)
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .ignoringFields("id", "meta")
        .isEqualTo(expectedNewLetterTemplateResponseDto);
    assertThat(resultLetterTemplate.getMeta())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .ignoringFields("createdTimestamp", "updatedTimestamp")
        .isEqualTo(meta);
    assertThat(resultLetterTemplate.getEffectiveFrom().toString()).isEqualTo(effectiveFrom);
    assertThat(resultLetterTemplate.getMeta().getCreatedTimestamp())
        .isEqualToIgnoringMinutes(meta.getCreatedTimestamp());
    assertThat(resultLetterTemplate.getMeta().getUpdatedTimestamp())
        .isEqualToIgnoringMinutes(meta.getUpdatedTimestamp());
    assertThat(response.getBody().getLinks().getSelfLink())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(searchLink);
    assertThat(response.getBody().getPage())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(expectedPageResource);
  }

  @Test
  void shouldFindTemplateFindByEffectiveOnDateIsNotMatching() throws Exception {
    // given
    String effectiveFrom = "2021-01-01";
    String searchDate = "2020-01-01";
    LetterTemplate letterTemplate =
        letterTemplateRepository.save(aValidLetterTemplateWithNullEffectiveTo(effectiveFrom));
    Map<String, Object> queryParams = Map.of(QUERY_PARAM_RUN_DATE, searchDate);

    // when
    ResponseEntity<GetLetterTemplatePagedResponse> response =
        getLetterTemplateFindByEffectiveOnDateRequest(
            queryParams, requestHeaders, GetLetterTemplatePagedResponse.class);

    // then
    URI searchUri = buildQueryUri(queryParams, LETTER_TEMPLATE_API_SEARCH_BY_EFFECTIVE_ON_DATE_URI);
    var searchLink = aValidLinkHolderWithASearchLink(port, searchUri, "0", "20");
    var expectedPageResource = validPageWithGivenDetails("20", "0", "0", "0");

    assertThat(response.getStatusCode()).isEqualTo(OK);
    assertThat(response.getBody().getEmbedded().getTemplates()).isEmpty();
    assertThat(response.getBody().getLinks().getSelfLink())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(searchLink);
    assertThat(response.getBody().getPage())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(expectedPageResource);
  }

  @ParameterizedTest
  @ValueSource(ints = {2, 10, 15})
  void shouldFindTemplatesAndPaginateWhenFindByEffectiveOnDateWithLessEqualSize(
      int validNumberOfRecord) throws Exception {
    // given
    LocalDate effectiveTo = LocalDate.parse("2024-03-31");
    LocalDate aDateTime = effectiveTo.minusDays(100);

    String size = "15";
    int backwardMinDays = 600;
    List<LetterTemplate> letterTemplates =
        buildTestLetterTemplates(effectiveTo, validNumberOfRecord, backwardMinDays);

    Map<String, Object> queryParams = new HashMap<>();
    queryParams.put(QUERY_PARAM_RUN_DATE, aDateTime.toString());
    queryParams.put(SIZE, size);

    // when
    ResponseEntity<GetLetterTemplatePagedResponse> response =
        getLetterTemplateFindByEffectiveOnDateRequest(
            queryParams, requestHeaders, GetLetterTemplatePagedResponse.class);

    // then
    queryParams.remove(SIZE);
    assertSearchResultWithoutPaginationLinks(
        validNumberOfRecord, size, letterTemplates, queryParams, response);
  }

  @ParameterizedTest
  @ValueSource(strings = {"2", "10"})
  void shouldFindTemplatesAndPaginateWhenFindByEffectiveOnDateWithRecordsMoreThanSize(String size)
      throws Exception {
    // given
    LocalDate effectiveTo = LocalDate.parse("2024-03-31");
    LocalDate aDateTime = effectiveTo.minusDays(100);

    int validNumberOfRecord = 19;
    int backwardMinDays = 600;
    List<LetterTemplate> letterTemplates =
        buildTestLetterTemplates(effectiveTo, validNumberOfRecord, backwardMinDays);

    Map<String, Object> queryParams = new HashMap<>();
    queryParams.put(QUERY_PARAM_RUN_DATE, aDateTime.toString());
    queryParams.put(SIZE, size);

    // when
    ResponseEntity<GetLetterTemplatePagedResponse> response =
        getLetterTemplateFindByEffectiveOnDateRequest(
            queryParams, requestHeaders, GetLetterTemplatePagedResponse.class);

    // then
    queryParams.remove(SIZE);
    assertMultipleSearchResults(validNumberOfRecord, letterTemplates, queryParams, response, size);
  }

  @ParameterizedTest
  @ValueSource(ints = {2, 20})
  void shouldFindTemplatesAndPaginateWhenFindByEffectiveOnDateWithoutSize(int validNumberOfRecord)
      throws Exception {
    // given
    LocalDate effectiveTo = LocalDate.parse("2024-03-31");
    LocalDate aDateTime = effectiveTo.minusDays(100);

    int backwardMinDays = 600;
    List<LetterTemplate> letterTemplates =
        buildTestLetterTemplates(effectiveTo, validNumberOfRecord, backwardMinDays);

    Map<String, Object> queryParams = new HashMap<>();
    queryParams.put(QUERY_PARAM_RUN_DATE, aDateTime.toString());

    // when
    ResponseEntity<GetLetterTemplatePagedResponse> response =
        getLetterTemplateFindByEffectiveOnDateRequest(
            queryParams, requestHeaders, GetLetterTemplatePagedResponse.class);

    // then
    assertSearchResultWithoutPaginationLinks(
        validNumberOfRecord, "20", letterTemplates, queryParams, response);
  }

  @ParameterizedTest
  @ValueSource(strings = {"0", "-5"})
  void shouldFindTemplatesAndPaginateWhenFindByEffectiveOnDateInvalidSizeWithLessThan20Records(
      String size) throws Exception {
    // given
    LocalDate effectiveTo = LocalDate.parse("2024-03-31");
    LocalDate aDateTime = effectiveTo.minusDays(100);
    int validNumberOfRecord = 15;

    int backwardMinDays = 600;
    List<LetterTemplate> letterTemplates =
        buildTestLetterTemplates(effectiveTo, validNumberOfRecord, backwardMinDays);

    Map<String, Object> queryParams = new HashMap<>();
    queryParams.put(QUERY_PARAM_RUN_DATE, aDateTime.toString());
    queryParams.put(SIZE, size);

    // when
    ResponseEntity<GetLetterTemplatePagedResponse> response =
        getLetterTemplateFindByEffectiveOnDateRequest(
            queryParams, requestHeaders, GetLetterTemplatePagedResponse.class);

    // then
    queryParams.remove(SIZE);
    assertSearchResultWithoutPaginationLinks(
        validNumberOfRecord, "20", letterTemplates, queryParams, response);
  }

  @ParameterizedTest
  @ValueSource(ints = {22, 70})
  void shouldFindTemplatesAndPaginateWhenFindByEffectiveOnDateWithRecordsMoreThan20WithoutSize(
      int validNumberOfRecord) throws Exception {
    // given
    LocalDate effectiveTo = LocalDate.parse("2024-03-31");
    LocalDate aDateTime = effectiveTo.minusDays(100);

    int backwardMinDays = 600;
    List<LetterTemplate> letterTemplates =
        buildTestLetterTemplates(effectiveTo, validNumberOfRecord, backwardMinDays);

    Map<String, Object> queryParams = new HashMap<>();
    queryParams.put(QUERY_PARAM_RUN_DATE, aDateTime.toString());

    // when
    ResponseEntity<GetLetterTemplatePagedResponse> response =
        getLetterTemplateFindByEffectiveOnDateRequest(
            queryParams, requestHeaders, GetLetterTemplatePagedResponse.class);

    // then
    assertMultipleSearchResults(validNumberOfRecord, letterTemplates, queryParams, response, "20");
  }

  @ParameterizedTest
  @ValueSource(strings = {"0", "-5"})
  void shouldFindTemplatesAndPaginateWhenFindByEffectiveOnDateWithRecordsMoreThan20WithInvalidSize(
      String size) throws Exception {
    // given
    LocalDate effectiveTo = LocalDate.parse("2024-03-31");
    LocalDate aDateTime = effectiveTo.minusDays(100);

    int validNumberOfRecord = 23;
    int backwardMinDays = 600;
    List<LetterTemplate> letterTemplates =
        buildTestLetterTemplates(effectiveTo, validNumberOfRecord, backwardMinDays);

    Map<String, Object> queryParams = new HashMap<>();
    queryParams.put(QUERY_PARAM_RUN_DATE, aDateTime.toString());
    queryParams.put(SIZE, size);

    // when
    ResponseEntity<GetLetterTemplatePagedResponse> response =
        getLetterTemplateFindByEffectiveOnDateRequest(
            queryParams, requestHeaders, GetLetterTemplatePagedResponse.class);

    // then
    queryParams.remove(SIZE);
    assertMultipleSearchResults(validNumberOfRecord, letterTemplates, queryParams, response, "20");
  }

  @Test
  void shouldReturnInternalServerErrorWhenExceptionOccurs() throws Exception {
    // given
    letterTemplateRepository.save(aValidLetterTemplate());
    SQLGrammarException sqlGrammarException = new SQLGrammarException("sql exception", null);
    InvalidDataAccessResourceUsageException exception =
        new InvalidDataAccessResourceUsageException("invalid data", sqlGrammarException);
    doThrow(exception).when(letterTemplateRepository).findByEffectiveOnDate(any(), any());
    Map<String, Object> queryParams = Map.of(QUERY_PARAM_RUN_DATE, "2023-03-31");

    // when
    ResponseEntity<ErrorResponse> response =
        getLetterTemplateFindByEffectiveOnDateRequest(
            queryParams, requestHeaders, ErrorResponse.class);
    // then
    assertThat(response.getStatusCode()).isEqualTo(INTERNAL_SERVER_ERROR);
    assertErrorInResponse(response, INTERNAL_SERVER_ERROR, "invalid data");
  }

  private List<LetterTemplate> buildTestLetterTemplates(
      LocalDate effectiveTo, int validNumberOfRecord, int backwardMinDays) {
    return IntStream.range(0, validNumberOfRecord)
        .mapToObj(
            i -> aGeneratedValidLetterTemplateWithEffectiveTo(effectiveTo, backwardMinDays + i))
        .map(letterTemplateRepository::save)
        .collect(Collectors.toList());
  }

  private void assertSearchResultWithoutPaginationLinks(
      int validNumberOfRecord,
      String size,
      List<LetterTemplate> letterTemplates,
      Map<String, Object> queryParams,
      ResponseEntity<GetLetterTemplatePagedResponse> response)
      throws URISyntaxException {
    List<NewLetterTemplateResponseDto> templates = response.getBody().getEmbedded().getTemplates();
    assertThat(response.getStatusCode()).isEqualTo(OK);

    assertTemplates(letterTemplates, templates, validNumberOfRecord);

    URI searchUri = buildQueryUri(queryParams, LETTER_TEMPLATE_API_SEARCH_BY_EFFECTIVE_ON_DATE_URI);
    var searchLink = aValidLinkHolderWithASearchLink(port, searchUri, "0", size);
    var expectedPageResource =
        validPageWithGivenDetails(size, "1", String.valueOf(validNumberOfRecord), "0");

    assertThat(templates.size()).isEqualTo(validNumberOfRecord);
    // The id, meta changes on every response

    assertThat(response.getBody().getLinks().getSelfLink())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(searchLink);
    assertThat(response.getBody().getLinks().getFirst()).isNull();
    assertThat(response.getBody().getLinks().getNext()).isNull();
    assertThat(response.getBody().getLinks().getLast())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isNull();
    assertThat(response.getBody().getPage())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(expectedPageResource);
  }

  private void assertMultipleSearchResults(
      int validNumberOfRecord,
      List<LetterTemplate> letterTemplates,
      Map<String, Object> queryParams,
      ResponseEntity<GetLetterTemplatePagedResponse> response,
      String pageSize)
      throws URISyntaxException {
    List<NewLetterTemplateResponseDto> templates = response.getBody().getEmbedded().getTemplates();
    assertThat(response.getStatusCode()).isEqualTo(OK);
    int pageSizeNum = Integer.valueOf(pageSize);
    assertTemplates(letterTemplates, templates, pageSizeNum);

    URI searchUri = buildQueryUri(queryParams, LETTER_TEMPLATE_API_SEARCH_BY_EFFECTIVE_ON_DATE_URI);

    var searchLink = aValidLinkHolderWithASearchLink(port, searchUri, "0", pageSize);
    var expectedPageResource =
        validPageWithGivenDetails(
            pageSize,
            String.valueOf(validNumberOfRecord / pageSizeNum + 1),
            String.valueOf(validNumberOfRecord),
            "0");
    var firstLink = aValidLinkHolderWithASearchLink(port, searchUri, "0", pageSize);
    var lastLink =
        aValidLinkHolderWithASearchLink(
            port, searchUri, String.valueOf(validNumberOfRecord / pageSizeNum), pageSize);
    var nextLink = aValidLinkHolderWithASearchLink(port, searchUri, "1", pageSize);

    assertThat(templates.size()).isEqualTo(pageSizeNum);
    // The id, meta changes on every response

    assertThat(response.getBody().getLinks().getSelfLink())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(searchLink);
    assertThat(response.getBody().getLinks().getFirst())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(firstLink);
    assertThat(response.getBody().getLinks().getNext())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(nextLink);
    assertThat(response.getBody().getLinks().getLast())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(lastLink);
    assertThat(response.getBody().getPage())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(expectedPageResource);
  }

  private void assertTemplates(
      List<LetterTemplate> letterTemplates,
      List<NewLetterTemplateResponseDto> templates,
      int pageSizeNum) {
    for (int i = 0; i < pageSizeNum; i++) {
      NewLetterTemplateResponseDto resultLetterTemplate = templates.get(i);
      LetterTemplate letterTemplate =
          letterTemplates.stream()
              .filter(e -> e.getId().equals(resultLetterTemplate.getId()))
              .findFirst()
              .get();

      Meta meta =
          validMetaWithAllFields(
              letterTemplate.getCreatedTimestamp(),
              letterTemplate.getCreatedBy(),
              letterTemplate.getUpdatedTimestamp(),
              letterTemplate.getUpdatedBy());
      Links links =
          aValidLinksBuilderWithASelfLinkAndTemplate(port, letterTemplate.getId().toString());
      NewLetterTemplateResponseDto expectedNewLetterTemplateResponseDto =
          LetterTemplateToNewLetterTemplateResponseDtoConverter.convert(letterTemplate, links);
      assertThat(resultLetterTemplate.getMeta().getCreatedTimestamp())
          .isEqualToIgnoringMinutes(meta.getCreatedTimestamp());
      assertThat(resultLetterTemplate.getMeta().getUpdatedTimestamp())
          .isEqualToIgnoringMinutes(meta.getUpdatedTimestamp());
      assertThat(resultLetterTemplate)
          .usingRecursiveComparison(recursiveComparisonConfiguration)
          .ignoringFields("id", "meta")
          .isEqualTo(expectedNewLetterTemplateResponseDto);
      assertThat(resultLetterTemplate.getMeta())
          .usingRecursiveComparison(recursiveComparisonConfiguration)
          .ignoringFields("createdTimestamp", "updatedTimestamp")
          .isEqualTo(meta);
      ;
    }
  }

  private ResponseEntity getLetterTemplateFindByEffectiveOnDateRequest(
      Map<String, Object> queryParams, HttpHeaders headers, Class expectedType)
      throws URISyntaxException {
    URI uri = buildQueryUri(queryParams, LETTER_TEMPLATE_API_SEARCH_BY_EFFECTIVE_ON_DATE_URI);
    RequestEntity<?> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, uri);
    return testRestTemplate.exchange(requestEntity, expectedType);
  }
}
