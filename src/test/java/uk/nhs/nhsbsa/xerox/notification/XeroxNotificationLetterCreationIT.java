package uk.nhs.nhsbsa.xerox.notification;

import static java.util.Comparator.comparing;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static uk.nhs.nhsbsa.exemptions.TestConstants.CREATED_BY;
import static uk.nhs.nhsbsa.exemptions.assertions.TestAssertions.assertErrorInResponse;
import static uk.nhs.nhsbsa.exemptions.assertions.TestAssertions.assertFieldError;
import static uk.nhs.nhsbsa.exemptions.testsupport.MetaTestDataFactory.validMetaWithAllFields;
import static uk.nhs.nhsbsa.exemptions.utilities.ComparisonUtil.recursiveComparisonConfiguration;
import static uk.nhs.nhsbsa.xerox.notification.model.Constants.HEADER_PARAM_CORRELATION_ID;
import static uk.nhs.nhsbsa.xerox.notification.model.Constants.HEADER_PARAM_SERVICE_NAME;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.BAD_REQUEST_ERROR_MESSAGE;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.LETTER_DETAILS_ID;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.SERVICE_NAME_HRT_PPC;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.VALIDATION_ERROR_NOT_EMPTY_MESSAGE;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.XEROX_NOTIFICATION_API_POST_URI;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterDetailsTestDataFactory.aValidLetterDetailsWithTemplateIdWithoutTransactionDate;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterTemplateTestDataFactory.aValidLetterTemplate;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LinksTestDataFactory.aValidLinksBuilderWithTemplateAndLetterDetails;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.XeroxHeadersTestDataFactory.validXeroxRequestHeaders;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.core5.net.URIBuilder;
import org.hibernate.exception.SQLGrammarException;
import org.javers.core.Changes;
import org.javers.repository.jql.QueryBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockReset;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import uk.nhs.nhsbsa.exemptions.TestConstants;
import uk.nhs.nhsbsa.exemptions.models.ErrorResponse;
import uk.nhs.nhsbsa.exemptions.models.Meta;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterDetails;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterTemplate;
import uk.nhs.nhsbsa.xerox.notification.model.Links;
import uk.nhs.nhsbsa.xerox.notification.model.NewLetterDetailsRequestDto;
import uk.nhs.nhsbsa.xerox.notification.model.NewLetterDetailsResponseDto;
import uk.nhs.nhsbsa.xerox.notification.model.XeroxNotificationApplicationRequestContext;
import uk.nhs.nhsbsa.xerox.notification.repository.LetterDetailsRepository;
import uk.nhs.nhsbsa.xerox.notification.repository.LetterTemplateRepository;
import uk.nhs.nhsbsa.xerox.notification.security.SpringSecurityAuditorAware;
import uk.nhs.nhsbsa.xerox.notification.testsupport.converters.LetterDetailsToNewLetterDetailsRequestDtoConverter;
import uk.nhs.nhsbsa.xerox.notification.testsupport.converters.LetterDetailsToNewLetterDetailsResponseDtoConverter;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureWebTestClient
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class XeroxNotificationLetterCreationIT extends AbstractBaseIT {

  @Mock
  private XeroxNotificationApplicationRequestContext xeroxNotificationApplicationRequestContext;

  @Autowired private SpringSecurityAuditorAware springSecurityAuditorAware;

  @SpyBean(reset = MockReset.AFTER)
  LetterDetailsRepository letterDetailsRepository;

  @SpyBean(reset = MockReset.AFTER)
  private LetterTemplateRepository letterTemplateRepository;

  @BeforeEach
  public void setup() {
    MockHttpServletRequest request = new MockHttpServletRequest();
    RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(CREATED_BY);
    ReflectionTestUtils.setField(
        springSecurityAuditorAware,
        "xeroxNotificationApplicationRequestContext",
        xeroxNotificationApplicationRequestContext);

    requestHeaders = validXeroxRequestHeaders("User1");
    requestHeaders.set(HEADER_PARAM_SERVICE_NAME, SERVICE_NAME_HRT_PPC);
    letterDetailsRepository.deleteAll();
    letterDetailsRepository.flush();
    letterTemplateRepository.deleteAll();
    letterTemplateRepository.flush();
  }

  @AfterEach
  public void tearDown() {
    RequestContextHolder.resetRequestAttributes();
  }

  @Test
  void shouldSaveValidLetterDetailsAndReturnSuccessResponse() throws Exception {
    // given
    LetterDetails letterDetails =
        aValidLetterDetailsWithTemplateIdWithoutTransactionDate(createLetterTemplate());
    UUID correlationId = UUID.randomUUID();
    requestHeaders.set(HEADER_PARAM_CORRELATION_ID, correlationId.toString());
    Meta meta =
        validMetaWithAllFields(
            letterDetails.getCreatedTimestamp(),
            letterDetails.getCreatedBy(),
            letterDetails.getUpdatedTimestamp(),
            letterDetails.getUpdatedBy());

    // when
    ResponseEntity<NewLetterDetailsResponseDto> response =
        postLetterDetailsSaveRequest(
            requestHeaders, letterDetails, NewLetterDetailsResponseDto.class);
    // then
    NewLetterDetailsResponseDto letterDetailsResult = response.getBody();
    Links links =
        aValidLinksBuilderWithTemplateAndLetterDetails(
            port,
            letterDetailsResult.getId().toString(),
            letterDetailsResult.getTemplateId().toString());
    NewLetterDetailsResponseDto newUserPreferenceResponseDto =
        LetterDetailsToNewLetterDetailsResponseDtoConverter.convert(letterDetails, links);

    assertThat(response.getStatusCode()).isEqualTo(CREATED);
    // The id and meta changes on every response
    assertThat(letterDetailsResult)
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .ignoringFields("id", "meta", "correlationId", "statusUpdateAt", "transactionDate")
        .isEqualTo(newUserPreferenceResponseDto);
    assertThat(letterDetailsResult.getTransactionDate())
        .isEqualTo(meta.getCreatedTimestamp().toLocalDate());
    assertThat(letterDetailsResult.getStatusUpdateAt())
        .isEqualToIgnoringMinutes(meta.getCreatedTimestamp());
    assertThat(letterDetailsResult.getCorrelationId()).isEqualTo(correlationId.toString());
    assertThat(letterDetailsResult.getMeta())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .ignoringFields("createdTimestamp", "updatedTimestamp")
        .isEqualTo(meta);
    assertThat(letterDetailsResult.getMeta().getCreatedTimestamp())
        .isEqualToIgnoringMinutes(meta.getCreatedTimestamp());
    assertThat(letterDetailsResult.getMeta().getUpdatedTimestamp())
        .isEqualToIgnoringMinutes(meta.getUpdatedTimestamp());
    assertJaversRecord(letterDetailsResult);
  }

  @Test
  void shouldSaveValidLetterDetailsWithoutCorrelationIdAndReturnSuccessResponse() throws Exception {
    // given
    LetterDetails letterDetails =
        aValidLetterDetailsWithTemplateIdWithoutTransactionDate(createLetterTemplate());
    requestHeaders.remove(HEADER_PARAM_CORRELATION_ID);
    Meta meta =
        validMetaWithAllFields(
            letterDetails.getCreatedTimestamp(),
            letterDetails.getCreatedBy(),
            letterDetails.getUpdatedTimestamp(),
            letterDetails.getUpdatedBy());

    // when
    ResponseEntity<NewLetterDetailsResponseDto> response =
        postLetterDetailsSaveRequest(
            requestHeaders, letterDetails, NewLetterDetailsResponseDto.class);

    // then
    NewLetterDetailsResponseDto letterDetailsResult = response.getBody();
    Links links =
        aValidLinksBuilderWithTemplateAndLetterDetails(
            port,
            letterDetailsResult.getId().toString(),
            letterDetailsResult.getTemplateId().toString());
    NewLetterDetailsResponseDto newUserPreferenceResponseDto =
        LetterDetailsToNewLetterDetailsResponseDtoConverter.convert(letterDetails, links);

    assertThat(response.getStatusCode()).isEqualTo(CREATED);
    // The id and meta changes on every response
    assertThat(letterDetailsResult)
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .ignoringFields("id", "meta", "correlationId", "statusUpdateAt", "transactionDate")
        .isEqualTo(newUserPreferenceResponseDto);
    assertThat(letterDetailsResult.getTransactionDate())
        .isEqualTo(meta.getCreatedTimestamp().toLocalDate());
    assertThat(letterDetailsResult.getStatusUpdateAt())
        .isEqualToIgnoringMinutes(meta.getCreatedTimestamp());
    assertThat(letterDetailsResult.getCorrelationId()).isNull();
    assertThat(letterDetailsResult.getMeta())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .ignoringFields("createdTimestamp", "updatedTimestamp")
        .isEqualTo(meta);
    assertThat(letterDetailsResult.getMeta().getCreatedTimestamp())
        .isEqualToIgnoringMinutes(meta.getCreatedTimestamp());
    assertThat(letterDetailsResult.getMeta().getUpdatedTimestamp())
        .isEqualToIgnoringMinutes(meta.getUpdatedTimestamp());
    assertJaversRecord(letterDetailsResult);
  }

  @ParameterizedTest
  @ValueSource(strings = {"abcd123", "xxxxxx-xxxx-xxxx-xxxxxxxxxxxxxxxxx"})
  void shouldReturnBadRequestWhenInvalidCorrelationId(String correlationId) throws Exception {
    // given
    requestHeaders.set(HEADER_PARAM_CORRELATION_ID, correlationId);
    LetterDetails letterDetails =
        aValidLetterDetailsWithTemplateIdWithoutTransactionDate(createLetterTemplate());

    // when
    ResponseEntity<ErrorResponse> response =
        postLetterDetailsSaveRequest(requestHeaders, letterDetails, ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, BAD_REQUEST_ERROR_MESSAGE);
  }

  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource(strings = {"aaaaaaaaaahhhhhhhhhhhhslllllllllllllllllllllll4785960684373737"})
  void shouldReturnBadRequestWhenUserIdHeaderMissingOrInvalid(String user) throws Exception {
    // given
    requestHeaders = validXeroxRequestHeaders(user);
    LetterDetails letterDetails =
        aValidLetterDetailsWithTemplateIdWithoutTransactionDate(createLetterTemplate());

    // when
    ResponseEntity<ErrorResponse> response =
        postLetterDetailsSaveRequest(requestHeaders, letterDetails, ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, BAD_REQUEST_ERROR_MESSAGE);
  }

  @Test
  void shouldReturnBadRequestWhenServiceNameHeaderMissing() throws Exception {
    // given
    requestHeaders.remove(HEADER_PARAM_SERVICE_NAME);
    LetterDetails letterDetails =
        aValidLetterDetailsWithTemplateIdWithoutTransactionDate(createLetterTemplate());

    // when
    ResponseEntity<ErrorResponse> response =
        postLetterDetailsSaveRequest(requestHeaders, letterDetails, ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, BAD_REQUEST_ERROR_MESSAGE);
  }

  @Test
  void shouldReturnBadRequestWhenServiceNameHeaderInvalid() throws Exception {
    // given
    requestHeaders.set(HEADER_PARAM_SERVICE_NAME, "HRT_PPC2");
    LetterDetails letterDetails =
        aValidLetterDetailsWithTemplateIdWithoutTransactionDate(createLetterTemplate());

    // when
    ResponseEntity<ErrorResponse> response =
        postLetterDetailsSaveRequest(requestHeaders, letterDetails, ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, BAD_REQUEST_ERROR_MESSAGE);
  }

  @Test
  void shouldReturnBadRequestWhenNoBody() throws Exception {
    // given
    // when
    ResponseEntity<ErrorResponse> response =
        postLetterDetailsSaveRequest(requestHeaders, null, ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, BAD_REQUEST_ERROR_MESSAGE);
  }

  @ParameterizedTest
  @CsvSource(
      value = {
        "null,randomUUID,aPersonalisation,reference",
        ",randomUUID,aPersonalisation,reference",
        "HRTABCD123,null,aPersonalisation,templateId",
        "HRTABCD123,,aPersonalisation,templateId",
        "HRTABCD123,randomUUID,null,personalisation",
        "HRTABCD123,randomUUID,,personalisation"
      },
      nullValues = {"null"})
  void shouldReturnBadRequestWhenMissingBodyFields(
      String reference, String templateId, String personalisation, String verifyField)
      throws Exception {
    // when
    ResponseEntity<ErrorResponse> response =
        postLetterDetailsWithMissFields(requestHeaders, reference, templateId, personalisation);
    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, "There were validation issues with the request.");
    List<ErrorResponse.FieldError> fieldErrors = response.getBody().getFieldErrors();
    Collections.sort(fieldErrors, comparing(ErrorResponse.FieldError::getField));
    assertFieldError(fieldErrors.get(0), verifyField, VALIDATION_ERROR_NOT_EMPTY_MESSAGE);
  }

  @ParameterizedTest
  @CsvSource(
      value = {"null,null,null", ",,"},
      nullValues = {"null"})
  void shouldReturnBadRequestWhenMissingAllBodyFields(
      String reference, String templateId, String personalisation) throws Exception {
    // when
    ResponseEntity<ErrorResponse> response =
        postLetterDetailsWithMissFields(requestHeaders, reference, templateId, personalisation);
    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, "There were validation issues with the request.");
    List<ErrorResponse.FieldError> fieldErrors = response.getBody().getFieldErrors();
    Collections.sort(fieldErrors, comparing(ErrorResponse.FieldError::getField));
    assertFieldError(fieldErrors.get(0), "personalisation", VALIDATION_ERROR_NOT_EMPTY_MESSAGE);
    assertFieldError(fieldErrors.get(1), "reference", VALIDATION_ERROR_NOT_EMPTY_MESSAGE);
    assertFieldError(fieldErrors.get(2), "templateId", VALIDATION_ERROR_NOT_EMPTY_MESSAGE);
  }

  @Test
  void shouldReturnBadRequestWhenAddressLessThan3Lines() throws Exception {
    // when
    ResponseEntity<ErrorResponse> response =
        postLetterDetailsWithMissFields(
            requestHeaders,
            "HRTABCD123",
            "randomUUID",
            "aAddressLineNumberLessThan3Personalisation");
    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, "There were validation issues with the request.");
    List<ErrorResponse.FieldError> fieldErrors = response.getBody().getFieldErrors();
    Collections.sort(fieldErrors, comparing(ErrorResponse.FieldError::getField));
    assertFieldError(
        fieldErrors.get(0),
        "personalisation",
        "must contain addressLine1, addressLine2 and addressLine3");
  }

  @ParameterizedTest
  @CsvSource(
      value = {
        "HRTABCD123,randomUUID,aNoPostcodePersonalisation",
        "HRTABCD123,randomUUID,aNoPostcodePersonalisation8Line",
        "HRTABCD123,randomUUID,aNoPostcodePersonalisation8LineMixOrder",
        "HRTABCD123,randomUUID,aNoPostcodePersonalisation5LineMixOrder",
      })
  void shouldReturnBadRequestWhenNoPostCodeAtLastLine(
      String reference, String templateId, String personalisation) throws Exception {
    // when
    ResponseEntity<ErrorResponse> response =
        postLetterDetailsWithMissFields(requestHeaders, reference, templateId, personalisation);
    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, "There were validation issues with the request.");
    List<ErrorResponse.FieldError> fieldErrors = response.getBody().getFieldErrors();
    Collections.sort(fieldErrors, comparing(ErrorResponse.FieldError::getField));
    assertFieldError(
        fieldErrors.get(0), "personalisation", "last address line must be a valid postcode format");
  }

  @Test
  void shouldReturnBadRequestWhenReferenceFieldMoreThan100() throws Exception {
    // when
    ResponseEntity<ErrorResponse> response =
        postLetterDetailsWithMissFields(
            requestHeaders,
            "asdsadjksajdlsakldjklsadjklsajdsadlasjdkksaldjklsajdlsadjskjdkskdsaljaksldjasdklasjlaksldjlsakdjklsajdkasldjksa",
            "randomUUID",
            "aPersonalisation");
    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, "There were validation issues with the request.");
    List<ErrorResponse.FieldError> fieldErrors = response.getBody().getFieldErrors();
    Collections.sort(fieldErrors, comparing(ErrorResponse.FieldError::getField));
    assertFieldError(
        fieldErrors.get(0), "reference", "size must be equal or less than 100 characters");
  }

  @Test
  void shouldReturnInternalServerErrorWhenExceptionOccurs() throws Exception {
    // given
    LetterDetails letterDetails =
        aValidLetterDetailsWithTemplateIdWithoutTransactionDate(createLetterTemplate());
    SQLGrammarException sqlGrammarException = new SQLGrammarException("sql exception", null);
    InvalidDataAccessResourceUsageException exception =
        new InvalidDataAccessResourceUsageException("invalid data", sqlGrammarException);
    doThrow(exception).when(letterDetailsRepository).save(any(LetterDetails.class));

    // when
    ResponseEntity<ErrorResponse> response =
        postLetterDetailsSaveRequest(requestHeaders, letterDetails, ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(INTERNAL_SERVER_ERROR);
    assertErrorInResponse(response, INTERNAL_SERVER_ERROR, "invalid data");
  }

  @Test
  void shouldReturnBadRequestWhenSaveADuplicateRecord() throws Exception {
    // given
    UUID templateId = createLetterTemplate();
    LetterDetails letterDetails =
        aValidLetterDetailsWithTemplateIdWithoutTransactionDate(templateId);
    postLetterDetailsSaveRequest(requestHeaders, letterDetails, null);
    LetterDetails letterDetails2 =
        aValidLetterDetailsWithTemplateIdWithoutTransactionDate(templateId);

    // when
    ResponseEntity<ErrorResponse> response =
        postLetterDetailsSaveRequest(requestHeaders, letterDetails2, ErrorResponse.class);

    // then
    assertThat(response.getBody().getMessage())
        .contains(
            "could not execute statement [ERROR: duplicate key value violates unique constraint \"transaction_dt_letter_template_id_reference_unique");
  }

  private ResponseEntity postLetterDetailsSaveRequest(
      HttpHeaders headers, LetterDetails letterDetails, Class expectedType)
      throws URISyntaxException {
    NewLetterDetailsRequestDto newLetterDetailsRequestDto =
        LetterDetailsToNewLetterDetailsRequestDtoConverter.convert(letterDetails);
    URI uri = new URIBuilder(new HttpGet(XEROX_NOTIFICATION_API_POST_URI).getUri()).build();
    RequestEntity<?> requestEntity =
        new RequestEntity<>(newLetterDetailsRequestDto, headers, HttpMethod.POST, uri);
    return testRestTemplate.exchange(requestEntity, expectedType);
  }

  private ResponseEntity postLetterDetailsWithMissFields(
      HttpHeaders headers, String reference, String templateId, String personalisation)
      throws URISyntaxException {
    if (templateId != null && templateId.equalsIgnoreCase("randomUUID")) {
      templateId = UUID.randomUUID().toString();
    }
    NewLetterDetailsRequestDto newLetterDetailsRequestDto =
        LetterDetailsToNewLetterDetailsRequestDtoConverter.convertMissFieldRequest(
            reference, templateId, personalisation);
    URI uri = new URIBuilder(new HttpGet(XEROX_NOTIFICATION_API_POST_URI).getUri()).build();
    RequestEntity<?> requestEntity =
        new RequestEntity<>(newLetterDetailsRequestDto, headers, HttpMethod.POST, uri);
    return testRestTemplate.exchange(requestEntity, ErrorResponse.class);
  }

  private UUID createLetterTemplate() {
    LetterTemplate letterTemplate = letterTemplateRepository.save(aValidLetterTemplate());
    return letterTemplate.getId();
  }

  private void assertJaversRecord(NewLetterDetailsResponseDto letterDetailsResult) {
    QueryBuilder jqlQuery =
        QueryBuilder.byClass(LetterDetails.class)
            .withCommitProperty(LETTER_DETAILS_ID, letterDetailsResult.getId().toString());
    Changes changes = javers.findChanges(jqlQuery.build());
    assertThat(changes).isNotEmpty();
    assertThat(changes)
        .allSatisfy(
            change -> {
              assertThat(change.getCommitMetadata()).isPresent();
              var meta = change.getCommitMetadata().get();
              assertThat(meta.getAuthor()).isEqualTo(TestConstants.CREATED_BY);
              assertThat(meta.getProperties()).isNotEmpty();
              assertThat(meta.getProperties().get(LETTER_DETAILS_ID))
                  .isEqualTo(letterDetailsResult.getId().toString());
            });
  }
}
