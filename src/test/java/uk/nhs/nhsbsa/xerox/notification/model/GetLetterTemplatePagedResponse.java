package uk.nhs.nhsbsa.xerox.notification.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import uk.nhs.nhsbsa.exemptions.model.Page;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetLetterTemplatePagedResponse {

  @JsonProperty("_embedded")
  private EmbeddedLetterTemplateDto embedded;

  @JsonProperty("_links")
  private Links links;

  @JsonProperty("page")
  private Page page;
}
