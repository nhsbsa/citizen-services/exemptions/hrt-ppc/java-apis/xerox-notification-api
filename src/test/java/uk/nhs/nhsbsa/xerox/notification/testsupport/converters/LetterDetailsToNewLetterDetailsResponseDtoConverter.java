package uk.nhs.nhsbsa.xerox.notification.testsupport.converters;

import uk.nhs.nhsbsa.xerox.notification.entity.LetterDetails;
import uk.nhs.nhsbsa.xerox.notification.model.Links;
import uk.nhs.nhsbsa.xerox.notification.model.NewLetterDetailsResponseDto;

/** Converts {@link LetterDetails} to {@link NewLetterDetailsResponseDto} */
public class LetterDetailsToNewLetterDetailsResponseDtoConverter {
  public static NewLetterDetailsResponseDto convert(LetterDetails letterDetails, Links links) {
    return NewLetterDetailsResponseDto.builder()
        .id(letterDetails.getId())
        .reference(letterDetails.getReference())
        .status(letterDetails.getStatus())
        .templateId(letterDetails.getTemplateId())
        .transactionDate(letterDetails.getTransactionDate())
        .statusUpdateAt(letterDetails.getStatusChangeAt())
        .personalisation(letterDetails.getPersonalisation())
        .meta(letterDetails.getMeta())
        .links(links)
        .build();
  }
}
