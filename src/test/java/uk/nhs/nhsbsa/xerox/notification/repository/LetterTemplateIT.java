package uk.nhs.nhsbsa.xerox.notification.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static uk.nhs.nhsbsa.exemptions.utilities.ComparisonUtil.recursiveComparisonConfiguration;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterTemplateTestDataFactory.aGeneratedValidLetterTemplateWithEffectiveTo;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterTemplateTestDataFactory.aLetterTemplateWithAllFields;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterTemplateTestDataFactory.aValidLetterTemplateWithEffectiveFromEffectiveTo;

import jakarta.validation.ConstraintViolationException;
import java.time.LocalDate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockReset;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.data.domain.Page;
import uk.nhs.nhsbsa.exemptions.models.ApplicationChannel;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterTemplate;
import uk.nhs.nhsbsa.xerox.notification.validators.DynamicStringProperties;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LetterTemplateIT {

  @Autowired LetterDetailsRepository letterDetailsRepository;
  @Autowired LetterTemplateRepository letterTemplateRepository;

  @SpyBean(reset = MockReset.AFTER)
  DynamicStringProperties dynamicStringProperties;

  @BeforeEach
  void setup() {
    letterDetailsRepository.deleteAll();
    letterDetailsRepository.flush();
    letterTemplateRepository.deleteAll();
    letterTemplateRepository.flush();
  }

  @Test
  void shouldThrowsExceptionWhenInvalidServiceName() {
    // given
    LetterTemplate letterTemplate =
        aLetterTemplateWithAllFields(
            1,
            "HRT_PPC_1",
            "ISSUE",
            LocalDate.parse("2022-04-01"),
            "HRT_PPC_LETTER_20230401221021.xml",
            ApplicationChannel.ONLINE);

    assertThatThrownBy(() -> letterTemplateRepository.saveAndFlush(letterTemplate))
        .isInstanceOf(ConstraintViolationException.class)
        .hasMessage(
            "Validation failed for classes [uk.nhs.nhsbsa.xerox.notification.entity.LetterTemplate] during persist time for groups [jakarta.validation.groups.Default, ]\n"
                + "List of constraint violations:[\n"
                + "\tConstraintViolationImpl{interpolatedMessage='invalid service name', propertyPath=serviceName, rootBeanClass=class uk.nhs.nhsbsa.xerox.notification.entity.LetterTemplate, messageTemplate='{serviceName.not-valid}'}\n"
                + "]");
  }

  @Test
  void shouldGetLetterTemplateWhenFindByEffectiveOnDateRequested() {
    // given
    LocalDate effectiveTo = LocalDate.parse("2024-03-31");
    LocalDate aDateTime = effectiveTo.minusDays(100);
    LocalDate effectiveFrom = effectiveTo.minusDays(200);

    LetterTemplate actualResult = createATestLetterTemplate(effectiveTo, effectiveFrom);

    // when
    Page<LetterTemplate> results = letterTemplateRepository.findByEffectiveOnDate(aDateTime, null);

    // then
    assertSingleLetterTemplateRecordPresent(results, actualResult);
  }

  @Test
  void shouldGetLetterTemplateWhenFindByEffectiveOnDateRequestedAndEffectToIsNull() {
    // given
    LocalDate effectiveFrom = LocalDate.parse("2024-03-31");
    LocalDate aDateTime = effectiveFrom.plusDays(100);

    LetterTemplate actualResult = createATestLetterTemplate(null, effectiveFrom);

    // when
    Page<LetterTemplate> results = letterTemplateRepository.findByEffectiveOnDate(aDateTime, null);

    // then.
    assertSingleLetterTemplateRecordPresent(results, actualResult);
  }

  @Test
  void shouldGetLetterTemplateWhenFindByEffectiveOnDateRequestedAndEffectFromIsEqualsToQueryDate() {
    // given
    LocalDate effectiveFrom = LocalDate.parse("2024-03-31");

    LetterTemplate actualResult = createATestLetterTemplate(null, effectiveFrom);

    // when
    Page<LetterTemplate> results =
        letterTemplateRepository.findByEffectiveOnDate(effectiveFrom, null);

    // then.
    assertSingleLetterTemplateRecordPresent(results, actualResult);
  }

  @Test
  void
      shouldGetLetterTemplateWhenFindByEffectiveOnDateRequestedAndEffectFromIsEqualsToQueryDateAndEffectTo() {
    // given
    LocalDate effectiveFrom = LocalDate.parse("2024-03-31");

    LetterTemplate actualResult = createATestLetterTemplate(effectiveFrom, effectiveFrom);

    // when
    Page<LetterTemplate> results =
        letterTemplateRepository.findByEffectiveOnDate(effectiveFrom, null);

    // then.
    assertSingleLetterTemplateRecordPresent(results, actualResult);
  }

  @Test
  void shouldEmptyWhenFindByEffectiveOnDateRequestedAndQueryDateBeforeEffectFrom() {
    // given
    LocalDate effectiveTo = LocalDate.parse("2024-03-31");
    LocalDate aDateTime = effectiveTo.minusDays(300);
    LocalDate effectiveFrom = effectiveTo.minusDays(200);

    LetterTemplate letterTemplate =
        aValidLetterTemplateWithEffectiveFromEffectiveTo(effectiveFrom, effectiveTo);
    letterTemplateRepository.save(letterTemplate);

    // when
    Page<LetterTemplate> results = letterTemplateRepository.findByEffectiveOnDate(aDateTime, null);

    // then.
    assertThat(results).isEmpty();
  }

  @Test
  void shouldEmptyWhenFindByEffectiveOnDateRequestedAndQueryDateAfterEffectTo() {
    // given
    LocalDate effectiveTo = LocalDate.parse("2024-03-31");
    LocalDate aDateTime = effectiveTo.plusDays(100);
    LocalDate effectiveFrom = effectiveTo.minusDays(200);

    LetterTemplate letterTemplate =
        aValidLetterTemplateWithEffectiveFromEffectiveTo(effectiveFrom, effectiveTo);
    letterTemplateRepository.save(letterTemplate);

    // when
    Page<LetterTemplate> results = letterTemplateRepository.findByEffectiveOnDate(aDateTime, null);

    // then.
    assertThat(results).isEmpty();
  }

  @Test
  void shouldGetMultipleLetterTemplatesWhenFindByEffectiveOnDateRequested() {
    // given
    LocalDate effectiveTo = LocalDate.parse("2024-03-31");
    LocalDate aDateTime = effectiveTo.minusDays(100);

    int validNumberOfRecord = createValidTestRecords(effectiveTo);

    // when
    Page<LetterTemplate> resultList =
        letterTemplateRepository.findByEffectiveOnDate(aDateTime, null);

    // then.
    assertThat(resultList).hasSize(validNumberOfRecord);
  }

  @Test
  void shouldOnlyGetMultipleValidLetterTemplatesWhenFindByEffectiveOnDateRequested() {
    // given
    LocalDate effectiveTo = LocalDate.parse("2024-03-31");
    LocalDate aDateTime = effectiveTo.minusDays(100);

    int validNumberOfRecord = createValidTestRecords(effectiveTo);

    int backwardMinDays = 600;
    int numberOfRecordBeforeEffectFrom = 3;
    LocalDate effectToOutRange = effectiveTo.minusDays(validNumberOfRecord + backwardMinDays + 1);
    for (int i = 0; i < numberOfRecordBeforeEffectFrom; i++) {
      LetterTemplate letterTemplate =
          aGeneratedValidLetterTemplateWithEffectiveTo(effectToOutRange, backwardMinDays + i);
      letterTemplateRepository.save(letterTemplate);
    }

    // when
    Page<LetterTemplate> resultList =
        letterTemplateRepository.findByEffectiveOnDate(aDateTime, null);

    // then.
    assertThat(resultList).hasSize(validNumberOfRecord);
  }

  private int createValidTestRecords(LocalDate effectiveTo) {
    int validNumberOfRecord = 10;
    int backwardMinDays = 600;
    for (int i = 0; i < validNumberOfRecord; i++) {
      LetterTemplate letterTemplate =
          aGeneratedValidLetterTemplateWithEffectiveTo(effectiveTo, backwardMinDays + i);
      letterTemplateRepository.save(letterTemplate);
    }
    return validNumberOfRecord;
  }

  private LetterTemplate createATestLetterTemplate(LocalDate effectiveTo, LocalDate effectiveFrom) {
    LetterTemplate letterTemplate =
        aValidLetterTemplateWithEffectiveFromEffectiveTo(effectiveFrom, effectiveTo);
    LetterTemplate actualResult = letterTemplateRepository.save(letterTemplate);
    return actualResult;
  }

  private static void assertSingleLetterTemplateRecordPresent(
      Page<LetterTemplate> resultLetterTemplate, LetterTemplate expectedLetterTemplate) {
    assertThat(resultLetterTemplate.getContent().size()).isEqualTo(1);
    assertThat(resultLetterTemplate.getContent().get(0))
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(expectedLetterTemplate);
  }
}
