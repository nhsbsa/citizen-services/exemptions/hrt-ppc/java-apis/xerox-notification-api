package uk.nhs.nhsbsa.xerox.notification.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NewLetterTemplateResponseDto {

  private UUID id;
  private int version;
  private String serviceName;
  private String templateName;
  private String effectiveFrom;
  private String effectiveTo;
  private String xeroxTemplate;

  @JsonProperty("_meta")
  private Meta meta;

  @JsonProperty("_links")
  private Links links;
}
