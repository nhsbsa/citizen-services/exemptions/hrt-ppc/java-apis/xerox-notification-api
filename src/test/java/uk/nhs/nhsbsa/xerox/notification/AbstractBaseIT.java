package uk.nhs.nhsbsa.xerox.notification;

import org.javers.core.Javers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractBaseIT {

  @LocalServerPort int port;

  @Autowired TestRestTemplate testRestTemplate;

  @Autowired Javers javers;

  HttpHeaders requestHeaders;
}
