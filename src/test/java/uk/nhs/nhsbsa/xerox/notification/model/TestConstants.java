package uk.nhs.nhsbsa.xerox.notification.model;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/** the type test constants. */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class TestConstants extends uk.nhs.nhsbsa.exemptions.TestConstants {

  public static final String XEROX_NOTIFICATION_API_PATH_URI = "/v1/xerox-notifications";
  public static final String BAD_REQUEST_ERROR_MESSAGE = "Bad Request";
  public static final String NOT_FOUND_ERROR_MESSAGE = "No message available";
  public static final String VALIDATION_ERROR_NOT_VALID_DATE = "must be a valid date";
  public static final String VALIDATION_ERROR_NOT_VALID_STATUS = "must be a valid status";
  public static final String VALIDATION_ERROR_NOT_EMPTY_MESSAGE = "must not be null or empty";
  public static final String VALIDATION_ERROR_NOT_OVER_100_MESSAGE =
      "size must be equal or less than 100 characters";
  public static final String VALIDATION_ERROR_NOT_OVER_500_MESSAGE =
      "must not be more than 500 characters";
  public static final String VALIDATION_ERROR_ADDRESS_LESS_THAN_3_MESSAGE =
      "must contain addressLine1, addressLine2 and addressLine3";
  public static final String VALIDATION_ERROR_ADDRESS_NO_POSTCODE_MESSAGE =
      "last address line must be a valid postcode format";
  public static final String VALIDATION_ERROR_INVALID_SERVICE_NAME_MESSAGE = "invalid service name";
  public static final String LETTER_TEMPLATE_API_SEARCH_BY_EFFECTIVE_ON_DATE_URI =
      XEROX_NOTIFICATION_API_PATH_URI + "/templates/search/findByEffectiveOnDate";
  public static final String LETTER_TEMPLATE_API_SEARCH_BY_TEMPLATE_ID_URI =
      XEROX_NOTIFICATION_API_PATH_URI + "/templates";
  public static final String XEROX_NOTIFICATION_API_POST_URI =
      XEROX_NOTIFICATION_API_PATH_URI + "/letter";
  public static final String VALIDATION_KEY = "xerox-services";
  public static final String SERVICE_NAME_HRT_PPC = "HRT_PPC";
  public static final String LETTER_DETAILS_ID = "letterDetailsId";
  public static final String REFERENCE = "reference";
  public static final String TEMPLATE_ID = "templateId";
  public static final String STATUS = "status";
  public static final String TRANSACTION_DATE = "transactionDate";
}
