package uk.nhs.nhsbsa.xerox.notification.testsupport;

import static uk.nhs.nhsbsa.exemptions.models.HeaderConstants.HEADER_FIELD_USER_ID;

import org.springframework.http.HttpHeaders;
import uk.nhs.nhsbsa.xerox.notification.model.TestConstants;

public class RequestHeaderFilterTestDataFactory {

  public static HttpHeaders validRequestHeaders() {
    HttpHeaders requestHeader = new HttpHeaders();
    requestHeader.set(HEADER_FIELD_USER_ID, TestConstants.CREATED_BY);
    return requestHeader;
  }
}
