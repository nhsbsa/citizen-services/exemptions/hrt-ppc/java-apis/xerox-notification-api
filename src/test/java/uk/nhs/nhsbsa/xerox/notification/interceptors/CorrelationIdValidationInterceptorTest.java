package uk.nhs.nhsbsa.xerox.notification.interceptors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static uk.nhs.nhsbsa.xerox.notification.model.Constants.HEADER_PARAM_CORRELATION_ID;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import uk.nhs.nhsbsa.exemptions.exception.InvalidHeaderException;
import uk.nhs.nhsbsa.xerox.notification.interceptor.CorrelationIdValidationInterceptor;

@ExtendWith(MockitoExtension.class)
public class CorrelationIdValidationInterceptorTest {
  @Mock private HttpServletRequest request;
  @Mock private HttpServletResponse response;

  @InjectMocks private CorrelationIdValidationInterceptor correlationIdValidationInterceptor;

  @Test
  void shouldReturnTrueWhenCorrelationIdIsValid() {
    // given
    given(request.getHeader(HEADER_PARAM_CORRELATION_ID)).willReturn(UUID.randomUUID().toString());

    // when
    var result =
        correlationIdValidationInterceptor.preHandle(
            request, response, correlationIdValidationInterceptor);

    // then
    assertThat(result).isTrue();
    verify(request).getHeader(HEADER_PARAM_CORRELATION_ID);
    verifyNoMoreInteractions(request);
    verifyNoInteractions(response);
  }

  @Test
  void shouldReturnTrueWhenCorrelationIdIsNull() {
    // given
    given(request.getHeader(HEADER_PARAM_CORRELATION_ID)).willReturn(null);

    // when
    var result =
        correlationIdValidationInterceptor.preHandle(
            request, response, correlationIdValidationInterceptor);

    // then
    assertThat(result).isTrue();
    verify(request).getHeader(HEADER_PARAM_CORRELATION_ID);
    verifyNoMoreInteractions(request);
    verifyNoInteractions(response);
  }

  @ParameterizedTest
  @ValueSource(strings = {"", "abcd123"})
  void shouldThrowExceptionWhenCorrelationIdIsInvalid(String uuid) {
    // given
    given(request.getHeader(HEADER_PARAM_CORRELATION_ID)).willReturn(uuid);

    // when
    // then
    assertThatThrownBy(
            () ->
                correlationIdValidationInterceptor.preHandle(
                    request, response, correlationIdValidationInterceptor))
        .isInstanceOf(InvalidHeaderException.class)
        .hasMessage(HEADER_PARAM_CORRELATION_ID)
        .hasStackTraceContaining(HEADER_PARAM_CORRELATION_ID);
    verify(request).getHeader(HEADER_PARAM_CORRELATION_ID);
    verifyNoMoreInteractions(request);
    verifyNoInteractions(response);
  }
}
