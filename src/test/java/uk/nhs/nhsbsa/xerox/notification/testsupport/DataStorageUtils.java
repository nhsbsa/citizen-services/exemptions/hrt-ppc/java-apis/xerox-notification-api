package uk.nhs.nhsbsa.xerox.notification.testsupport;

import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterDetailsTestDataFactory.aLetterDetailsWithReference;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterDetailsTestDataFactory.aValidLetterDetailsWithStatus;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterTemplateTestDataFactory.aValidLetterTemplateWithEffectiveFromEffectiveTo;

import java.time.LocalDate;
import java.util.UUID;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterDetails;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterTemplate;
import uk.nhs.nhsbsa.xerox.notification.model.StatusType;
import uk.nhs.nhsbsa.xerox.notification.repository.LetterDetailsRepository;
import uk.nhs.nhsbsa.xerox.notification.repository.LetterTemplateRepository;

public class DataStorageUtils {
  public static LetterDetails saveALetterDetails(
      int minusDays,
      StatusType status,
      LetterDetailsRepository letterDetailsRepository,
      LetterTemplateRepository letterTemplateRepository) {

    LetterTemplate letterTemplate = saveLetterTemplate(minusDays, letterTemplateRepository);

    return saveLetterDetailsWithTemplateId(status, letterDetailsRepository, letterTemplate.getId());
  }

  public static LetterDetails saveLetterDetailsWithTemplateId(
      StatusType status, LetterDetailsRepository letterDetailsRepository, UUID letterTemplateId) {
    LetterDetails letterDetails = aValidLetterDetailsWithStatus(status);
    letterDetails.setTemplateId(letterTemplateId);
    letterDetailsRepository.save(letterDetails);
    return letterDetails;
  }

  public static LetterDetails saveLetterDetailsWithTemplateIdAndReference(
      String reference, LetterDetailsRepository letterDetailsRepository, UUID templateId) {
    LetterDetails letterDetails = aLetterDetailsWithReference(reference);
    letterDetails.setTemplateId(templateId);
    letterDetailsRepository.save(letterDetails);
    return letterDetails;
  }

  public static LetterTemplate saveLetterTemplate(
      int minusDays, LetterTemplateRepository letterTemplateRepository) {
    LocalDate effectiveTo = LocalDate.now();
    LocalDate effectiveFrom = effectiveTo.minusDays(minusDays);

    LetterTemplate letterTemplate =
        aValidLetterTemplateWithEffectiveFromEffectiveTo(effectiveFrom, effectiveTo);
    LetterTemplate actualResult = letterTemplateRepository.save(letterTemplate);
    return actualResult;
  }
}
