package uk.nhs.nhsbsa.xerox.notification.testsupport;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterDetails;
import uk.nhs.nhsbsa.xerox.notification.model.LetterDetailsUpdateDto;

public class UpdateLetterDetailsTestDataFactory {

  public static LetterDetailsUpdateDto aValidLetterDetailsUpdateDto() {
    return LetterDetailsUpdateDto.builder()
        .reference(Optional.of("test12122"))
        .transactionDate(Optional.of("2000-01-01"))
        .status(Optional.of("SENT"))
        .fileName("HRT_PPC_LETTER_20230601321021.xml")
        .failureReason("Invalid address")
        .build();
  }

  public static LetterDetailsUpdateDto aValidLetterDetailsUpdateDtoWithSameValue(String value) {
    return LetterDetailsUpdateDto.builder()
        .reference(Optional.ofNullable(value))
        .transactionDate(Optional.ofNullable(value))
        .status(Optional.ofNullable(value))
        .build();
  }

  public static LetterDetailsUpdateDto buildLetterDetailsUpdateDto(
      String builderName, String argument) {
    try {
      Method method =
          UpdateLetterDetailsTestDataFactory.class.getDeclaredMethod(builderName, String.class);
      return (LetterDetailsUpdateDto) method.invoke(null, argument);
    } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
      throw new RuntimeException(e);
    }
  }

  public static LetterDetailsUpdateDto aValidLetterDetailsUpdateDtoWithOnlyReference(
      String reference) {
    return LetterDetailsUpdateDto.builder().reference(Optional.ofNullable(reference)).build();
  }

  public static LetterDetailsUpdateDto aValidLetterDetailsUpdateDtoWithOnlyTransactionDate(
      String transactionDate) {
    return LetterDetailsUpdateDto.builder()
        .transactionDate(Optional.ofNullable(transactionDate))
        .build();
  }

  public static LetterDetailsUpdateDto aValidLetterDetailsUpdateDtoWithOnlyStatus(String status) {
    return LetterDetailsUpdateDto.builder().status(Optional.ofNullable(status)).build();
  }

  public static LetterDetailsUpdateDto aValidLetterDetailsUpdateDtoWithOnlyFileName(
      String fileName) {
    return LetterDetailsUpdateDto.builder().fileName(fileName).build();
  }

  public static LetterDetailsUpdateDto aValidLetterDetailsUpdateDtoWithOnlyFailureReason(
      String failureReason) {
    return LetterDetailsUpdateDto.builder().failureReason(failureReason).build();
  }

  public static LetterDetailsUpdateDto aValidLetterDetailsUpdateDtoFromLetterDetails(
      LetterDetails letterDetails) {
    return LetterDetailsUpdateDto.builder()
        .reference(Optional.of(letterDetails.getReference()))
        .transactionDate(Optional.of(letterDetails.getTransactionDate().toString()))
        .status(Optional.of(letterDetails.getStatus().name()))
        .fileName(letterDetails.getFileName())
        .failureReason(letterDetails.getFailureReason())
        .build();
  }
}
