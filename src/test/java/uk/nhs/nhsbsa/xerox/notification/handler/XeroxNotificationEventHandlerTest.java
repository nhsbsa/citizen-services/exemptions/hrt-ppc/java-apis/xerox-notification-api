package uk.nhs.nhsbsa.xerox.notification.handler;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static uk.nhs.nhsbsa.exemptions.utilities.TestLoggingUtilities.getLogEvents;
import static uk.nhs.nhsbsa.exemptions.utilities.TestLoggingUtilities.startRecordingLogsFor;
import static uk.nhs.nhsbsa.exemptions.utilities.TestLoggingUtilities.stopRecordingLogsFor;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LetterDetailsTestDataFactory.aValidLetterDetails;

import ch.qos.logback.classic.spi.ILoggingEvent;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterDetails;
import uk.nhs.nhsbsa.xerox.notification.model.StatusType;
import uk.nhs.nhsbsa.xerox.notification.model.XeroxNotificationApplicationRequestContext;

@ExtendWith(MockitoExtension.class)
public class XeroxNotificationEventHandlerTest {

  @Mock
  private XeroxNotificationApplicationRequestContext xeroxNotificationApplicationRequestContext;

  @Mock private LetterDetails letterDetails;
  @InjectMocks private XeroxNotificationEventHandler xeroxNotificationEventHandler;

  @BeforeEach
  void init() {
    startRecordingLogsFor(XeroxNotificationEventHandler.class);
  }

  @AfterEach
  void tearDown() {
    stopRecordingLogsFor(XeroxNotificationEventHandler.class);
  }

  @Test
  void shouldPopulateInformationWhenValidLetterDetailsProvided() {
    // given
    LocalDateTime localDateTime = LocalDateTime.parse("2022-01-01T12:12:12");
    LocalDate localDate = LocalDate.parse("2023-01-01");
    UUID correlationId = UUID.randomUUID();
    given(xeroxNotificationApplicationRequestContext.getCorrelationId())
        .willReturn(Optional.of(correlationId));
    given(xeroxNotificationApplicationRequestContext.getTransactionDate()).willReturn(localDate);
    given(xeroxNotificationApplicationRequestContext.getStatus()).willReturn(StatusType.PENDING);

    // when
    xeroxNotificationEventHandler.handleLetterDetailsBeforeCreate(letterDetails);

    // then
    verify(letterDetails).setCorrelationId(correlationId);
    verify(letterDetails).setTransactionDate(localDate);
    verify(letterDetails).setStatus(StatusType.PENDING);
    verify(xeroxNotificationApplicationRequestContext).getCorrelationId();
    verify(xeroxNotificationApplicationRequestContext).getTransactionDate();
    verify(xeroxNotificationApplicationRequestContext).getStatus();
    verifyNoMoreInteractions(xeroxNotificationApplicationRequestContext, letterDetails);
  }

  @Test
  void shouldLogLetterDetailsCreatedAfterCreate() {
    // given
    LetterDetails letterDetailsLog = aValidLetterDetails();

    // when
    xeroxNotificationEventHandler.handleLetterDetailsAfterCreate(letterDetailsLog);

    // then
    List<ILoggingEvent> events = getLogEvents();
    assertThat(events).hasSize(1);
    AssertionsForClassTypes.assertThat(events.get(0).getFormattedMessage())
        .startsWith("LetterDetails created [");
    AssertionsForClassTypes.assertThat(events.get(0).getFormattedMessage()).endsWith("]");
    verifyNoInteractions(xeroxNotificationApplicationRequestContext, letterDetails);
  }

  @Test
  void shouldLogLetterDetailsUpdatedAfterUpdated() {
    // given
    LetterDetails letterDetailsLog = aValidLetterDetails();

    // when
    xeroxNotificationEventHandler.handleLetterDetailsAfterUpdate(letterDetailsLog);

    // then
    List<ILoggingEvent> events = getLogEvents();
    assertThat(events).hasSize(1);
    AssertionsForClassTypes.assertThat(events.get(0).getFormattedMessage())
        .startsWith("LetterDetails updated [");
    AssertionsForClassTypes.assertThat(events.get(0).getFormattedMessage()).endsWith("]");
    verifyNoInteractions(xeroxNotificationApplicationRequestContext, letterDetails);
  }
}
