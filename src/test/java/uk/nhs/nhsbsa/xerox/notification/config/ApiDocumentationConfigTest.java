package uk.nhs.nhsbsa.xerox.notification.config;

import static org.assertj.core.api.Assertions.assertThat;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
public class ApiDocumentationConfigTest {

  @InjectMocks private ApiDocumentationConfig apiDocumentationConfig;

  @BeforeEach
  void init() {
    ReflectionTestUtils.setField(apiDocumentationConfig, "appVersion", "99.99");
  }

  @Test
  void shouldReturnOpenApiDocumentation() {
    // when
    OpenAPI responseEntity = apiDocumentationConfig.openApiDocumentation();

    // then
    assertThat(responseEntity).isEqualTo(expectedOpenApi());
  }

  private OpenAPI expectedOpenApi() {
    return new OpenAPI()
        .components(new Components())
        .addServersItem(new Server().url("http://localhost:8160"))
        .info(
            new Info()
                .title("Xerox Notification service")
                .description("Responsible for the persistence and retrieval of xerox notifications")
                .version("99.99")
                .contact(
                    new Contact()
                        .name("NHS Business Services Authority")
                        .url("https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc"))
                .license(
                    new License()
                        .name("Apache 2.0")
                        .url("https://opensource.org/licenses/Apache-2.0")));
  }
}
