package uk.nhs.nhsbsa.xerox.notification.validators;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.VALIDATION_ERROR_ADDRESS_LESS_THAN_3_MESSAGE;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.VALIDATION_ERROR_ADDRESS_NO_POSTCODE_MESSAGE;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.VALIDATION_ERROR_NOT_EMPTY_MESSAGE;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.PersonalisationTestDataFactory.aAddressLineNumberLessThan3Personalisation;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.PersonalisationTestDataFactory.aNoPostcodePersonalisation;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.PersonalisationTestDataFactory.aPersonalisation;

import jakarta.validation.ConstraintValidatorContext;
import java.util.HashMap;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class PersonalisationDetailsValidatorTest {
  @Mock PersonalisationDetails personalisationDetails;
  @Mock ConstraintValidatorContext context;
  @Mock ConstraintValidatorContext.ConstraintViolationBuilder builder;

  @Test
  void shouldBeTrueWhenValueIsValid() {
    // given
    PersonalisationDetailsValidator validator = new PersonalisationDetailsValidator();
    validator.initialize(personalisationDetails);

    // when
    boolean result = validator.isValid(aPersonalisation(), context);

    // then
    assertThat(result).isTrue();
    verify(context).disableDefaultConstraintViolation();
    verifyNoMoreInteractions(context);
    verifyNoInteractions(personalisationDetails, builder);
  }

  @Test
  void shouldBeFalseWhenAddressLessThan3() {
    // given
    given(context.buildConstraintViolationWithTemplate(anyString())).willReturn(builder);
    PersonalisationDetailsValidator validator = new PersonalisationDetailsValidator();
    validator.initialize(personalisationDetails);

    // when
    boolean result = validator.isValid(aAddressLineNumberLessThan3Personalisation(), context);

    // then
    assertThat(result).isFalse();
    verify(context).disableDefaultConstraintViolation();
    verify(context)
        .buildConstraintViolationWithTemplate(VALIDATION_ERROR_ADDRESS_LESS_THAN_3_MESSAGE);
    verify(builder).addConstraintViolation();
    verifyNoMoreInteractions(context, builder);
    verifyNoInteractions(personalisationDetails);
  }

  @Test
  void shouldBeFalseWhenLastAddressLineIsNotPostcode() {
    // given
    given(context.buildConstraintViolationWithTemplate(anyString())).willReturn(builder);
    PersonalisationDetailsValidator validator = new PersonalisationDetailsValidator();
    validator.initialize(personalisationDetails);

    // when
    boolean result = validator.isValid(aNoPostcodePersonalisation(), context);

    // then
    assertThat(result).isFalse();
    verify(context).disableDefaultConstraintViolation();
    verify(context)
        .buildConstraintViolationWithTemplate(VALIDATION_ERROR_ADDRESS_NO_POSTCODE_MESSAGE);
    verify(builder).addConstraintViolation();
    verifyNoMoreInteractions(context, builder);
    verifyNoInteractions(personalisationDetails);
  }

  @Test
  void shouldBeFalseWhenValueMapIsEmpty() {
    // given
    given(context.buildConstraintViolationWithTemplate(anyString())).willReturn(builder);
    PersonalisationDetailsValidator validator = new PersonalisationDetailsValidator();
    validator.initialize(personalisationDetails);

    // when
    boolean result = validator.isValid(new HashMap<>(), context);

    // then
    assertThat(result).isFalse();
    verify(context).disableDefaultConstraintViolation();
    verify(context).buildConstraintViolationWithTemplate(VALIDATION_ERROR_NOT_EMPTY_MESSAGE);
    verify(builder).addConstraintViolation();
    verifyNoMoreInteractions(context, builder);
    verifyNoInteractions(personalisationDetails);
  }
}
