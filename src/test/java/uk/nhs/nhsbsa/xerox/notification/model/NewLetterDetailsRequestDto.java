package uk.nhs.nhsbsa.xerox.notification.model;

import java.util.Map;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NewLetterDetailsRequestDto {
  @Setter(AccessLevel.PRIVATE)
  private String reference;

  @Setter(AccessLevel.PRIVATE)
  private String templateId;

  @Setter(AccessLevel.PRIVATE)
  @JdbcTypeCode(SqlTypes.JSON)
  private Map<String, String> personalisation;
}
