package uk.nhs.nhsbsa.xerox.notification.interceptors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static uk.nhs.nhsbsa.xerox.notification.model.Constants.QUERY_PARAM_RUN_DATE;
import static uk.nhs.nhsbsa.xerox.notification.model.Constants.XEROX_NOTIFICATION_API_SEARCH_BY_EFFECTIVE_ON_DATE_URI;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpMethod;
import uk.nhs.nhsbsa.exemptions.exception.InvalidQueryParameterException;
import uk.nhs.nhsbsa.xerox.notification.interceptor.RunDateValidationInterceptor;

@ExtendWith(MockitoExtension.class)
public class RunDateValidationInterceptorTest {

  @Mock private HttpServletRequest request;
  @Mock private HttpServletResponse response;

  @InjectMocks private RunDateValidationInterceptor runDateValidationInterceptor;

  @Test
  void shouldReturnTrueWhenRunDateIsIsValidForFilteredByEffectiveOnDate() {
    // given
    given(request.getMethod()).willReturn(HttpMethod.GET.name());
    given(request.getRequestURI())
        .willReturn(XEROX_NOTIFICATION_API_SEARCH_BY_EFFECTIVE_ON_DATE_URI);
    given(request.getParameter(QUERY_PARAM_RUN_DATE)).willReturn("2023-04-01");

    // when
    var result =
        runDateValidationInterceptor.preHandle(request, response, runDateValidationInterceptor);

    // then
    assertThat(result).isTrue();
    verify(request).getParameter(QUERY_PARAM_RUN_DATE);
    verify(request).getMethod();
    verify(request).getRequestURI();
    verifyNoMoreInteractions(request);
    verifyNoInteractions(response);
  }

  @Test
  void shouldReturnTrueWhenGetRequestIsReceivedWithNullRequestURI() {
    // given
    given(request.getMethod()).willReturn(HttpMethod.GET.name());
    given(request.getRequestURI()).willReturn(null);

    // when
    boolean result =
        runDateValidationInterceptor.preHandle(request, response, runDateValidationInterceptor);

    // then
    assertThat(result).isTrue();
    verify(request).getMethod();
    verify(request).getRequestURI();
    verifyNoMoreInteractions(request);
    verifyNoInteractions(response);
  }

  @Test
  void shouldReturnTrueWhenGetRequestIsReceivedWithInvalidRequestURI() {
    // given
    given(request.getMethod()).willReturn(HttpMethod.GET.name());
    given(request.getRequestURI()).willReturn("invalidRequestURI");

    // when
    boolean result =
        runDateValidationInterceptor.preHandle(request, response, runDateValidationInterceptor);

    // then
    assertThat(result).isTrue();
    verify(request).getMethod();
    verify(request).getRequestURI();
    verifyNoMoreInteractions(request);
    verifyNoInteractions(response);
  }

  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource(
      strings = {
        "",
        "abcd",
        "01-01-2023",
        "2021-13-01",
        "2021-01-32",
        "2021/01/31",
        "31/01/2023",
        "01/31/2023",
        "20230101",
        "2023-04-01,2023-04-02",
      })
  void shouldThrowExceptionWhenGetRequestIsReceivedWithInvalidRequestURI(String runDate) {
    // given
    given(request.getMethod()).willReturn(HttpMethod.GET.name());
    given(request.getRequestURI())
        .willReturn(XEROX_NOTIFICATION_API_SEARCH_BY_EFFECTIVE_ON_DATE_URI);
    given(request.getParameter(QUERY_PARAM_RUN_DATE)).willReturn(runDate);

    // when
    // then
    assertThatThrownBy(
            () ->
                runDateValidationInterceptor.preHandle(
                    request, response, runDateValidationInterceptor))
        .isInstanceOf(InvalidQueryParameterException.class)
        .hasMessage(QUERY_PARAM_RUN_DATE)
        .hasStackTraceContaining(QUERY_PARAM_RUN_DATE);
    verify(request).getParameter(QUERY_PARAM_RUN_DATE);
    verify(request).getRequestURI();
    verify(request).getMethod();
    verifyNoMoreInteractions(request);
    verifyNoInteractions(response);
  }

  @ParameterizedTest
  @ValueSource(strings = {"POST", "HEAD", "PUT", "PATCH", "DELETE", "OPTIONS", "TRACE"})
  void shouldNotValidateIfHttpMethodOtherThanGet(String httpMethod) {
    // given
    given(request.getMethod()).willReturn(httpMethod);
    given(request.getRequestURI())
        .willReturn(XEROX_NOTIFICATION_API_SEARCH_BY_EFFECTIVE_ON_DATE_URI);

    // when
    var result =
        runDateValidationInterceptor.preHandle(request, response, runDateValidationInterceptor);

    // then
    assertThat(result).isTrue();
    verify(request).getMethod();
    verifyNoMoreInteractions(request);
    verifyNoInteractions(response);
  }
}
