package uk.nhs.nhsbsa.xerox.notification;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.OK;
import static uk.nhs.nhsbsa.exemptions.TestConstants.CREATED_BY;
import static uk.nhs.nhsbsa.exemptions.assertions.TestAssertions.assertErrorInResponse;
import static uk.nhs.nhsbsa.exemptions.testsupport.PageTestDataFactory.validPageWithGivenDetails;
import static uk.nhs.nhsbsa.exemptions.utilities.ComparisonUtil.recursiveComparisonConfiguration;
import static uk.nhs.nhsbsa.exemptions.utilities.UriBuilderUtil.buildQueryUri;
import static uk.nhs.nhsbsa.xerox.notification.model.Constants.HEADER_PARAM_SERVICE_NAME;
import static uk.nhs.nhsbsa.xerox.notification.model.Constants.QUERY_PARAM_STATUS;
import static uk.nhs.nhsbsa.xerox.notification.model.Constants.QUERY_PARAM_TEMPLATE_ID;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.BAD_REQUEST_ERROR_MESSAGE;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.SERVICE_NAME_HRT_PPC;
import static uk.nhs.nhsbsa.xerox.notification.model.TestConstants.XEROX_NOTIFICATION_API_POST_URI;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.LinksTestDataFactory.aValidLinksBuilderWithTemplateAndLetterDetails;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.MetaUtility.validMetaWithAllFields;
import static uk.nhs.nhsbsa.xerox.notification.testsupport.XeroxHeadersTestDataFactory.validXeroxRequestHeaders;

import com.querydsl.core.types.Predicate;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.UUID;
import org.hibernate.exception.SQLGrammarException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockReset;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import uk.nhs.nhsbsa.exemptions.models.ErrorResponse;
import uk.nhs.nhsbsa.xerox.notification.entity.LetterDetails;
import uk.nhs.nhsbsa.xerox.notification.model.GetLetterDetailsPagedResponse;
import uk.nhs.nhsbsa.xerox.notification.model.Links;
import uk.nhs.nhsbsa.xerox.notification.model.Meta;
import uk.nhs.nhsbsa.xerox.notification.model.NewLetterDetailsResponseDto;
import uk.nhs.nhsbsa.xerox.notification.model.StatusType;
import uk.nhs.nhsbsa.xerox.notification.model.XeroxNotificationApplicationRequestContext;
import uk.nhs.nhsbsa.xerox.notification.repository.LetterDetailsRepository;
import uk.nhs.nhsbsa.xerox.notification.repository.LetterTemplateRepository;
import uk.nhs.nhsbsa.xerox.notification.security.SpringSecurityAuditorAware;
import uk.nhs.nhsbsa.xerox.notification.testsupport.DataStorageUtils;
import uk.nhs.nhsbsa.xerox.notification.testsupport.converters.LetterDetailsToNewLetterDetailsResponseDtoConverter;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureWebTestClient
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class XeroxNotificationGetLetterIT extends AbstractBaseIT {
  @Mock
  private XeroxNotificationApplicationRequestContext xeroxNotificationApplicationRequestContext;

  @Autowired private SpringSecurityAuditorAware springSecurityAuditorAware;

  @SpyBean(reset = MockReset.AFTER)
  LetterDetailsRepository letterDetailsRepository;

  @SpyBean(reset = MockReset.AFTER)
  private LetterTemplateRepository letterTemplateRepository;

  @BeforeEach
  public void setup() {
    MockHttpServletRequest request = new MockHttpServletRequest();
    RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    given(xeroxNotificationApplicationRequestContext.getUser()).willReturn(CREATED_BY);
    ReflectionTestUtils.setField(
        springSecurityAuditorAware,
        "xeroxNotificationApplicationRequestContext",
        xeroxNotificationApplicationRequestContext);

    requestHeaders = validXeroxRequestHeaders(CREATED_BY);
    requestHeaders.set(HEADER_PARAM_SERVICE_NAME, SERVICE_NAME_HRT_PPC);
    letterDetailsRepository.deleteAll();
    letterDetailsRepository.flush();
    letterTemplateRepository.deleteAll();
    letterTemplateRepository.flush();
  }

  @AfterEach
  public void tearDown() {
    RequestContextHolder.resetRequestAttributes();
  }

  @Test
  void shouldReturnLetterDetailsWhenQueryByValidTemplateIdAndStatus() throws Exception {
    // given
    LetterDetails letterDetails = saveALetterDetails(200, StatusType.PENDING);
    Map<String, Object> queryParams =
        Map.of(
            QUERY_PARAM_TEMPLATE_ID,
            letterDetails.getTemplateId().toString(),
            QUERY_PARAM_STATUS,
            StatusType.PENDING.name());

    // when
    ResponseEntity<GetLetterDetailsPagedResponse> response =
        getLetterDetailsFindByRequestWithParameter(
            queryParams, requestHeaders, GetLetterDetailsPagedResponse.class);

    // then
    assertGetResults(letterDetails, response);
  }

  @Test
  void shouldReturnLetterDetailsWhenQueryByValidStatus() throws Exception {
    // given
    LetterDetails letterDetails = saveALetterDetails(200, StatusType.PENDING);
    Map<String, Object> queryParams = Map.of(QUERY_PARAM_STATUS, StatusType.PENDING.name());

    // when
    ResponseEntity<GetLetterDetailsPagedResponse> response =
        getLetterDetailsFindByRequestWithParameter(
            queryParams, requestHeaders, GetLetterDetailsPagedResponse.class);

    // then
    assertGetResults(letterDetails, response);
  }

  @Test
  void shouldReturnLetterDetailsWhenQueryByValidTemplateId() throws Exception {
    // given
    LetterDetails letterDetails = saveALetterDetails(200, StatusType.PENDING);
    Map<String, Object> queryParams =
        Map.of(QUERY_PARAM_TEMPLATE_ID, letterDetails.getTemplateId().toString());

    // when
    ResponseEntity<GetLetterDetailsPagedResponse> response =
        getLetterDetailsFindByRequestWithParameter(
            queryParams, requestHeaders, GetLetterDetailsPagedResponse.class);

    // then
    assertGetResults(letterDetails, response);
  }

  @Test
  void shouldReturnLetterDetailsWhenNoParameters() throws Exception {
    // given
    LetterDetails letterDetails = saveALetterDetails(200, StatusType.PENDING);
    Map<String, Object> queryParams = Map.of();

    // when
    ResponseEntity<GetLetterDetailsPagedResponse> response =
        getLetterDetailsFindByRequestWithParameter(
            queryParams, requestHeaders, GetLetterDetailsPagedResponse.class);

    // then
    assertGetResults(letterDetails, response);
  }

  @Test
  void shouldReturnLetterDetailsWhenParametersAreEmpty() throws Exception {
    // given
    LetterDetails letterDetails = saveALetterDetails(200, StatusType.PENDING);
    Map<String, Object> queryParams = Map.of(QUERY_PARAM_TEMPLATE_ID, "", QUERY_PARAM_STATUS, "");

    // when
    ResponseEntity<GetLetterDetailsPagedResponse> response =
        getLetterDetailsFindByRequestWithParameter(
            queryParams, requestHeaders, GetLetterDetailsPagedResponse.class);

    // then
    assertGetResults(letterDetails, response);
  }

  @Test
  void shouldReturnEmptyRecordsWhenNoRecordsFound() throws Exception {
    // given
    saveALetterDetails(200, StatusType.PENDING);
    Map<String, Object> queryParams =
        Map.of(
            QUERY_PARAM_TEMPLATE_ID,
            UUID.randomUUID().toString(),
            QUERY_PARAM_STATUS,
            StatusType.SENT.name());

    // when
    ResponseEntity<GetLetterDetailsPagedResponse> response =
        getLetterDetailsFindByRequestWithParameter(
            queryParams, requestHeaders, GetLetterDetailsPagedResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(OK);
    assertThat(response.getBody().getEmbedded().getLetter()).isEmpty();
    var expectedPageResource = validPageWithGivenDetails("20", "0", "0", "0");
    assertThat(response.getBody().getPage())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(expectedPageResource);
  }

  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource(strings = {"aaaaaaaaaahhhhhhhhhhhhslllllllllllllllllllllll4785960684373737"})
  void shouldReturnBadRequestWhenUserIdHeaderMissingOrInvalid(String user) throws Exception {
    // given
    requestHeaders = validXeroxRequestHeaders(user);
    LetterDetails letterDetails = saveALetterDetails(200, StatusType.PENDING);
    Map<String, Object> queryParams =
        Map.of(QUERY_PARAM_TEMPLATE_ID, letterDetails.getTemplateId().toString());

    // when
    ResponseEntity<ErrorResponse> response =
        getLetterDetailsFindByRequestWithParameter(
            queryParams, requestHeaders, ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, BAD_REQUEST_ERROR_MESSAGE);
  }

  @ParameterizedTest
  @ValueSource(strings = {"pending", "abcd", "Sent", "PENDING,SENT"})
  void shouldReturnBadRequestWhenInvalidStatus(String status) throws Exception {
    // given
    saveALetterDetails(200, StatusType.PENDING);
    Map<String, Object> queryParams = Map.of(QUERY_PARAM_STATUS, status);

    // when
    ResponseEntity<ErrorResponse> response =
        getLetterDetailsFindByRequestWithParameter(
            queryParams, requestHeaders, ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, BAD_REQUEST_ERROR_MESSAGE);
  }

  @ParameterizedTest
  @ValueSource(
      strings = {
        "notvalid",
        "xxxx-xxxxxxx-xxxxxx-xxxxxxxxx",
        "e2506dda-05f3-11ee-be56asdas-0242ac12000ad"
      })
  void shouldReturnBadRequestWhenInvalidTemplate(String templateId) throws Exception {
    // given
    saveALetterDetails(200, StatusType.PENDING);
    Map<String, Object> queryParams = Map.of(QUERY_PARAM_TEMPLATE_ID, templateId);

    // when
    ResponseEntity<ErrorResponse> response =
        getLetterDetailsFindByRequestWithParameter(
            queryParams, requestHeaders, ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
    assertErrorInResponse(response, BAD_REQUEST, BAD_REQUEST_ERROR_MESSAGE);
  }

  @Test
  void shouldReturnInternalServerErrorWhenExceptionOccurs() throws Exception {
    // given
    LetterDetails letterDetails = saveALetterDetails(200, StatusType.PENDING);
    SQLGrammarException sqlGrammarException = new SQLGrammarException("sql exception", null);
    InvalidDataAccessResourceUsageException exception =
        new InvalidDataAccessResourceUsageException("invalid data", sqlGrammarException);
    doThrow(exception)
        .when(letterDetailsRepository)
        .findAll(any(Predicate.class), any(Pageable.class));
    Map<String, Object> queryParams =
        Map.of(QUERY_PARAM_TEMPLATE_ID, letterDetails.getTemplateId().toString());

    // when
    ResponseEntity<ErrorResponse> response =
        getLetterDetailsFindByRequestWithParameter(
            queryParams, requestHeaders, ErrorResponse.class);

    // then
    assertThat(response.getStatusCode()).isEqualTo(INTERNAL_SERVER_ERROR);
    assertErrorInResponse(response, INTERNAL_SERVER_ERROR, "invalid data");
  }

  private void assertGetResults(
      LetterDetails letterDetails, ResponseEntity<GetLetterDetailsPagedResponse> response) {
    Meta meta =
        validMetaWithAllFields(
            letterDetails.getCreatedTimestamp(),
            letterDetails.getCreatedBy(),
            letterDetails.getUpdatedTimestamp(),
            letterDetails.getUpdatedBy());
    Links links =
        aValidLinksBuilderWithTemplateAndLetterDetails(
            port, letterDetails.getId().toString(), letterDetails.getTemplateId().toString());
    NewLetterDetailsResponseDto newUserPreferenceResponseDto =
        LetterDetailsToNewLetterDetailsResponseDtoConverter.convert(letterDetails, links);
    var expectedPageResource = validPageWithGivenDetails("20", "1", "1", "0");
    assertThat(response.getStatusCode()).isEqualTo(OK);
    assertThat(response.getBody().getEmbedded().getLetter()).hasSize(1);
    NewLetterDetailsResponseDto letterDetailsResult =
        response.getBody().getEmbedded().getLetter().get(0);
    assertThat(letterDetailsResult)
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .ignoringFields("meta", "statusUpdateAt")
        .isEqualTo(newUserPreferenceResponseDto);
    assertThat(letterDetailsResult.getStatusUpdateAt())
        .isEqualToIgnoringMinutes(newUserPreferenceResponseDto.getStatusUpdateAt());
    assertThat(response.getBody().getPage())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .isEqualTo(expectedPageResource);
    assertThat(letterDetailsResult.getMeta())
        .usingRecursiveComparison(recursiveComparisonConfiguration)
        .ignoringFields("createdTimestamp", "updatedTimestamp")
        .isEqualTo(meta);
    assertThat(letterDetailsResult.getMeta().getCreatedTimestamp())
        .isEqualToIgnoringMinutes(meta.getCreatedTimestamp());
    assertThat(letterDetailsResult.getMeta().getUpdatedTimestamp())
        .isEqualToIgnoringMinutes(meta.getUpdatedTimestamp());
  }

  private ResponseEntity getLetterDetailsFindByRequestWithParameter(
      Map<String, Object> queryParams, HttpHeaders headers, Class expectedType)
      throws URISyntaxException {
    URI uri = buildQueryUri(queryParams, XEROX_NOTIFICATION_API_POST_URI);
    RequestEntity<?> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, uri);
    return testRestTemplate.exchange(requestEntity, expectedType);
  }

  private LetterDetails saveALetterDetails(int minusDays, StatusType status) {
    return DataStorageUtils.saveALetterDetails(
        minusDays, status, letterDetailsRepository, letterTemplateRepository);
  }
}
