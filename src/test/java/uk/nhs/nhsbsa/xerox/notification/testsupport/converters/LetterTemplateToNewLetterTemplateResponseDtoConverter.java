package uk.nhs.nhsbsa.xerox.notification.testsupport.converters;

import uk.nhs.nhsbsa.xerox.notification.entity.LetterTemplate;
import uk.nhs.nhsbsa.xerox.notification.model.Links;
import uk.nhs.nhsbsa.xerox.notification.model.NewLetterTemplateResponseDto;

/**
 * Converts {@link LetterTemplate} to a letterTemplate response dto {@link
 * NewLetterTemplateResponseDto}
 */
public class LetterTemplateToNewLetterTemplateResponseDtoConverter {

  public static NewLetterTemplateResponseDto convert(LetterTemplate letterTemplate, Links links) {
    return NewLetterTemplateResponseDto.builder()
        .id(letterTemplate.getId())
        .version(letterTemplate.getVersion())
        .serviceName(letterTemplate.getServiceName())
        .templateName(letterTemplate.getTemplateName())
        .effectiveFrom(letterTemplate.getEffectiveFrom().toString())
        .effectiveTo(
            letterTemplate.getEffectiveTo() == null
                ? null
                : letterTemplate.getEffectiveTo().toString())
        .xeroxTemplate(letterTemplate.getTemplate())
        .meta(letterTemplate.getMeta())
        .links(links)
        .build();
  }

  public static NewLetterTemplateResponseDto convert(LetterTemplate letterTemplate) {
    return NewLetterTemplateResponseDto.builder()
        .id(letterTemplate.getId())
        .meta(letterTemplate.getMeta())
        .build();
  }
}
