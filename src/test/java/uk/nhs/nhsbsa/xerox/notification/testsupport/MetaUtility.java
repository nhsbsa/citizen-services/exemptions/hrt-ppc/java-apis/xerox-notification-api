package uk.nhs.nhsbsa.xerox.notification.testsupport;

import java.time.LocalDateTime;
import uk.nhs.nhsbsa.xerox.notification.entity.VersionedEntity;
import uk.nhs.nhsbsa.xerox.notification.model.Meta;

public class MetaUtility {

  public static <T extends VersionedEntity> Meta getExpectedMetaData(T t) {
    return Meta.builder()
        .createdTimestamp(t.getCreatedTimestamp())
        .createdBy(t.getCreatedBy())
        .updatedTimestamp(t.getUpdatedTimestamp())
        .updatedBy(t.getUpdatedBy())
        .build();
  }

  public static Meta validMetaWithAllFields(
      LocalDateTime createdTimestamp,
      String createdBy,
      LocalDateTime updatedTimestamp,
      String updatedBy) {
    return Meta.builder()
        .createdTimestamp(createdTimestamp)
        .createdBy(createdBy)
        .updatedTimestamp(updatedTimestamp)
        .updatedBy(updatedBy)
        .build();
  }
}
